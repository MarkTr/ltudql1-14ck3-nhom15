/****** Object:  Database [UDQLThuVien]    Script Date: 1/11/2017 8:06:16 PM ******/
CREATE DATABASE [UDQLThuVien]
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [UDQLThuVien].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [UDQLThuVien] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [UDQLThuVien] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [UDQLThuVien] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [UDQLThuVien] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [UDQLThuVien] SET ARITHABORT OFF 
GO
ALTER DATABASE [UDQLThuVien] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [UDQLThuVien] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [UDQLThuVien] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [UDQLThuVien] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [UDQLThuVien] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [UDQLThuVien] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [UDQLThuVien] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [UDQLThuVien] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [UDQLThuVien] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [UDQLThuVien] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [UDQLThuVien] SET  DISABLE_BROKER 
GO
ALTER DATABASE [UDQLThuVien] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [UDQLThuVien] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [UDQLThuVien] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [UDQLThuVien] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [UDQLThuVien] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [UDQLThuVien] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [UDQLThuVien] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [UDQLThuVien] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [UDQLThuVien] SET  MULTI_USER 
GO
ALTER DATABASE [UDQLThuVien] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [UDQLThuVien] SET DB_CHAINING OFF 
GO
USE [UDQLThuVien]
GO
/****** Object:  Table [dbo].[ChiTietPhieu]    Script Date: 1/11/2017 8:06:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietPhieu](
	[MaPhieu] [nvarchar](15) NOT NULL,
	[MaSach] [nvarchar](20) NOT NULL,
	[Soluong] [int] NULL,
	[NgayMuon] [datetime] NOT NULL,
	[HanTra] [datetime] NULL,
	[NgayTra] [datetime] NULL,
	[SoNgayTre] [int] NULL,
	[GhiChu] [nvarchar](200) NULL,
	[DaTra] [bit] NULL,
	[DaMat] [bit] NULL,
 CONSTRAINT [PK_ChiTietPhieuMuon_1] PRIMARY KEY CLUSTERED 
(
	[MaPhieu] ASC,
	[MaSach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DocGia]    Script Date: 1/11/2017 8:06:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocGia](
	[MADG] [nvarchar](10) NOT NULL,
	[TenDG] [nvarchar](50) NOT NULL,
	[CMND] [nvarchar](20) NOT NULL,
	[DiaChi] [nvarchar](100) NOT NULL,
	[NgaySinh] [datetime] NULL,
	[GioiTinh] [nvarchar](10) NOT NULL,
	[HanSuDung] [datetime] NULL,
	[LoaiDocGia] [int] NULL,
	[Fee] [int] NULL,
	[SachQuaHan] [int] NULL,
	[SoSachDangMuon] [int] NULL,
	[TinhTrang] [bit] NOT NULL,
 CONSTRAINT [PK_DocGia_1] PRIMARY KEY CLUSTERED 
(
	[MADG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HoaDon]    Script Date: 1/11/2017 8:06:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HoaDon](
	[IDHoaDon] [int] NOT NULL,
	[MaDG] [nvarchar](10) NOT NULL,
	[HanThe] [datetime] NULL,
	[TenDG] [nvarchar](50) NULL,
	[Phi] [int] NOT NULL,
 CONSTRAINT [PK_HoaDon] PRIMARY KEY CLUSTERED 
(
	[IDHoaDon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiDocGia]    Script Date: 1/11/2017 8:06:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiDocGia](
	[ID] [int] NOT NULL,
	[Role] [nvarchar](50) NOT NULL,
	[Phi] [int] NOT NULL,
	[SoSachMuonDc] [int] NOT NULL,
	[TaiLieuDB] [bit] NOT NULL,
	[SoNgayDcMuon] [int] NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiSach]    Script Date: 1/11/2017 8:06:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiSach](
	[MaLoaiSach] [int] NOT NULL,
	[LoaiSach] [nvarchar](50) NULL,
	[ThongTin] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoaiSach] PRIMARY KEY CLUSTERED 
(
	[MaLoaiSach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhanVien]    Script Date: 1/11/2017 8:06:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhanVien](
	[MSNV] [nvarchar](20) NOT NULL,
	[HoTenNV] [nvarchar](50) NOT NULL,
	[NgaySinhNV] [datetime] NULL,
	[GioiTinhNV] [nvarchar](10) NULL,
	[DienThoai] [nvarchar](11) NULL,
 CONSTRAINT [PK_NhanVien] PRIMARY KEY CLUSTERED 
(
	[MSNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PhieuMuonTra]    Script Date: 1/11/2017 8:06:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhieuMuonTra](
	[MaPhieu] [nvarchar](15) NOT NULL,
	[MSDG] [nvarchar](10) NOT NULL,
	[MSNVChoMuon] [nvarchar](20) NOT NULL,
	[MSNVNhanTra] [nvarchar](20) NOT NULL,
	[TongSoMuon] [int] NOT NULL,
	[DaTra] [int] NOT NULL,
	[Phat] [int] NULL,
 CONSTRAINT [PK_MuonSach] PRIMARY KEY CLUSTERED 
(
	[MaPhieu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuyenSach]    Script Date: 1/11/2017 8:06:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuyenSach](
	[MaSach] [nvarchar](20) NOT NULL,
	[TenSach] [nvarchar](100) NULL,
	[TacGia] [nvarchar](50) NULL,
	[NXB] [nvarchar](50) NULL,
	[MaLoaiSach] [int] NULL,
	[NamXB] [int] NULL,
	[LanXB] [int] NULL,
	[SoLuong] [int] NULL,
	[NoiDungTomTat] [nvarchar](255) NULL,
	[SoTrang] [int] NULL,
	[DangMuon] [int] NULL,
	[MatOrHu] [int] NULL,
	[ISBN] [nchar](13) NULL,
 CONSTRAINT [PK_QuyenSach] PRIMARY KEY CLUSTERED 
(
	[MaSach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TaiKhoan]    Script Date: 1/11/2017 8:06:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaiKhoan](
	[TaiKhoan] [nvarchar](50) NOT NULL,
	[MatKhau] [nvarchar](36) NOT NULL,
	[MSNVOF] [nvarchar](20) NOT NULL,
	[Cap] [int] NOT NULL,
 CONSTRAINT [PK_TaiKhoan_1] PRIMARY KEY CLUSTERED 
(
	[TaiKhoan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0', N'Khánh Huy', N'025502265', N'459/2', CAST(N'2017-04-01 00:00:00.000' AS DateTime), N'Male', CAST(N'2017-01-01 00:00:00.000' AS DateTime), 0, 2000, 0, 0, 0)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG1', N'Mều', N'261351478', N'BT', CAST(N'2017-04-01 00:00:00.000' AS DateTime), N'Female', CAST(N'2017-01-01 00:00:00.000' AS DateTime), 1, 2000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG2', N'Trịnh Mark', N'025516626', N'Vòng Xoay', CAST(N'2017-04-01 00:00:00.000' AS DateTime), N'Male', CAST(N'2017-01-01 00:00:00.000' AS DateTime), 1, 2000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG3', N'Danh', N'062254485', N'135 B', CAST(N'2017-04-01 00:00:00.000' AS DateTime), N'Male', CAST(N'2017-01-01 00:00:00.000' AS DateTime), 1, 2000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG4', N'trọng', N'6255841125', N'okboy', CAST(N'2017-04-01 00:00:00.000' AS DateTime), N'', CAST(N'2017-01-01 00:00:00.000' AS DateTime), 0, 2000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG5', N'TeaPlus', N'025513368', N'THP', CAST(N'2017-01-04 00:00:00.000' AS DateTime), N'Male', CAST(N'2017-04-04 00:00:00.000' AS DateTime), 1, 2000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG6', N'asd', N'asd', N'asdasd', CAST(N'2017-01-04 00:00:00.000' AS DateTime), N'Male', CAST(N'2017-04-04 00:00:00.000' AS DateTime), 0, 1000, 0, 0, 1)
INSERT [dbo].[LoaiDocGia] ([ID], [Role], [Phi], [SoSachMuonDc], [TaiLieuDB], [SoNgayDcMuon]) VALUES (0, N'Normal User', 1000, 3, 0, 7)
INSERT [dbo].[LoaiDocGia] ([ID], [Role], [Phi], [SoSachMuonDc], [TaiLieuDB], [SoNgayDcMuon]) VALUES (1, N'V.I.P User', 2000, 5, 1, 14)
INSERT [dbo].[LoaiSach] ([MaLoaiSach], [LoaiSach], [ThongTin]) VALUES (1, N'MalOaiSach', NULL)
INSERT [dbo].[NhanVien] ([MSNV], [HoTenNV], [NgaySinhNV], [GioiTinhNV], [DienThoai]) VALUES (N'NV1', N'Tur', NULL, N'Male', NULL)
INSERT [dbo].[NhanVien] ([MSNV], [HoTenNV], [NgaySinhNV], [GioiTinhNV], [DienThoai]) VALUES (N'NV2', N'Danh', NULL, N'Fmale', NULL)
INSERT [dbo].[PhieuMuonTra] ([MaPhieu], [MSDG], [MSNVChoMuon], [MSNVNhanTra], [TongSoMuon], [DaTra], [Phat]) VALUES (N'PX1', N'DG0', N'NV1', N'NV2', 1, 1, 0)
INSERT [dbo].[PhieuMuonTra] ([MaPhieu], [MSDG], [MSNVChoMuon], [MSNVNhanTra], [TongSoMuon], [DaTra], [Phat]) VALUES (N'PX2', N'DG0', N'NV1', N'NV2', 1, 0, 0)
INSERT [dbo].[QuyenSach] ([MaSach], [TenSach], [TacGia], [NXB], [MaLoaiSach], [NamXB], [LanXB], [SoLuong], [NoiDungTomTat], [SoTrang], [DangMuon], [MatOrHu], [ISBN]) VALUES (N'BOOK0001', N'Tsubasa', N'Clamp', N'Japan', 1, 2003, 1, 1, NULL, 69, 0, 0, N'8965753415313')
INSERT [dbo].[QuyenSach] ([MaSach], [TenSach], [TacGia], [NXB], [MaLoaiSach], [NamXB], [LanXB], [SoLuong], [NoiDungTomTat], [SoTrang], [DangMuon], [MatOrHu], [ISBN]) VALUES (N'BOOK0002', N'Naruto', N'JAP', N'Jap', 1, 1996, 4, 20, N'Nói về anh chàng Naruto', 78, 0, 0, N'8564157985123')
INSERT [dbo].[QuyenSach] ([MaSach], [TenSach], [TacGia], [NXB], [MaLoaiSach], [NamXB], [LanXB], [SoLuong], [NoiDungTomTat], [SoTrang], [DangMuon], [MatOrHu], [ISBN]) VALUES (N'BOOK0003', N'Naruto-Advan', N'JAP', N'Jap', 1, 1996, 4, 100, N'Nói v? anh chàng Naruto', 78, 0, 0, N'8564157985123')
INSERT [dbo].[TaiKhoan] ([TaiKhoan], [MatKhau], [MSNVOF], [Cap]) VALUES (N'1460408', N'ruadén96', N'NV1', 0)
ALTER TABLE [dbo].[ChiTietPhieu]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhieuMuon_MuonTra] FOREIGN KEY([MaPhieu])
REFERENCES [dbo].[PhieuMuonTra] ([MaPhieu])
GO
ALTER TABLE [dbo].[ChiTietPhieu] CHECK CONSTRAINT [FK_ChiTietPhieuMuon_MuonTra]
GO
ALTER TABLE [dbo].[ChiTietPhieu]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhieuMuon_QuyenSach] FOREIGN KEY([MaSach])
REFERENCES [dbo].[QuyenSach] ([MaSach])
GO
ALTER TABLE [dbo].[ChiTietPhieu] CHECK CONSTRAINT [FK_ChiTietPhieuMuon_QuyenSach]
GO
ALTER TABLE [dbo].[DocGia]  WITH CHECK ADD  CONSTRAINT [FK_DocGia_LoaiDocGia] FOREIGN KEY([LoaiDocGia])
REFERENCES [dbo].[LoaiDocGia] ([ID])
GO
ALTER TABLE [dbo].[DocGia] CHECK CONSTRAINT [FK_DocGia_LoaiDocGia]
GO
ALTER TABLE [dbo].[HoaDon]  WITH CHECK ADD  CONSTRAINT [FK_HoaDon_DocGia] FOREIGN KEY([MaDG])
REFERENCES [dbo].[DocGia] ([MADG])
GO
ALTER TABLE [dbo].[HoaDon] CHECK CONSTRAINT [FK_HoaDon_DocGia]
GO
ALTER TABLE [dbo].[PhieuMuonTra]  WITH CHECK ADD  CONSTRAINT [FK_MuonTra_DocGia] FOREIGN KEY([MSDG])
REFERENCES [dbo].[DocGia] ([MADG])
GO
ALTER TABLE [dbo].[PhieuMuonTra] CHECK CONSTRAINT [FK_MuonTra_DocGia]
GO
ALTER TABLE [dbo].[PhieuMuonTra]  WITH CHECK ADD  CONSTRAINT [FK_MuonTra_NhanVien] FOREIGN KEY([MSNVChoMuon])
REFERENCES [dbo].[NhanVien] ([MSNV])
GO
ALTER TABLE [dbo].[PhieuMuonTra] CHECK CONSTRAINT [FK_MuonTra_NhanVien]
GO
ALTER TABLE [dbo].[PhieuMuonTra]  WITH CHECK ADD  CONSTRAINT [FK_MuonTra_NhanVien1] FOREIGN KEY([MSNVNhanTra])
REFERENCES [dbo].[NhanVien] ([MSNV])
GO
ALTER TABLE [dbo].[PhieuMuonTra] CHECK CONSTRAINT [FK_MuonTra_NhanVien1]
GO
ALTER TABLE [dbo].[QuyenSach]  WITH CHECK ADD  CONSTRAINT [FK_QuyenSach_LoaiSach] FOREIGN KEY([MaLoaiSach])
REFERENCES [dbo].[LoaiSach] ([MaLoaiSach])
GO
ALTER TABLE [dbo].[QuyenSach] CHECK CONSTRAINT [FK_QuyenSach_LoaiSach]
GO
ALTER TABLE [dbo].[TaiKhoan]  WITH CHECK ADD  CONSTRAINT [FK_TaiKhoan_NhanVien] FOREIGN KEY([MSNVOF])
REFERENCES [dbo].[NhanVien] ([MSNV])
GO
ALTER TABLE [dbo].[TaiKhoan] CHECK CONSTRAINT [FK_TaiKhoan_NhanVien]
GO
USE [master]
GO
ALTER DATABASE [UDQLThuVien] SET  READ_WRITE 
GO
