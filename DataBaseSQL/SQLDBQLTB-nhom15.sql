USE [master]
GO
/****** Object:  Database [UDQLThuVien]    Script Date: 1/14/2017 9:57:51 PM ******/
CREATE DATABASE [UDQLThuVien]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'UDQLThuVien', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\UDQLThuVien.mdf' , SIZE = 7168KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'UDQLThuVien_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\UDQLThuVien_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [UDQLThuVien] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [UDQLThuVien].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [UDQLThuVien] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [UDQLThuVien] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [UDQLThuVien] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [UDQLThuVien] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [UDQLThuVien] SET ARITHABORT OFF 
GO
ALTER DATABASE [UDQLThuVien] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [UDQLThuVien] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [UDQLThuVien] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [UDQLThuVien] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [UDQLThuVien] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [UDQLThuVien] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [UDQLThuVien] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [UDQLThuVien] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [UDQLThuVien] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [UDQLThuVien] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [UDQLThuVien] SET  DISABLE_BROKER 
GO
ALTER DATABASE [UDQLThuVien] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [UDQLThuVien] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [UDQLThuVien] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [UDQLThuVien] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [UDQLThuVien] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [UDQLThuVien] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [UDQLThuVien] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [UDQLThuVien] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [UDQLThuVien] SET  MULTI_USER 
GO
ALTER DATABASE [UDQLThuVien] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [UDQLThuVien] SET DB_CHAINING OFF 
GO
ALTER DATABASE [UDQLThuVien] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [UDQLThuVien] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [UDQLThuVien]
GO
/****** Object:  Table [dbo].[ChiTietPhieu]    Script Date: 1/14/2017 9:57:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietPhieu](
	[MaPhieu] [nvarchar](15) NOT NULL,
	[MaSach] [nvarchar](20) NOT NULL,
	[Soluong] [int] NULL,
	[NgayMuon] [datetime] NULL,
	[HanTra] [datetime] NULL,
	[NgayTra] [datetime] NULL,
	[SoNgayTre] [int] NULL,
	[GhiChu] [nvarchar](200) NULL,
	[DaTra] [bit] NULL,
	[DaMat] [bit] NULL,
 CONSTRAINT [PK_ChiTietPhieuMuon_1] PRIMARY KEY CLUSTERED 
(
	[MaPhieu] ASC,
	[MaSach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DocGia]    Script Date: 1/14/2017 9:57:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocGia](
	[MADG] [nvarchar](10) NOT NULL,
	[TenDG] [nvarchar](50) NOT NULL,
	[CMND] [nvarchar](20) NOT NULL,
	[DiaChi] [nvarchar](100) NOT NULL,
	[NgaySinh] [datetime] NULL,
	[GioiTinh] [nvarchar](10) NOT NULL,
	[HanSuDung] [datetime] NULL,
	[LoaiDocGia] [int] NULL,
	[Fee] [int] NULL,
	[SachQuaHan] [int] NULL,
	[SoSachDangMuon] [int] NULL,
	[TinhTrang] [bit] NOT NULL,
 CONSTRAINT [PK_DocGia_1] PRIMARY KEY CLUSTERED 
(
	[MADG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HoaDon]    Script Date: 1/14/2017 9:57:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HoaDon](
	[IDHoaDon] [nchar](10) NOT NULL,
	[MaDG] [nvarchar](10) NOT NULL,
	[HanThe] [datetime] NULL,
	[TenDG] [nvarchar](50) NULL,
	[Phi] [int] NOT NULL,
 CONSTRAINT [PK_HoaDon] PRIMARY KEY CLUSTERED 
(
	[IDHoaDon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiDocGia]    Script Date: 1/14/2017 9:57:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiDocGia](
	[ID] [int] NOT NULL,
	[Role] [nvarchar](50) NOT NULL,
	[Phi] [int] NOT NULL,
	[SoSachMuonDc] [int] NOT NULL,
	[TaiLieuDB] [bit] NOT NULL,
	[SoNgayDcMuon] [int] NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiSach]    Script Date: 1/14/2017 9:57:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiSach](
	[MaLoaiSach] [int] NOT NULL,
	[LoaiSach] [nvarchar](50) NULL,
	[ThongTin] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoaiSach] PRIMARY KEY CLUSTERED 
(
	[MaLoaiSach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhanVien]    Script Date: 1/14/2017 9:57:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhanVien](
	[MSNV] [nvarchar](20) NOT NULL,
	[HoTenNV] [nvarchar](50) NOT NULL,
	[NgaySinhNV] [datetime] NULL,
	[GioiTinhNV] [nvarchar](10) NULL,
	[DienThoai] [nvarchar](11) NULL,
 CONSTRAINT [PK_NhanVien] PRIMARY KEY CLUSTERED 
(
	[MSNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PhieuMuonTra]    Script Date: 1/14/2017 9:57:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhieuMuonTra](
	[MaPhieu] [nvarchar](15) NOT NULL,
	[MSDG] [nvarchar](10) NOT NULL,
	[MSNVChoMuon] [nvarchar](20) NULL,
	[MSNVNhanTra] [nvarchar](20) NULL,
	[TongSoMuon] [int] NOT NULL,
	[DaTra] [int] NOT NULL,
	[Phat] [int] NULL,
 CONSTRAINT [PK_MuonSach] PRIMARY KEY CLUSTERED 
(
	[MaPhieu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuyenSach]    Script Date: 1/14/2017 9:57:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuyenSach](
	[MaSach] [nvarchar](20) NOT NULL,
	[TenSach] [nvarchar](100) NULL,
	[TacGia] [nvarchar](50) NULL,
	[NXB] [nvarchar](50) NULL,
	[MaLoaiSach] [int] NULL,
	[NamXB] [int] NULL,
	[LanXB] [int] NULL,
	[SoLuong] [int] NULL,
	[NoiDungTomTat] [nvarchar](500) NULL,
	[SoTrang] [int] NULL,
	[DangMuon] [int] NULL,
	[MatOrHu] [int] NULL,
	[ISBN] [nchar](13) NULL,
 CONSTRAINT [PK_QuyenSach] PRIMARY KEY CLUSTERED 
(
	[MaSach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TaiKhoan]    Script Date: 1/14/2017 9:57:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaiKhoan](
	[TaiKhoan] [nvarchar](50) NOT NULL,
	[MatKhau] [nvarchar](max) NOT NULL,
	[MSNVOF] [nvarchar](20) NOT NULL,
	[Cap] [int] NOT NULL,
 CONSTRAINT [PK_TaiKhoan_1] PRIMARY KEY CLUSTERED 
(
	[TaiKhoan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0000', N'Khánh Huy', N'025502265', N'459/2', CAST(N'2017-04-01 00:00:00.000' AS DateTime), N'Male', CAST(N'2017-01-01 00:00:00.000' AS DateTime), 0, 2000, 0, 0, 0)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0001', N'Mều', N'261351478', N'BT', CAST(N'2017-04-01 00:00:00.000' AS DateTime), N'Female', CAST(N'2017-01-01 00:00:00.000' AS DateTime), 1, 2000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0002', N'Trịnh Mark', N'025516626', N'Vòng Xoay', CAST(N'2017-04-01 00:00:00.000' AS DateTime), N'Male', CAST(N'2017-01-01 00:00:00.000' AS DateTime), 1, 2000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0003', N'Danh', N'062254485', N'135 B', CAST(N'2017-04-01 00:00:00.000' AS DateTime), N'Male', CAST(N'2017-01-01 00:00:00.000' AS DateTime), 1, 2000, 0, 0, 0)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0004', N'trọng', N'6255841125', N'okboy', CAST(N'2017-04-01 00:00:00.000' AS DateTime), N'', CAST(N'2017-01-01 00:00:00.000' AS DateTime), 0, 2000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0005', N'TeaPlus', N'025513368', N'THP', CAST(N'2017-01-04 00:00:00.000' AS DateTime), N'Male', CAST(N'2017-04-04 00:00:00.000' AS DateTime), 1, 2000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0006', N'asd', N'asd', N'asdasd', CAST(N'2017-01-04 00:00:00.000' AS DateTime), N'Male', CAST(N'2017-04-04 00:00:00.000' AS DateTime), 0, 1000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0007', N'Rùa', N'02551258', N'459/2t rj', CAST(N'1996-04-01 00:00:00.000' AS DateTime), N'Nam', CAST(N'2017-04-13 00:00:00.000' AS DateTime), 0, 1000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0008', N'asd', N'025515529', N'qwewqe', CAST(N'1996-01-13 00:00:00.000' AS DateTime), N'Nữ', CAST(N'2017-04-13 00:00:00.000' AS DateTime), 1, 2000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0009', N'asdasd', N'054655132', N'dasd', CAST(N'1996-01-13 00:00:00.000' AS DateTime), N'Nam', CAST(N'2017-04-13 00:00:00.000' AS DateTime), 0, 1000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0010', N'asdasd', N'asdsad', N'asdasd', CAST(N'1996-01-13 00:00:00.000' AS DateTime), N'Nam', CAST(N'2017-04-13 00:00:00.000' AS DateTime), 1, 2000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0011', N'eqweqw', N'qweqwe', N'eqwewq', CAST(N'1996-01-13 00:00:00.000' AS DateTime), N'Nam', CAST(N'2017-04-13 00:00:00.000' AS DateTime), 0, 1000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0012', N'dasdas', N'asdasd', N'asdasd', CAST(N'1996-01-13 00:00:00.000' AS DateTime), N'Nam', CAST(N'2017-04-13 00:00:00.000' AS DateTime), 1, 2000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0013', N'asdasd', N'thu2', N'asdasd', CAST(N'1996-01-13 00:00:00.000' AS DateTime), N'Nam', CAST(N'2017-04-13 00:00:00.000' AS DateTime), 0, 1000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0014', N'asd', N'qweqwe', N'dasd', CAST(N'1996-01-13 00:00:00.000' AS DateTime), N'Nam', CAST(N'2017-04-13 00:00:00.000' AS DateTime), 0, 1000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0015', N'asdasd', N'asd', N'dasd', CAST(N'1996-01-13 00:00:00.000' AS DateTime), N'Nam', CAST(N'2017-04-13 00:00:00.000' AS DateTime), 0, 1000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0016', N'weqwe', N'asdas', N'eqwe', CAST(N'1996-01-13 00:00:00.000' AS DateTime), N'Nam', CAST(N'2017-04-13 00:00:00.000' AS DateTime), 0, 1000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0017', N'Sol', N'asdas', N'Earth', CAST(N'1995-01-13 00:00:00.000' AS DateTime), N'Nam', CAST(N'2017-04-13 00:00:00.000' AS DateTime), 1, 2000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0018', N'qweqwe', N'sdsada', N'qweqwe', CAST(N'1996-01-13 00:00:00.000' AS DateTime), N'Nam', CAST(N'2017-04-13 00:00:00.000' AS DateTime), 0, 1000, 0, 0, 1)
INSERT [dbo].[DocGia] ([MADG], [TenDG], [CMND], [DiaChi], [NgaySinh], [GioiTinh], [HanSuDung], [LoaiDocGia], [Fee], [SachQuaHan], [SoSachDangMuon], [TinhTrang]) VALUES (N'DG0019', N'asdasd', N'asdasdasdasdasd', N'asdasd', CAST(N'1996-01-13 00:00:00.000' AS DateTime), N'Nam', CAST(N'2017-04-13 00:00:00.000' AS DateTime), 0, 1000, 0, 0, 1)
INSERT [dbo].[HoaDon] ([IDHoaDon], [MaDG], [HanThe], [TenDG], [Phi]) VALUES (N'HD0000    ', N'DG0001', CAST(N'2017-01-01 00:00:00.000' AS DateTime), N'Mều', 2000)
INSERT [dbo].[HoaDon] ([IDHoaDon], [MaDG], [HanThe], [TenDG], [Phi]) VALUES (N'HD0001    ', N'DG0001', CAST(N'2017-01-04 00:00:00.000' AS DateTime), N'Mều', 2000)
INSERT [dbo].[HoaDon] ([IDHoaDon], [MaDG], [HanThe], [TenDG], [Phi]) VALUES (N'HD0002    ', N'DG0009', CAST(N'1996-01-13 00:00:00.000' AS DateTime), N'asdasd', 1000)
INSERT [dbo].[HoaDon] ([IDHoaDon], [MaDG], [HanThe], [TenDG], [Phi]) VALUES (N'HD0003    ', N'DG0001', CAST(N'2017-04-01 00:00:00.000' AS DateTime), N'Mều', 2000)
INSERT [dbo].[HoaDon] ([IDHoaDon], [MaDG], [HanThe], [TenDG], [Phi]) VALUES (N'HD0004    ', N'DG0003', CAST(N'2017-04-01 00:00:00.000' AS DateTime), N'Danh', 2000)
INSERT [dbo].[LoaiDocGia] ([ID], [Role], [Phi], [SoSachMuonDc], [TaiLieuDB], [SoNgayDcMuon]) VALUES (0, N'Normal User', 1000, 3, 0, 7)
INSERT [dbo].[LoaiDocGia] ([ID], [Role], [Phi], [SoSachMuonDc], [TaiLieuDB], [SoNgayDcMuon]) VALUES (1, N'V.I.P User', 2000, 5, 1, 14)
INSERT [dbo].[LoaiSach] ([MaLoaiSach], [LoaiSach], [ThongTin]) VALUES (0, N'test thử', N'asdsad')
INSERT [dbo].[LoaiSach] ([MaLoaiSach], [LoaiSach], [ThongTin]) VALUES (1, N'MalOaiSach', NULL)
INSERT [dbo].[LoaiSach] ([MaLoaiSach], [LoaiSach], [ThongTin]) VALUES (2, N'Thông Tin', N'dành cho công nghệ thông tin')
INSERT [dbo].[LoaiSach] ([MaLoaiSach], [LoaiSach], [ThongTin]) VALUES (3, N'Phieu luu', N'ranh roi di long nhong')
INSERT [dbo].[LoaiSach] ([MaLoaiSach], [LoaiSach], [ThongTin]) VALUES (4, N'', NULL)
INSERT [dbo].[NhanVien] ([MSNV], [HoTenNV], [NgaySinhNV], [GioiTinhNV], [DienThoai]) VALUES (N'NV1', N'Tur', NULL, N'Male', NULL)
INSERT [dbo].[NhanVien] ([MSNV], [HoTenNV], [NgaySinhNV], [GioiTinhNV], [DienThoai]) VALUES (N'NV2', N'Danh', NULL, N'Fmale', NULL)
INSERT [dbo].[NhanVien] ([MSNV], [HoTenNV], [NgaySinhNV], [GioiTinhNV], [DienThoai]) VALUES (N'NV3', N'Rùa', NULL, N'Nam', NULL)
INSERT [dbo].[PhieuMuonTra] ([MaPhieu], [MSDG], [MSNVChoMuon], [MSNVNhanTra], [TongSoMuon], [DaTra], [Phat]) VALUES (N'PMT0000', N'DG0001', N'NV1', NULL, 0, 0, 0)
INSERT [dbo].[QuyenSach] ([MaSach], [TenSach], [TacGia], [NXB], [MaLoaiSach], [NamXB], [LanXB], [SoLuong], [NoiDungTomTat], [SoTrang], [DangMuon], [MatOrHu], [ISBN]) VALUES (N'BOOK0001', N'sdasd', N'dasd', N'das', 0, 1996, 123, 9999, N'viết lại derc', 9999, 0, 0, N'123435       ')
INSERT [dbo].[QuyenSach] ([MaSach], [TenSach], [TacGia], [NXB], [MaLoaiSach], [NamXB], [LanXB], [SoLuong], [NoiDungTomTat], [SoTrang], [DangMuon], [MatOrHu], [ISBN]) VALUES (N'BOOK0002', N'Naruto-Advan', N'JAP', N'Jap', 1, 1996, 4, 100, N'thử sửa lại', 78, 0, 0, N'8564157985123')
INSERT [dbo].[QuyenSach] ([MaSach], [TenSach], [TacGia], [NXB], [MaLoaiSach], [NamXB], [LanXB], [SoLuong], [NoiDungTomTat], [SoTrang], [DangMuon], [MatOrHu], [ISBN]) VALUES (N'BOOK0003', N'asdasd', N'dasd', N'dasd', 1, 1996, 213, 123, N'asdsad', 123, 0, 0, N'123123123    ')
INSERT [dbo].[QuyenSach] ([MaSach], [TenSach], [TacGia], [NXB], [MaLoaiSach], [NamXB], [LanXB], [SoLuong], [NoiDungTomTat], [SoTrang], [DangMuon], [MatOrHu], [ISBN]) VALUES (N'BOOK0004', N'asdasd', N'sadasd', N'asdsad', 0, 123, 123, 123, N'asd', 123, 0, 0, N'12354        ')
INSERT [dbo].[QuyenSach] ([MaSach], [TenSach], [TacGia], [NXB], [MaLoaiSach], [NamXB], [LanXB], [SoLuong], [NoiDungTomTat], [SoTrang], [DangMuon], [MatOrHu], [ISBN]) VALUES (N'BOOK0005', N'jav', N'tokuda', N'tokyo hot', 2, 2016, 0, 1000, N'chịch nện thông blablabla', 1, 0, 0, N'56248932458  ')
INSERT [dbo].[QuyenSach] ([MaSach], [TenSach], [TacGia], [NXB], [MaLoaiSach], [NamXB], [LanXB], [SoLuong], [NoiDungTomTat], [SoTrang], [DangMuon], [MatOrHu], [ISBN]) VALUES (N'BOOK0006', N'Naruto-1', N'JAP', N'Jap', 1, 1996, 4, 100, N'Nói về anh chàng siêu nhân', 78, 0, 0, N'856415756253 ')
INSERT [dbo].[QuyenSach] ([MaSach], [TenSach], [TacGia], [NXB], [MaLoaiSach], [NamXB], [LanXB], [SoLuong], [NoiDungTomTat], [SoTrang], [DangMuon], [MatOrHu], [ISBN]) VALUES (N'BOOK0007', N'Naruto-Advan3', N'JAP', N'Jap', 1, 1996, 4, 100, N'Nói về anh chàng Naruto', 78, 0, 0, N'85645985123  ')
INSERT [dbo].[QuyenSach] ([MaSach], [TenSach], [TacGia], [NXB], [MaLoaiSach], [NamXB], [LanXB], [SoLuong], [NoiDungTomTat], [SoTrang], [DangMuon], [MatOrHu], [ISBN]) VALUES (N'BOOK0008', N'kamentai', N'JAV', N'Jap', 1, 1996, 4, 100, N'Nói asdo', 78, 0, 0, N'8564157985123')
INSERT [dbo].[QuyenSach] ([MaSach], [TenSach], [TacGia], [NXB], [MaLoaiSach], [NamXB], [LanXB], [SoLuong], [NoiDungTomTat], [SoTrang], [DangMuon], [MatOrHu], [ISBN]) VALUES (N'BOOK0009', N'echio', N'JAP', N'Jap', 1, 1996, 4, 100, N'Nói thử xài decro', 78, 0, 0, N'85634345123  ')
INSERT [dbo].[QuyenSach] ([MaSach], [TenSach], [TacGia], [NXB], [MaLoaiSach], [NamXB], [LanXB], [SoLuong], [NoiDungTomTat], [SoTrang], [DangMuon], [MatOrHu], [ISBN]) VALUES (N'BOOK0010', N'biconobocu', N'JAP', N'Jap', 1, 1996, 4, 100, N'hentai', 78, 0, 0, N'8564157985123')
INSERT [dbo].[QuyenSach] ([MaSach], [TenSach], [TacGia], [NXB], [MaLoaiSach], [NamXB], [LanXB], [SoLuong], [NoiDungTomTat], [SoTrang], [DangMuon], [MatOrHu], [ISBN]) VALUES (N'BOOK0011', N'macxibocu', N'Eng', N'Jap', 1, 1996, 4, 100, N'Nói về anh chàng Naruto', 78, 0, 0, N'8564157985123')
INSERT [dbo].[TaiKhoan] ([TaiKhoan], [MatKhau], [MSNVOF], [Cap]) VALUES (N'123', N'asd', N'NV2', 0)
INSERT [dbo].[TaiKhoan] ([TaiKhoan], [MatKhau], [MSNVOF], [Cap]) VALUES (N'1460408', N'ruaden', N'NV1', 1)
INSERT [dbo].[TaiKhoan] ([TaiKhoan], [MatKhau], [MSNVOF], [Cap]) VALUES (N'ruaden', N'J2Rf5vbhoS8mAQ6m0IChAUSqnA092k1SxvFfXzHRl3WaB+jBZy9gppGGvikLn0hWDsoWcNysBWodqtIUNAMJcA==', N'NV3', 1)
ALTER TABLE [dbo].[ChiTietPhieu]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhieuMuon_MuonTra] FOREIGN KEY([MaPhieu])
REFERENCES [dbo].[PhieuMuonTra] ([MaPhieu])
GO
ALTER TABLE [dbo].[ChiTietPhieu] CHECK CONSTRAINT [FK_ChiTietPhieuMuon_MuonTra]
GO
ALTER TABLE [dbo].[ChiTietPhieu]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietPhieuMuon_QuyenSach] FOREIGN KEY([MaSach])
REFERENCES [dbo].[QuyenSach] ([MaSach])
GO
ALTER TABLE [dbo].[ChiTietPhieu] CHECK CONSTRAINT [FK_ChiTietPhieuMuon_QuyenSach]
GO
ALTER TABLE [dbo].[DocGia]  WITH CHECK ADD  CONSTRAINT [FK_DocGia_LoaiDocGia] FOREIGN KEY([LoaiDocGia])
REFERENCES [dbo].[LoaiDocGia] ([ID])
GO
ALTER TABLE [dbo].[DocGia] CHECK CONSTRAINT [FK_DocGia_LoaiDocGia]
GO
ALTER TABLE [dbo].[HoaDon]  WITH CHECK ADD  CONSTRAINT [FK_HoaDon_DocGia] FOREIGN KEY([MaDG])
REFERENCES [dbo].[DocGia] ([MADG])
GO
ALTER TABLE [dbo].[HoaDon] CHECK CONSTRAINT [FK_HoaDon_DocGia]
GO
ALTER TABLE [dbo].[PhieuMuonTra]  WITH CHECK ADD  CONSTRAINT [FK_MuonTra_DocGia] FOREIGN KEY([MSDG])
REFERENCES [dbo].[DocGia] ([MADG])
GO
ALTER TABLE [dbo].[PhieuMuonTra] CHECK CONSTRAINT [FK_MuonTra_DocGia]
GO
ALTER TABLE [dbo].[PhieuMuonTra]  WITH CHECK ADD  CONSTRAINT [FK_MuonTra_NhanVien] FOREIGN KEY([MSNVChoMuon])
REFERENCES [dbo].[NhanVien] ([MSNV])
GO
ALTER TABLE [dbo].[PhieuMuonTra] CHECK CONSTRAINT [FK_MuonTra_NhanVien]
GO
ALTER TABLE [dbo].[PhieuMuonTra]  WITH CHECK ADD  CONSTRAINT [FK_MuonTra_NhanVien1] FOREIGN KEY([MSNVNhanTra])
REFERENCES [dbo].[NhanVien] ([MSNV])
GO
ALTER TABLE [dbo].[PhieuMuonTra] CHECK CONSTRAINT [FK_MuonTra_NhanVien1]
GO
ALTER TABLE [dbo].[QuyenSach]  WITH CHECK ADD  CONSTRAINT [FK_QuyenSach_LoaiSach] FOREIGN KEY([MaLoaiSach])
REFERENCES [dbo].[LoaiSach] ([MaLoaiSach])
GO
ALTER TABLE [dbo].[QuyenSach] CHECK CONSTRAINT [FK_QuyenSach_LoaiSach]
GO
ALTER TABLE [dbo].[TaiKhoan]  WITH CHECK ADD  CONSTRAINT [FK_TaiKhoan_NhanVien] FOREIGN KEY([MSNVOF])
REFERENCES [dbo].[NhanVien] ([MSNV])
GO
ALTER TABLE [dbo].[TaiKhoan] CHECK CONSTRAINT [FK_TaiKhoan_NhanVien]
GO
USE [master]
GO
ALTER DATABASE [UDQLThuVien] SET  READ_WRITE 
GO
