﻿Imports DAO
Imports DTO
Imports System.Text
Imports System.Security.Cryptography

Public Class NhanVienBUS
    Public Function getAll()
        Dim nvDAO As New NhanVienDAO()
        Return nvDAO.getAll()
    End Function
    Public Function insert(ByVal nv As NhanVienDTO)
        Dim nvDAO As New NhanVienDAO()
        Return nvDAO.insert(nv)
    End Function
    Public Function update(ByVal nv As NhanVienDTO)
        Dim nvDAO As New NhanVienDAO()
        Return nvDAO.update(nv)
    End Function
    Public Function Delete(ByVal nv As NhanVienDTO)
        Dim nvDAO As New NhanVienDAO()
        Return nvDAO.delete(nv)
    End Function
    Public Function getByID(ByVal Id As String)
        Dim nvDAO As New NhanVienDAO()
        Return nvDAO.getById(Id)
    End Function
    Public Function getByName(ByVal Name As String)
        Dim nvDAO As New NhanVienDAO()
        Return nvDAO.getByName(Name)
    End Function
    Public Function getSex(ByVal ID As String)
        Dim nvDAO As New NhanVienDAO()
        Return nvDAO.getSex(ID)
    End Function

    Public Function searchByID(ByVal Id As String)
        Dim nvDAo As New NhanVienDAO()
        Return nvDAo.searchById(Id)
    End Function

    Public Function searchByPhone(ByVal sdt As String)
        Dim nvDAO As New NhanVienDAO()
        Return nvDAO.searchBySDT(sdt)
    End Function

    Public Function searchBySex(ByVal sex As String)
        Dim nvDAO As New NhanVienDAO()
        Return nvDAO.searchBySex(sex)
    End Function

End Class
