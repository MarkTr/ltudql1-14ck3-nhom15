﻿Imports DAO
Imports DTO
Public Class SachBUS
    Public Shared Function LoadBook(ByVal i As Integer, ByVal par As String) As DataTable

        Return SachDAO.LoadSach(i, par)
    End Function
    Public Shared Sub ThemSach(ByVal Sach As SachDTO)
        Try
            If SachDAO.ThemSach(Sach) Then
                MessageBox.Show("Thêm Sách Thành Công!!!", "Quản Lý Sách", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show("Không thể thêm sách!!! Lỗi" + ex.Message, "Quản Lý Sách", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Public Shared Function LayMaSach() As String
        Return SachDAO.LoadMaSach()
    End Function
    Public Shared Function loadcbb() As DataTable
        Return SachDAO.Loadcombobox
    End Function
    Public Shared Sub xoaSach(ByVal masach As String)
        Try
            If SachDAO.XoaSach(masach) Then
                MessageBox.Show("Xóa Sách Thành Công!!!", "Quản Lý Sách", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show("Không thể xóa sách!!! Lỗi " + ex.Message, "Quản Lý Sách", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Public Shared Sub CapNhatSach(ByVal sach As SachDTO)
        Try
            If SachDAO.CapNhatSach(sach) Then
                MessageBox.Show("Cập nhật Sách Thành Công!!!", "Quản Lý Sách", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show("Không thể cập nhật sách!!! Lỗi " + ex.Message, "Quản Lý Sách", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Public Shared Function Load1Sach(ByVal str As String) As SachDTO

        Return SachDAO.Load1Sach(str)
    End Function
End Class
