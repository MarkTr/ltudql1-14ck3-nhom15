﻿Imports DAO
Imports DTO
Imports System.Text
Imports System.Security.Cryptography

Public Class TaiKhoanBUS

    Public Function insert(ByVal tk As TaiKhoanDTO)
        Dim tkDAO As New TaiKhoanDAO()
        Using md5Hash As MD5 = MD5.Create()
            Dim hash As String = EncryptPassword(tk.MatKhau, "TMCVTDNHKHPTT")
            tk.MatKhau = hash
        End Using
        Return tkDAO.insert(tk)
    End Function
    Public Function CreateRandomSalt() As String
        Const mix As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+=][}{<>"
        Dim salt As String = ""
        Dim rnd As New Random
        Dim sb As New StringBuilder
        For i As Integer = 1 To 100
            Dim x As Integer = rnd.Next(0, mix.Length - 1)
            salt &= (mix.Substring(x, 1))
        Next
        Return salt
    End Function
    Public Function EncryptPassword(password As String, salt As String) As String
        Dim convertedToBytes As Byte() = Encoding.UTF8.GetBytes(password & salt)
        Dim hashType As HashAlgorithm = New SHA512Managed()
        Dim hashBytes As Byte() = hashType.ComputeHash(convertedToBytes)
        Dim hashedResult As String = Convert.ToBase64String(hashBytes)
        Return hashedResult
    End Function

    Private Function VerifyMd5Hash(ByVal md5Hash As MD5, ByVal input As String, ByVal hash As String) As Boolean
        Dim hashOfInput As String = EncryptPassword(input, "TMCVTDNHKHPTT")
        Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase
        If 0 = comparer.Compare(hashOfInput, hash) Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Function checkLogin(ByVal tk As String, ByVal mk As String)
        Dim tkDAO As New TaiKhoanDAO()
        Using md5Hash As MD5 = MD5.Create()
            Dim hash As String = EncryptPassword(mk, "TMCVTDNHKHPTT")
            Return tkDAO.checkLogin(tk, hash)
        End Using

    End Function

    Public Function getByMSNV(ByVal msnv As String)
        Dim tkDAO As New TaiKhoanDAO()
        Return tkDAO.getByMSNV(msnv)
    End Function

    Public Function Update(ByVal tk As TaiKhoanDTO)
        Using md5Hash As MD5 = MD5.Create()
            Dim has As String = EncryptPassword(tk.MatKhau, "TMCVTDNHKHPTT")
            tk.MatKhau = has
                Dim tkDAO As New TaiKhoanDAO()
            Return tkDAO.Update(tk)
        End Using
    End Function
    Public Function Delete(ByVal tk As String)
        Dim tkDAO As New TaiKhoanDAO()
        Return tkDAO.Delete(tk)
    End Function

    Public Function getByUsername(ByVal tk As String)
        Dim tkDAO As New TaiKhoanDAO()
        Return tkDAO.getByUserName(tk)
    End Function

    Public Function getAccountRole(ByVal tk As String)
        Dim tkDAO As New TaiKhoanDAO()
        Return tkDAO.getAccountRole(tk)
    End Function

    Public Function getAll()
        Dim tkDAO As New TaiKhoanDAO()
        Return tkDAO.getAll()
    End Function

    Public Function getByRole(ByVal role As Integer)
        Dim tkDAO As New TaiKhoanDAO()
        Return tkDAO.getByRole(role)
    End Function

    Public Function searchByUsername(ByVal UID As String)
        Dim tkDAO As New TaiKhoanDAO()
        Return tkDAO.searchByUserName(UID)
    End Function
    Public Function searchByMSNV(ByVal msnv As String)
        Dim tkDAO As New TaiKhoanDAO()
        Return tkDAO.searchByMSNV(msnv)
    End Function

    Public Function getMSNV(ByVal UID As String)
        Dim tkDAO As New TaiKhoanDAO()
        Return tkDAO.getMSNV(UID)
    End Function
    Public Function getUsername(ByVal MSNV As String)
        Dim tkDAO As New TaiKhoanDAO()
        Return tkDAO.getUsername(MSNV)
    End Function
End Class
