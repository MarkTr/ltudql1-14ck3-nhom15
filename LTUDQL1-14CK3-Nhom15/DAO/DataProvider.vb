﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Data
Imports DAO
Public Class DataProvider
    'Tạo sẵn chuỗi kết nối để tiện việc tạo truy vấn
    'Shared strConnect = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=QLTV-demo.mdb" ' sau khi từ bỏ Access -_- giữ lại làm kỷ niệm
    'Shared strConnect = "Data Source=NIRUKO\SQLEXPRESS;Initial Catalog=UDQLThuVien;Integrated Security=True"

    Public Shared conn As SqlConnection
    Public Shared tableAdapter As SqlDataAdapter
    Public Shared table As DataTable
    Shared strConnect As String = ConfigurationManager.ConnectionStrings("Conn").ConnectionString ' chuỗi strConnect lấy từ App.config
    Public Shared Function ConnectDB() As SqlConnection ' hàm tạo ra đề gọi connect nhiều lần :D
        ' tạo kết nối bằng chuỗi strConnect ở trên
        Try
            conn = New SqlConnection(strConnect)
            conn.Open()
            Return conn
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Shared Sub DisConnect(ByVal connec As SqlConnection) 'hàm đóng kết nối
        connec.Close()
    End Sub


    Public Shared Function LoaderFromDATA(ByVal Cmd As SqlCommand) As DataTable 'trả về 1 bảng table
        Try
            Cmd.Connection = ConnectDB()
            Cmd.CommandType = CommandType.Text
            'tạo table để hứng data
            table = New DataTable("table")
            tableAdapter = New SqlDataAdapter(Cmd)
            tableAdapter.Fill(table)
            'đóng connect
            DisConnect(Cmd.Connection)
            Return table
        Catch ex As Exception
            ' MessageBox.Show(ex.Message)
            Throw ex
        End Try





    End Function
    Public Shared Function Load1Query(ByVal Cmd As SqlCommand) As Object
        Cmd.Connection = ConnectDB()
        'scalar là trả thẳng về 1 giá trị

        Dim strS As Object = Cmd.ExecuteScalar()
        DisConnect(Cmd.Connection)
        Return strS
    End Function

    Shared Function RunNonQuery(ByVal cmd As SqlCommand) As Boolean ' hàm chạy thêm xóa sửa truyền vào 1 câu query trả về True False
        Try
            cmd.Connection = ConnectDB()
            cmd.ExecuteNonQuery()
            DisConnect(cmd.Connection)
            Return True
        Catch ex As Exception
            MessageBox.Show("lỗi câu câu query " + ex.Message)
            Return False
        End Try
    End Function
    Public Shared Function DoiChuoi(ByVal str As String) As Boolean ' hàm thay đổi chuỗi kết nối nếu như lúc chạy phần mềm có gặp trục trặc với chuỗi kết nối
        Try
            Dim Config As Configuration
            Config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
            Config.ConnectionStrings.ConnectionStrings("Conn").ConnectionString = str

            Dim path = Application.StartupPath.ToString() 'này ở trong thư mục debug/relase
            path = path.Substring(0, path.LastIndexOf("\")) ' cái này sẽ là dường dẫn ở thư mục gốc

            Config.SaveAs("App.config", ConfigurationSaveMode.Modified, True) 'lưu vào trong thư mục gốc
            ConfigurationManager.RefreshSection(Config.ConnectionStrings.SectionInformation.Name)

            '  Config.SaveAs(ConfigurationSaveMode.Modified, True) ' lưu vào debug/relase
            '  ConfigurationManager.RefreshSection(Config.ConnectionStrings.SectionInformation.Name)

            My.MySettings.Default.Reload()

            'my copy
            'Dim _config As Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)

            ''Note: VB wants to use ConnectionStrings "Index number", and not name!
            ''Turns out Index 0 is "LocalSqlServer", but Index 1 was our connection string "Name" we wanted to change
            ''You might need to play with this...

            ''_config.ConnectionStrings.ConnectionStrings(1).ConnectionString = strConn
            '_config.ConnectionStrings.ConnectionStrings("Conn").ConnectionString = str
            ''Save to file
            '_config.Save(ConfigurationSaveMode.Modified)
            ''force changes to take effect so that we can start using
            ''this new connection string immediately

            'ConfigurationManager.RefreshSection(_config.ConnectionStrings.SectionInformation.Name)
            'My.MySettings.Default.Reload()

            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return False
        End Try
    End Function
End Class
