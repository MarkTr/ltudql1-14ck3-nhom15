﻿Imports DTO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports DAO
Public Class DocGiaDAO

    Public Shared Function Insert_DocGia(ByVal reader As DocGiaDTO) As Boolean
        Dim Check As Boolean = False
        Dim strQ As String = "insert into DOCGIA values (@madg,@tendg,@cmnd,@diachi,@ngaysinh,@gioitinh,@hsd,@ID,@fee,@SsachQuaHan,@SSachDangMuon,@tinhtrang)"
        'Dim strQ = String.Format("insert into DOCGIA values ( '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')", Reader.MaDG, Reader.TenDG, Reader.CMND, Reader.DiaChi, Reader.NgaySinh, Reader.Email, Reader.GioiTinh, Reader.HSD, Reader.Role_ID, Reader.Fee)

        Dim cmd As New SqlCommand(strQ)
        cmd.CommandType = CommandType.Text
        Try
            cmd.Parameters.Add("@madg", SqlDbType.NVarChar).Value = reader.MaDG
            cmd.Parameters.Add("@tendg", SqlDbType.NVarChar).Value = reader.TenDG
            cmd.Parameters.Add("@cmnd", SqlDbType.NVarChar).Value = reader.CMND
            cmd.Parameters.Add("@diachi", SqlDbType.NVarChar).Value = reader.DiaChi
            cmd.Parameters.Add("@ngaysinh", SqlDbType.Date).Value = reader.NgaySinh
            cmd.Parameters.Add("@gioitinh", SqlDbType.NVarChar).Value = reader.GioiTinh
            cmd.Parameters.Add("@hsd", SqlDbType.Date).Value = reader.HSD
            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = reader.Role_ID
            cmd.Parameters.Add("@fee", SqlDbType.Int).Value = reader.Fee
            cmd.Parameters.Add("@SsachQuaHan", SqlDbType.Int).Value = 0
            cmd.Parameters.Add("@SSachDangMuon", SqlDbType.Int).Value = 0
            cmd.Parameters.Add("@tinhtrang", SqlDbType.Bit).Value = True
            If DataProvider.RunNonQuery(cmd) Then
                Check = True
            End If

        Catch ex As Exception
            Throw ex
            Check = False
        End Try
        Return Check
    End Function

    Public Shared Function Update_DocGia(ByVal reader As DocGiaDTO) As Boolean
        Dim Check As Boolean = False
        Dim strQ As String = "update DOCGIA set TenDG=@tendg, NgaySinh=@ngaysinh, LoaiDocGia=@ID, DiaChi=@diachi, GioiTinh=@gioitinh, CMND=@cmnd where MADG=@madg "
        'Dim strQ = String.Format("insert into DOCGIA values ( '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')", Reader.MaDG, Reader.TenDG, Reader.CMND, Reader.DiaChi, Reader.NgaySinh, Reader.Email, Reader.GioiTinh, Reader.HSD, Reader.Role_ID, Reader.Fee)

        Dim cmd As New SqlCommand(strQ)
        cmd.CommandType = CommandType.Text
        Try
            cmd.Parameters.Add("@madg", SqlDbType.NVarChar).Value = reader.MaDG
            cmd.Parameters.Add("@tendg", SqlDbType.NVarChar).Value = reader.TenDG
            cmd.Parameters.Add("@cmnd", SqlDbType.NVarChar).Value = reader.CMND
            cmd.Parameters.Add("@diachi", SqlDbType.NVarChar).Value = reader.DiaChi
            cmd.Parameters.Add("@ngaysinh", SqlDbType.Date).Value = reader.NgaySinh
            cmd.Parameters.Add("@gioitinh", SqlDbType.NVarChar).Value = reader.GioiTinh
            cmd.Parameters.Add("@ID", SqlDbType.Int).Value = reader.Role_ID
            If DataProvider.RunNonQuery(cmd) Then
                Check = True
            End If

        Catch ex As Exception
            Throw ex
            Check = False
        End Try
        Return Check
    End Function

    Public Shared Function XoaDG(ByVal MADG As String) As Boolean
        Try
            Dim strQ As String = "Update DocGia	Set TinhTrang =0 where MADG=@Par"
            Dim cmd As New SqlCommand(strQ)
            cmd.Parameters.Add("@Par", SqlDbType.NVarChar).Value = MADG
            DataProvider.RunNonQuery(cmd)
            Return True
        Catch ex As Exception
            MessageBox.Show("Đã có lỗi xảy ra!!!", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End Try
    End Function

    Public Shared Function LoadSacle() As String
        Dim cmd As New SqlCommand("select top 1 madg from DOCGIA order by madg desc")
        Return DataProvider.Load1Query(cmd)
    End Function

    Public Shared Function LoadRead(ByVal paramet As String, ByVal i As Integer) As DataTable

        Dim strQ As New List(Of String)
        If i = -1 Then
            i = 0
        End If
        With strQ
            .Add("Madg like @Par and DG.LoaiDocGia=Ty.ID and TinhTrang =1 ")
            .Add("CMND like @Par and DG.LoaiDocGia=Ty.ID and TinhTrang =1 ")
            .Add("TenDG like @Par and DG.LoaiDocGia=Ty.ID and TinhTrang =1")
        End With
        Dim str As String = ("SELECT DG.MADG, DG.TenDG, DG.CMND, TY.Role,DG.Fee,DG.SoSachDangMuon,DG.SachQuaHan FROM DocGia DG, LoaiDocGia TY WHERE ")
        str += strQ(i)
        Dim cmd As New SqlCommand(str)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@Par", SqlDbType.NVarChar).Value = paramet

        Return DataProvider.LoaderFromDATA(cmd)
    End Function
    Public Shared Function LoadOneReader(ByVal paramet) As DocGiaDTO
        Dim temDG As New DocGiaDTO
        Dim strQ As String = ("SELECT * FROM DocGia WHERE MADG = @Par")
        Dim cmd As New SqlCommand(strQ)
        Dim table As New DataTable

        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@Par", SqlDbType.NVarChar).Value = paramet


        table = DataProvider.LoaderFromDATA(cmd)
        Dim row As DataRowCollection = table.Rows

        temDG.MaDG = row(0).Item(0).ToString
        temDG.TenDG = row(0).Item(1).ToString
        temDG.CMND = row(0).Item(2).ToString
        temDG.DiaChi = row(0).Item(3).ToString
        temDG.NgaySinh = row(0).Item(4)
        temDG.GioiTinh = row(0).Item(5).ToString
        temDG.HSD = row(0).Item(6).ToString
        temDG.Role_ID = row(0).Item(7).ToString
        temDG.Fee = row(0).Item(8).ToString

        Return temDG
    End Function
    Public Shared Function Loadcombobox() As DataTable

        Dim strQ As String = "select * from LoaiDocGia"
        Dim cmd As New SqlCommand(strQ)

        Return DataProvider.LoaderFromDATA(cmd)
    End Function

End Class
