﻿Imports DTO
Imports System.Data.SqlClient
Public Class HoaDonDAO
    Public Shared Function LoadMAHD() As String
        Dim cmd As New SqlCommand("select top 1 IDHoaDon from HoaDon order by IDHoaDon desc")
        Return DataProvider.Load1Query(cmd)
    End Function
    Public Shared Function GhiPhieu(ByVal Phieu As HoaDonDTO) As Boolean
        Dim strQ As String = "insert into HoaDon values (@ID,@MaDG,@HanThe,@TenDG,@Phi)"
        Dim cmd As New SqlCommand(strQ)

        Try
            cmd.Parameters.Add("@ID", SqlDbType.NChar).Value = Phieu.IDBill
            cmd.Parameters.Add("@MaDG", SqlDbType.NVarChar).Value = Phieu.IDreader
            cmd.Parameters.Add("@HanThe", SqlDbType.DateTime).Value = Phieu.Exp
            cmd.Parameters.Add("@TenDG", SqlDbType.NVarChar).Value = Phieu.NameRead
            cmd.Parameters.Add("@Phi", SqlDbType.Int).Value = Phieu.Fee
            DataProvider.RunNonQuery(cmd)
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
End Class
