﻿Imports DTO
Imports System.Data.SqlClient
Imports System.Data.OleDb
Public Class NhanVienDAO

    Public Function getAll() As DataTable
        Dim cmd As New SqlCommand("SELECT * FROM NhanVien")
        Return DataProvider.LoaderFromDATA(cmd)
    End Function
    Public Function getSex(ByVal tk As String)
        Dim query As String = String.Format("SELECT GioiTinhNV FROM NhanVien WHERE MSNV = '{0}'", tk)
        Dim cmd As New SqlCommand(query)
        Dim kq As String
        Return kq = DataProvider.Load1Query(cmd)
    End Function
    Public Function getById(ByVal ID As String)
        Dim query As String = String.Format("SELECT * FROM NhanVien WHERE MSNV = '{0}'", ID)
        Dim cmd As New SqlCommand(query)
        Dim tbl As DataTable
        Dim nv As New NhanVienDTO()
        Try
            tbl = DataProvider.LoaderFromDATA(cmd)
            If tbl.Rows.Count > 0 Then
                nv.MSNV = tbl.Rows(0)(0)
                nv.HoTenNV = tbl.Rows(0)(1)
                nv.NgaySinh = tbl.Rows(0)(2)
                nv.GioiTinh = tbl.Rows(0)(3)
                nv.DienThoai = tbl.Rows(0)(4)
                Return nv
            Else
                Return DBNull.Value
            End If
        Catch ex As Exception
            Return DBNull.Value
        End Try
    End Function

    Public Function insert(ByVal nv As NhanVienDTO)
        Dim query As String = String.Format("INSERT INTO NhanVien VALUES(@id, @hoten, @ngaysinh, @gioitinh, @sdt)")
        Dim cmd As New SqlCommand(query) With {.CommandType = CommandType.Text}
        Try
            cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = nv.MSNV
            cmd.Parameters.Add("hoten", SqlDbType.NVarChar).Value = nv.HoTenNV
            cmd.Parameters.Add("@ngaysinh", SqlDbType.DateTime).Value = nv.NgaySinh
            cmd.Parameters.Add("@gioitinh", SqlDbType.NVarChar).Value = nv.GioiTinh
            cmd.Parameters.Add("@sdt", SqlDbType.NVarChar).Value = nv.DienThoai
            Return DataProvider.RunNonQuery(cmd)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function update(ByVal nv As NhanVienDTO)
        Dim query As String = String.Format("UPDATE NhanVien set HoTenNV = @hoten, NgaySinhNV = @ngaysinh, GioiTinhNV = @gioitinh, DienThoai = @sdt WHERE MSNV = @msnv")
        Dim cmd As New SqlCommand(query) With {.CommandType = CommandType.Text}
        Try
            cmd.Parameters.Add("@msnv", SqlDbType.NVarChar).Value = nv.MSNV
            cmd.Parameters.Add("@hoten", SqlDbType.NVarChar).Value = nv.HoTenNV
            cmd.Parameters.Add("@ngaysinh", SqlDbType.DateTime).Value = nv.NgaySinh
            cmd.Parameters.Add("@gioitinh", SqlDbType.NVarChar).Value = nv.GioiTinh
            cmd.Parameters.Add("@sdt", SqlDbType.NVarChar).Value = nv.DienThoai
            Return DataProvider.RunNonQuery(cmd)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function delete(ByVal nv As NhanVienDTO)
        Dim query As String = String.Format("DELETE FROM NhanVien WHERE MSNV = @manv")
        Dim cmd As New SqlCommand(query) With {.CommandType = CommandType.Text}
        Try
            cmd.Parameters.Add("@manv", SqlDbType.NVarChar).Value = nv.MSNV
            Return DataProvider.RunNonQuery(cmd)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function getByName(ByVal Name As String)
        Dim query As String = String.Format("SELECT  * FROM NhanVien WHERE HoTenNV LIKE N'%{0}%'", Name)
        Dim cmd As New SqlCommand(query)
        Try
            Return DataProvider.LoaderFromDATA(cmd)
        Catch ex As Exception
            Return DBNull.Value
        End Try
    End Function

    Public Function searchById(ByVal Id As String)
        Dim query As String = String.Format("SELECT  * FROM NhanVien WHERE MSNV LIKE N'%{0}%'", Id)
        Dim cmd As New SqlCommand(query)
        Try
            Return DataProvider.LoaderFromDATA(cmd)
        Catch ex As Exception
            Return DBNull.Value
        End Try
    End Function

    Public Function searchBySDT(ByVal SDT As String)
        Dim query As String = String.Format("SELECT  * FROM NhanVien WHERE DienThoai LIKE '%{0}%'", SDT)
        Dim cmd As New SqlCommand(query)
        Try
            Return DataProvider.LoaderFromDATA(cmd)
        Catch ex As Exception
            Return DBNull.Value
        End Try
    End Function

    Public Function searchBySex(ByVal gt As String)
        Dim query As String = String.Format("SELECT  * FROM NhanVien WHERE GioiTinhNV = N'{0}'", gt)
        Dim cmd As New SqlCommand(query)
        Try
            Return DataProvider.LoaderFromDATA(cmd)
        Catch ex As Exception
            Return DBNull.Value
        End Try
    End Function

End Class
