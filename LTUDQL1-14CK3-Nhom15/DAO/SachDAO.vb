﻿Imports DTO
Imports System.Data.SqlClient
Public Class SachDAO
    Public Shared Function LoadSach(ByVal i As Integer, ByVal Par As String) As DataTable
        Dim strQ As String = "SELECT MaSach, TenSach, TacGia, NXB,LoaiSach FROM QuyenSach QS, LoaiSach LS where QS.Maloaisach=LS.Maloaisach and "
        Dim strcbb As New List(Of String)
        With strcbb
            .Add("MaSach like @par")
            .Add("TenSach like @par")
            .Add("TacGia like @par")
            .Add("NXB like @par")
            .Add("NamXB like @par")
            .Add("MaSach like @par or TenSach like @par or SoLuong like @par")
        End With
        strQ += strcbb(i)
        Dim cmd As New SqlCommand(strQ)
        cmd.CommandType = CommandType.Text
        cmd.Parameters.Add("@par", SqlDbType.NVarChar).Value = Par

        DataProvider.LoaderFromDATA(cmd)

        Return DataProvider.LoaderFromDATA(cmd)
    End Function

    Public Shared Function ThemSach(ByVal Sach As SachDTO) As Boolean
        Dim strQ As String = "insert into QuyenSach values (@MaSach, @TenSach, @TacGia, @NXB, @MaLoaiSach, @NamXB, @LanXB, @SoLuong, @NoiDungTomTat, @SoTrang, @DangMuon, @MatOrHu, @ISBN)"
        Dim ischeck As Boolean = False
        Dim cmd As New SqlCommand(strQ)
        cmd.CommandType = CommandType.Text

        Try
            cmd.Parameters.Add("@MaSach", SqlDbType.NVarChar).Value = Sach.MaSach
            cmd.Parameters.Add("@ISBN", SqlDbType.NVarChar).Value = Sach.ISBN
            cmd.Parameters.Add("@NoiDungTomTat", SqlDbType.NVarChar).Value = Sach.Noidungtomtat
            cmd.Parameters.Add("@TacGia", SqlDbType.NVarChar).Value = Sach.TacGia
            cmd.Parameters.Add("@TenSach", SqlDbType.NVarChar).Value = Sach.TenSach
            cmd.Parameters.Add("@NXB", SqlDbType.NVarChar).Value = Sach.NXB
            cmd.Parameters.Add("@SoLuong", SqlDbType.Int).Value = Sach.SoLuong
            cmd.Parameters.Add("@SoTrang", SqlDbType.Int).Value = Sach.SoTrang
            cmd.Parameters.Add("@NamXB", SqlDbType.Int).Value = Sach.NamXB
            cmd.Parameters.Add("@MaLoaiSach", SqlDbType.Int).Value = Sach.MaloaiSach
            cmd.Parameters.Add("@LanXB", SqlDbType.Int).Value = Sach.LanXB

            cmd.Parameters.Add("@DangMuon", SqlDbType.Int).Value = 0
            cmd.Parameters.Add("@MatOrHu", SqlDbType.Int).Value = 0


            If DataProvider.RunNonQuery(cmd) Then
                ischeck = True
            End If

        Catch ex As Exception
            ischeck = False
        End Try
        Return ischeck
    End Function
    Public Shared Function XoaSach(ByVal par As String) As Integer
        Dim str As String = "delete from QuyenSach where maSach=@par "
        Dim cmd As New SqlCommand(str)
        cmd.Parameters.Add("@par", SqlDbType.NVarChar).Value = par
        If DataProvider.RunNonQuery(cmd) Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Shared Function LoadMaSach() As String
        Dim cmd As New SqlCommand("select top 1 masach from QuyenSach order by MaSach desc")
        Return DataProvider.Load1Query(cmd)
    End Function
    Public Shared Function Loadcombobox() As DataTable
        Dim strQ As String = "select * from LoaiSach"
        Dim cmd As New SqlCommand(strQ)
        Return DataProvider.LoaderFromDATA(cmd)
    End Function
    Public Shared Function Load1Sach(ByVal par As String) As SachDTO
        Dim strQ As String = "select * from QuyenSach where maSach=@par"
        Dim cmd As New SqlCommand(strQ)
        Dim tem As New SachDTO
        Dim table As New DataTable
        cmd.Parameters.Add("@par", SqlDbType.NVarChar).Value = par

        table = DataProvider.LoaderFromDATA(cmd)
        Dim row As DataRowCollection = table.Rows



        tem.MaSach = row(0).Item(0).ToString
        tem.TenSach = row(0).Item(1).ToString
        tem.TacGia = row(0).Item(2).ToString
        tem.NXB = row(0).Item(3).ToString
        tem.MaloaiSach = row(0).Item(4).ToString
        tem.NamXB = row(0).Item(5).ToString
        tem.LanXB = row(0).Item(6).ToString
        tem.SoLuong = row(0).Item(7).ToString
        tem.Noidungtomtat = row(0).Item(8).ToString
        tem.SoTrang = row(0).Item(9).ToString
        tem.DangMuon = row(0).Item(10).ToString
        tem.MatorHu = row(0).Item(11).ToString
        tem.ISBN = row(0).Item(12).ToString
        Return tem
    End Function
    Public Shared Function CapNhatSach(ByVal Sach As SachDTO) As Boolean
        Dim strQ As String = "update QuyenSach set TenSach= @TenSach,TacGia= @TacGia,NXB= @NXB,MaLoaiSach= @MaLoaiSach,NamXB= @NamXB,LanXB= @LanXB,SoLuong= @SoLuong,NoiDungTomTat= @NoiDungTomTat,SoTrang= @SoTrang,ISBN= @ISBN where Masach= @MaSach "
        Dim ischeck As Boolean = False
        Dim cmd As New SqlCommand(strQ)
        cmd.CommandType = CommandType.Text

        Try
            cmd.Parameters.Add("@MaSach", SqlDbType.NVarChar).Value = Sach.MaSach
            cmd.Parameters.Add("@ISBN", SqlDbType.NVarChar).Value = Sach.ISBN
            cmd.Parameters.Add("@NoiDungTomTat", SqlDbType.NVarChar).Value = Sach.Noidungtomtat
            cmd.Parameters.Add("@TacGia", SqlDbType.NVarChar).Value = Sach.TacGia
            cmd.Parameters.Add("@TenSach", SqlDbType.NVarChar).Value = Sach.TenSach
            cmd.Parameters.Add("@NXB", SqlDbType.NVarChar).Value = Sach.NXB
            cmd.Parameters.Add("@SoLuong", SqlDbType.Int).Value = Sach.SoLuong
            cmd.Parameters.Add("@SoTrang", SqlDbType.Int).Value = Sach.SoTrang
            cmd.Parameters.Add("@NamXB", SqlDbType.Int).Value = Sach.NamXB
            cmd.Parameters.Add("@MaLoaiSach", SqlDbType.Int).Value = Sach.MaloaiSach
            cmd.Parameters.Add("@LanXB", SqlDbType.Int).Value = Sach.LanXB



            If DataProvider.RunNonQuery(cmd) Then
                ischeck = True
            End If

        Catch ex As Exception
            ischeck = False
        End Try
        Return ischeck
    End Function

End Class

'"insert into QuyenSach values (@MaSach, @TenSach, @TacGia, @NXB, @MaLoaiSach, @NamXB, @LanXB, @SoLuong, @NoiDungTomTat, @SoTrang, @DangMuon, @MatOrHu, @ISBN)"