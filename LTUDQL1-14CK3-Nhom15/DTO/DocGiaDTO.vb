﻿Public Class DocGiaDTO
#Region "attributes"

    Dim _MaDG As String
    Dim _TenDG As String
    Dim _NgaySinh As DateTime
    Dim _Fee As Integer
    Dim _GioiTinh As String
    Dim _HSD As DateTime
    Dim _Role_ID As Integer
    Dim _CMND As String
    Dim _DiaChi As String
    Dim _SachQuaHan As Integer
    Dim _SoSachDangMuon As Integer
    Dim _TinhTrang As Boolean

#End Region
#Region "Properties"


    Public Property TenDG() As String
        Get
            Return _TenDG
        End Get
        Set(value As String)
            _TenDG = value
        End Set
    End Property

    Public Property NgaySinh() As Date
        Get
            Return _NgaySinh
        End Get
        Set(value As Date)
            _NgaySinh = value
        End Set
    End Property

    Public Property Fee() As Integer
        Get
            Return _Fee
        End Get
        Set(value As Integer)
            _Fee = value
        End Set
    End Property

    Public Property GioiTinh() As String
        Get
            Return _GioiTinh
        End Get
        Set(value As String)
            _GioiTinh = value
        End Set
    End Property


    Public Property CMND() As String
        Get
            Return _CMND
        End Get
        Set(value As String)
            _CMND = value
        End Set
    End Property

    Public Property DiaChi() As String
        Get
            Return _DiaChi
        End Get
        Set(value As String)
            _DiaChi = value
        End Set
    End Property

    Public Property HSD() As Date
        Get
            Return _HSD
        End Get
        Set(value As Date)
            _HSD = value
        End Set
    End Property



    Public Property MaDG() As String
        Get
            Return _MaDG
        End Get
        Set(value As String)
            _MaDG = value
        End Set
    End Property

    Public Property SachQuaHan() As Integer
        Get
            Return _SachQuaHan
        End Get
        Set(value As Integer)
            _SachQuaHan = value
        End Set
    End Property

    Public Property SoSachDangMuon() As Integer
        Get
            Return _SoSachDangMuon
        End Get
        Set(value As Integer)
            _SoSachDangMuon = value
        End Set
    End Property

    Public Property Role_ID As Integer
        Get
            Return _Role_ID
        End Get
        Set(value As Integer)
            _Role_ID = value
        End Set
    End Property

    Public Property TinhTrang As Boolean
        Get
            Return _TinhTrang
        End Get
        Set(value As Boolean)
            _TinhTrang = value
        End Set
    End Property


#End Region
End Class
