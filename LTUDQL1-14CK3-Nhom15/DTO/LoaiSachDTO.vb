﻿Public Class LoaiSachDTO
    Dim _MaLoaiSach As Integer
    Dim _LoaiSach As String
    Dim _ThongTin As String

    Public Property MaLoaiSach() As Integer
        Get
            Return _MaLoaiSach
        End Get
        Set(value As Integer)
            _MaLoaiSach = value
        End Set
    End Property

    Public Property LoaiSach() As String
        Get
            Return _LoaiSach
        End Get
        Set(value As String)
            _LoaiSach = value
        End Set
    End Property

    Public Property ThongTin() As String
        Get
            Return _ThongTin
        End Get
        Set(value As String)
            _ThongTin = value
        End Set
    End Property
End Class
