﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class QLTK
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTK = New System.Windows.Forms.TextBox()
        Me.cmbChucVu = New System.Windows.Forms.ComboBox()
        Me.dgvTK = New System.Windows.Forms.DataGridView()
        Me.btnThem = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnXoa = New System.Windows.Forms.Button()
        Me.btnSua = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblMK = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtMSNV = New System.Windows.Forms.TextBox()
        Me.txtMK = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.cmbSearch = New System.Windows.Forms.ComboBox()
        Me.btnChamHoi = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.dgvTK, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(31, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Tài khoản: "
        '
        'txtTK
        '
        Me.txtTK.Location = New System.Drawing.Point(98, 22)
        Me.txtTK.Name = "txtTK"
        Me.txtTK.Size = New System.Drawing.Size(126, 20)
        Me.txtTK.TabIndex = 1
        '
        'cmbChucVu
        '
        Me.cmbChucVu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbChucVu.FormattingEnabled = True
        Me.cmbChucVu.Location = New System.Drawing.Point(301, 64)
        Me.cmbChucVu.Name = "cmbChucVu"
        Me.cmbChucVu.Size = New System.Drawing.Size(141, 21)
        Me.cmbChucVu.TabIndex = 2
        '
        'dgvTK
        '
        Me.dgvTK.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvTK.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTK.Location = New System.Drawing.Point(12, 165)
        Me.dgvTK.Name = "dgvTK"
        Me.dgvTK.Size = New System.Drawing.Size(675, 229)
        Me.dgvTK.TabIndex = 3
        '
        'btnThem
        '
        Me.btnThem.Location = New System.Drawing.Point(478, 19)
        Me.btnThem.Name = "btnThem"
        Me.btnThem.Size = New System.Drawing.Size(197, 23)
        Me.btnThem.TabIndex = 4
        Me.btnThem.Text = "&Thêm"
        Me.btnThem.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.btnChamHoi)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.lblMK)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.btnSua)
        Me.GroupBox1.Controls.Add(Me.btnXoa)
        Me.GroupBox1.Controls.Add(Me.btnThem)
        Me.GroupBox1.Controls.Add(Me.txtMK)
        Me.GroupBox1.Controls.Add(Me.txtMSNV)
        Me.GroupBox1.Controls.Add(Me.txtTK)
        Me.GroupBox1.Controls.Add(Me.cmbChucVu)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(681, 112)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Thông tin tài khoản"
        '
        'btnXoa
        '
        Me.btnXoa.Location = New System.Drawing.Point(478, 48)
        Me.btnXoa.Name = "btnXoa"
        Me.btnXoa.Size = New System.Drawing.Size(197, 23)
        Me.btnXoa.TabIndex = 4
        Me.btnXoa.Text = "&Xóa"
        Me.btnXoa.UseVisualStyleBackColor = True
        '
        'btnSua
        '
        Me.btnSua.Location = New System.Drawing.Point(478, 77)
        Me.btnSua.Name = "btnSua"
        Me.btnSua.Size = New System.Drawing.Size(197, 23)
        Me.btnSua.TabIndex = 4
        Me.btnSua.Text = "&Sửa"
        Me.btnSua.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 67)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Của nhân viên: "
        '
        'lblMK
        '
        Me.lblMK.AutoSize = True
        Me.lblMK.Location = New System.Drawing.Point(242, 24)
        Me.lblMK.Name = "lblMK"
        Me.lblMK.Size = New System.Drawing.Size(51, 13)
        Me.lblMK.TabIndex = 0
        Me.lblMK.Text = "mật khẩu"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(242, 67)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Chức vụ: "
        '
        'txtMSNV
        '
        Me.txtMSNV.Location = New System.Drawing.Point(98, 64)
        Me.txtMSNV.Name = "txtMSNV"
        Me.txtMSNV.Size = New System.Drawing.Size(126, 20)
        Me.txtMSNV.TabIndex = 1
        '
        'txtMK
        '
        Me.txtMK.Location = New System.Drawing.Point(301, 22)
        Me.txtMK.Name = "txtMK"
        Me.txtMK.Size = New System.Drawing.Size(141, 20)
        Me.txtMK.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 136)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Tìm kiếm: "
        '
        'txtSearch
        '
        Me.txtSearch.Location = New System.Drawing.Point(73, 133)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(163, 20)
        Me.txtSearch.TabIndex = 7
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(430, 131)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(116, 23)
        Me.btnSearch.TabIndex = 8
        Me.btnSearch.Text = "&Tìm Kiếm "
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'cmbSearch
        '
        Me.cmbSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSearch.FormattingEnabled = True
        Me.cmbSearch.Location = New System.Drawing.Point(257, 133)
        Me.cmbSearch.Name = "cmbSearch"
        Me.cmbSearch.Size = New System.Drawing.Size(151, 21)
        Me.cmbSearch.TabIndex = 9
        '
        'btnChamHoi
        '
        Me.btnChamHoi.Location = New System.Drawing.Point(446, 20)
        Me.btnChamHoi.Name = "btnChamHoi"
        Me.btnChamHoi.Size = New System.Drawing.Size(24, 25)
        Me.btnChamHoi.TabIndex = 5
        Me.btnChamHoi.Text = "?"
        Me.btnChamHoi.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.ForeColor = System.Drawing.Color.DarkCyan
        Me.Button1.Location = New System.Drawing.Point(651, 136)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(36, 23)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "?"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'QLTK
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(705, 406)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.cmbSearch)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.txtSearch)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvTK)
        Me.Name = "QLTK"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "QLTK"
        CType(Me.dgvTK, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTK As System.Windows.Forms.TextBox
    Friend WithEvents cmbChucVu As System.Windows.Forms.ComboBox
    Friend WithEvents dgvTK As System.Windows.Forms.DataGridView
    Friend WithEvents btnThem As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblMK As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnSua As System.Windows.Forms.Button
    Friend WithEvents btnXoa As System.Windows.Forms.Button
    Friend WithEvents txtMK As System.Windows.Forms.TextBox
    Friend WithEvents txtMSNV As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents cmbSearch As System.Windows.Forms.ComboBox
    Friend WithEvents btnChamHoi As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
