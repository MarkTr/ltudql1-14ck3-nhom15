﻿Imports BUS
Imports DTO

Public Class QLTK

    Private Sub loadData()
        Dim tkBUS As New TaiKhoanBUS()
        Dim dtb As DataTable = tkBUS.getAll()
        dgvTK.DataSource = dtb
    End Sub

    Private Sub QLTK_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvTK.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        cmbChucVu.Items.Add("Thủ thư")
        cmbChucVu.Items.Add("Quản lý (Admin)")
        cmbSearch.Items.Add("Tài khoản")
        cmbSearch.Items.Add("Mã nhân viên")
        cmbSearch.Items.Add("Chức vụ")
        cmbChucVu.SelectedIndex = -1
        cmbSearch.SelectedIndex = -1
        loadData()
        dgvTK.Columns(0).HeaderText = "Tài khoản"
        dgvTK.Columns(1).HeaderText = "Mật khẩu"
        dgvTK.Columns(2).HeaderText = "Của nhân viên"
        dgvTK.Columns(3).HeaderText = "Cấp độ"
    End Sub
    Private Sub Bind(ByVal tk As TaiKhoanDTO)
        txtTK.Enabled = False
        txtTK.Text = tk.TaiKhoan
        txtMK.Text = tk.MatKhau
        txtMSNV.Text = tk.MSNV
        cmbChucVu.SelectedIndex = tk.Cap
    End Sub
    Private Sub ClearBind()
        txtTK.Enabled = True
        txtTK.Clear()
        txtMK.Clear()
        txtMSNV.Clear()
        cmbChucVu.SelectedIndex = -1
    End Sub
    Private Sub dgvTK_SelectionChanged(sender As Object, e As EventArgs) Handles dgvTK.SelectionChanged
        Try
            Dim tkBUS As New TaiKhoanBUS()
            Dim tk As New TaiKhoanDTO()
            tk = tkBUS.getByUsername(dgvTK.CurrentRow.Cells(0).Value)
            Bind(tk)
        Catch ex As Exception
            ClearBind()
            Return
        End Try
    End Sub

    Private Sub btnThem_Click(sender As Object, e As EventArgs) Handles btnThem.Click
        If (txtTK.Text = String.Empty) Then
            MessageBox.Show("Bạn chưa nhập tài khoản.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtTK.Focus()
            Return
        End If
        If (txtMK.Text = String.Empty) Then
            MessageBox.Show("Bạn chưa nhập mật khẩu.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtMK.Focus()
            Return
        End If
        If (txtMSNV.Text = String.Empty) Then
            MessageBox.Show("Bạn chưa chỉ định tài khoản này cho nhân viên nào.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtMSNV.Focus()
            Return
        End If
        Dim tkBUS As New TaiKhoanBUS()
        If tkBUS.getByUsername(txtTK.Text) IsNot DBNull.Value Then
            MessageBox.Show("tên tài khoản này đã được sử dụng.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If
        If tkBUS.getByMSNV(txtMSNV.Text) IsNot DBNull.Value Then
            MessageBox.Show("Nhân viên này đã được cấp tài khoản.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If
        Try
            Dim tk As New TaiKhoanDTO() With {
                .TaiKhoan = txtTK.Text,
                .MatKhau = txtMK.Text,
                .MSNV = txtMSNV.Text,
                .Cap = cmbChucVu.SelectedIndex}
            If (tkBUS.insert(tk) <> False) Then
                MessageBox.Show("Tài khoản đã được thêm vào thành công.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                dgvTK.DataSource = Nothing
                loadData()
            End If
        Catch ex As Exception
            MessageBox.Show("Đã có lỗi xảy ra trong quá trình thêm tài khoản " & vbNewLine & " .Bạn hãy kiểm tra lại thông tin tài khoản cần thêm." & vbNewLine & " .Kiểm tra lại kết nối cơ sở dữ liệu.")
            Return
        End Try
    End Sub

    Private Sub txtMK_Click(sender As Object, e As EventArgs) Handles txtMK.Click
        txtMK.Clear()
    End Sub

    Private Sub btnSua_Click(sender As Object, e As EventArgs) Handles btnSua.Click
        If (txtTK.Text = String.Empty) Then
            MessageBox.Show("Bạn chưa nhập tài khoản.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtTK.Focus()
            Return
        End If
        If (txtMK.Text = String.Empty) Then
            MessageBox.Show("Bạn chưa nhập mật khẩu.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtMK.Focus()
            Return
        End If
        If (txtMSNV.Text = String.Empty) Then
            MessageBox.Show("Bạn chưa chỉ định tài khoản này cho nhân viên nào.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtMSNV.Focus()
            Return
        End If
        Dim tkBUS As New TaiKhoanBUS()
        Try
            Dim tk As New TaiKhoanDTO() With {
                .TaiKhoan = txtTK.Text,
                .MatKhau = txtMK.Text,
                .MSNV = txtMSNV.Text,
                .Cap = cmbChucVu.SelectedIndex}
            If tkBUS.Update(tk) = True Then
                MessageBox.Show("Thông tin tài khoản đã được cập nhật thành công.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                dgvTK.DataSource = Nothing
                loadData()
            End If
        Catch ex As Exception
            MessageBox.Show("Đã có lỗi xảy ra trong quá trình cập nhật dữ liệu." & vbNewLine & " *Lưu ý rằng bạn không thể chỉnh sửa tên tài khoản và mỗi nhân viên chỉ có thể được cấp 1 tài khoản duy nhất.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Private Sub btnChamHoi_Click(sender As Object, e As EventArgs) Handles btnChamHoi.Click
        MessageBox.Show("Vì lý do bảo mật nên mật khẩu của User sẽ được hiển thị ở dạng mã hash (SHA512) " & vbNewLine & "Nếu muốn thay đổi mật khẩu, bạn chỉ cần click chuột vào ô nhập mật khẩu, đoạn mã hash trên textbox đó sẽ biến mất và bạn chỉ cần nhập mật khẩu như bình thường. 👌( ͡° ͜ʖ ͡°👌)")
    End Sub

    Public str As String

    Private Sub btnXoa_Click(sender As Object, e As EventArgs) Handles btnXoa.Click
        Dim tkBUS As New TaiKhoanBUS()
        Dim frm As New frmMain()
        If str = txtTK.Text Then
            MessageBox.Show("Bạn không thể xóa tài khoản của chính mình.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If
        Dim tk As String = txtTK.Text
        Try
            If tkBUS.Delete(tk) = True Then
                MessageBox.Show("Đã xóa thành công tài khoản trên.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                dgvTK.DataSource = Nothing
                loadData()
            End If
            Return
        Catch ex As Exception
            MessageBox.Show("Đã có lỗi xảy ra trong quá trình xóa tài khoản.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If txtSearch.Text = String.Empty Then
            Dim tkBUS As New TaiKhoanBUS()
            dgvTK.DataSource = Nothing
            dgvTK.DataSource = tkBUS.getAll()
            txtSearch.Focus()
            Return
        End If
        If cmbSearch.SelectedIndex = -1 Then
            MessageBox.Show("Bạn cần chọn mục để tìm kiếm.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        End If
        Select Case cmbSearch.SelectedIndex
            Case 0
                Dim tkBUS As New TaiKhoanBUS()
                Try
                    Dim dtb As DataTable = tkBUS.searchByUsername(txtSearch.Text)
                    dgvTK.DataSource = Nothing
                    dgvTK.DataSource = dtb
                Catch ex As Exception
                    MessageBox.Show("Đã có lỗi xảy ra trong quá trình tìm kiếm.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return
                End Try
            Case 1
                Dim tkBUS As New TaiKhoanBUS()
                Try
                    Dim dtb As DataTable = tkBUS.searchByMSNV(txtSearch.Text)
                    dgvTK.DataSource = Nothing
                    dgvTK.DataSource = dtb
                Catch ex As Exception
                    MessageBox.Show("Đã có lỗi xảy ra trong quá trình tìm kiếm.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return
                End Try
            Case 2
                Dim tkBUS As New TaiKhoanBUS()
                Try
                    Dim searchPara As Integer
                    If txtSearch.Text = "thủ thư" Then
                        searchPara = 0
                    ElseIf txtSearch.Text.Contains("admin") Then
                        searchPara = 1
                    Else
                        MessageBox.Show("Dữ liệu tìm kiếm không hợp lệ." & vbNewLine & "*Lưu ý: Đối với tìm kiếm theo chức vụ thì bạn chỉ có thể tìm kiếm theo 2 loại Admin (Administrator) hoặc Thủ thư.")
                        Return
                    End If
                    Dim dtb As DataTable = tkBUS.getByRole(searchPara)
                    dgvTK.DataSource = Nothing
                    dgvTK.DataSource = dtb
                Catch ex As Exception
                    MessageBox.Show("Đã có lỗi xảy ra trong quá trình tìm kiếm.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return
                End Try

        End Select
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        MessageBox.Show("Cap (cấp) là một hệ thống phân cấp tài khoản theo 2 chức vụ là thủ thư và Quản lý (Admin), trong đó: " & vbNewLine & vbTab & "+ 0: là thủ thư." & vbNewLine & vbTab & "+ 1: Quản lý (Administrator)." & vbNewLine & "Các tài khoản có cấp là 0 thì sẽ không được sử dụng chức năng quản lý tài khoản cũng như là quản lý thông tin nhân viên.")
    End Sub
End Class