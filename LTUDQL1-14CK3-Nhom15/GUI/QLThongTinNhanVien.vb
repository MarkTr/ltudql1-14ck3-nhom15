﻿Imports DTO
Imports BUS

Public Class QLThongTinNhanVien

    Private Sub loadData()
        Dim nvBUS As New NhanVienBUS()
        Dim nvdt As DataTable = nvBUS.getAll()
        dgvNhanVien.DataSource = nvdt
    End Sub

    Private Sub QLThongTinNhanVien_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvNhanVien.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        cmbGioiTinh.Items.Add("Nam")
        cmbGioiTinh.Items.Add("Nữ")
        cmbGioiTinh.SelectedIndex = -1
        cmbSearch.Items.Add("Mã số")
        cmbSearch.Items.Add("Họ tên")
        cmbSearch.Items.Add("Số điện thoại")
        cmbSearch.Items.Add("Giới tính")
        cmbSearch.SelectedIndex = -1
        loadData()
        dgvNhanVien.Columns(0).HeaderText = "Mã nhân viên"
        dgvNhanVien.Columns(1).HeaderText = "Họ tên"
        dgvNhanVien.Columns(2).HeaderText = "Ngày sinh"
        dgvNhanVien.Columns(2).DefaultCellStyle.Format = "MM/dd/yyyy"
        dgvNhanVien.Columns(3).HeaderText = "Giới tính"
        dgvNhanVien.Columns(4).HeaderText = "Số điện thoại"
    End Sub

    Private Sub Bind(ByVal nv As NhanVienDTO)
        txtMSNV.Enabled = False
        txtHoTen.Text = nv.HoTenNV
        txtMSNV.Text = nv.MSNV
        txtSDT.Text = nv.DienThoai
        If nv.GioiTinh = "Nam" Then
            cmbGioiTinh.SelectedIndex = 0
        Else
            cmbGioiTinh.SelectedIndex = 1
        End If
        dtpNgaySinh.Value = nv.NgaySinh
    End Sub

    Private Sub clearBind()
        txtMSNV.Enabled = True
        txtHoTen.Clear()
        txtMSNV.Clear()
        txtSDT.Clear()
        cmbGioiTinh.SelectedIndex = -1
        dtpNgaySinh.Value = Now
    End Sub



    Private Sub dgvNhanVien_SelectionChanged(sender As Object, e As EventArgs) Handles dgvNhanVien.SelectionChanged
        Try
            Dim nvBUS As New NhanVienBUS()
            Dim nv As New NhanVienDTO()
            nv = nvBUS.getByID(dgvNhanVien.CurrentRow.Cells(0).Value)
            Bind(nv)
        Catch ex As Exception
            clearBind()
            Return
        End Try
    End Sub

    Private Sub btnThem_Click(sender As Object, e As EventArgs) Handles btnThem.Click
        Dim nvBUS As New NhanVienBUS()
        If txtHoTen.Text = String.Empty Then
            MessageBox.Show("Bạn chưa nhập tên nhân viên.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtHoTen.Focus()
            Return
        End If
        If txtMSNV.Text = String.Empty Then
            MessageBox.Show("Bạn chưa nhập mã số nhân viên.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtMSNV.Focus()
            Return
        End If
        If txtSDT.Text = String.Empty Then
            MessageBox.Show("Bạn chưa nhập vào số điện thoại nhân viên.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtSDT.Focus()
            Return
        End If

        If cmbGioiTinh.SelectedIndex = -1 Then
            MessageBox.Show("Bạn chưa chọn giới tính cho nhân viên.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If

        If nvBUS.getByID(txtMSNV.Text) IsNot DBNull.Value Then
            MessageBox.Show("Mã nhân viên nhập vào đã bị trùng, xin hãy nhập một mã khác.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtMSNV.Focus()
            Return
        End If
        Dim nv As New NhanVienDTO() With {
            .HoTenNV = txtHoTen.Text,
            .MSNV = txtMSNV.Text,
            .DienThoai = txtSDT.Text,
            .GioiTinh = cmbGioiTinh.SelectedItem,
            .NgaySinh = dtpNgaySinh.Value}
        Try
            If (nvBUS.insert(nv) <> False) Then
                MessageBox.Show("Đã thêm nhân viên thành công.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                dgvNhanVien.DataSource = Nothing
                loadData()
            End If
        Catch ex As Exception
            MessageBox.Show("Đã có lỗi xảy ra trong quá trình thêm nhân viên " & vbNewLine & " Vui lòng kiểm tra lại các thông tin của nhân viên cần thêm.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End Try
    End Sub

    Private Sub btnSua_Click(sender As Object, e As EventArgs) Handles btnSua.Click
        Dim nvBUS As New NhanVienBUS()
        If txtHoTen.Text = String.Empty Then
            MessageBox.Show("Bạn chưa nhập tên nhân viên.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtHoTen.Focus()
            Return
        End If
        If txtSDT.Text = String.Empty Then
            MessageBox.Show("Bạn chưa nhập vào số điện thoại nhân viên.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtSDT.Focus()
            Return
        End If
        If cmbGioiTinh.SelectedIndex = -1 Then
            MessageBox.Show("Bạn chưa chọn giới tính cho nhân viên.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If
        Dim nv As New NhanVienDTO() With {
            .HoTenNV = txtHoTen.Text,
            .MSNV = txtMSNV.Text,
            .DienThoai = txtSDT.Text,
            .GioiTinh = cmbGioiTinh.SelectedItem,
            .NgaySinh = dtpNgaySinh.Value}
        Try
            If nvBUS.update(nv) <> False Then
                MessageBox.Show("Chỉnh sửa thành công.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                dgvNhanVien.DataSource = Nothing
                loadData()
            End If
        Catch ex As Exception
            MessageBox.Show("Đã có lỗi xảy ra trong quá trình chỉnh sửa thông tin nhân viên.")
            Return
        End Try
    End Sub

    Public str As String

    Private Sub btnXoa_Click(sender As Object, e As EventArgs) Handles btnXoa.Click
        Dim nvBUS As New NhanVienBUS()

        Dim tkBUS As New TaiKhoanBUS()
        If (tkBUS.getMSNV(str) = txtMSNV.Text) Then
            MessageBox.Show("Bạn không thể xóa chính mình ra khỏi tập dữ liệu.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If
        Dim nv As New NhanVienDTO() With {
            .MSNV = txtMSNV.Text,
            .HoTenNV = txtHoTen.Text,
            .NgaySinh = dtpNgaySinh.Value,
            .GioiTinh = cmbGioiTinh.SelectedItem,
            .DienThoai = txtSDT.Text}
        Try
            If MessageBox.Show("Bạn có chắc là muốn xóa nhân viên này ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                tkBUS.Delete(tkBUS.getUsername(txtMSNV.Text))
                If nvBUS.Delete(nv) <> False Then
                    tkBUS.Delete(tkBUS.getUsername(txtMSNV.Text))
                    MessageBox.Show(String.Format("Đã xóa thành công nhân viên {0} và các dữ liệu có liên quan tới nhân viên đó.", nv.MSNV), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    dgvNhanVien.DataSource = Nothing
                    loadData()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Có lỗi xảy ra trong quá trình xóa nhân viên.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End Try
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If txtSearch.Text = String.Empty Then
            dgvNhanVien.DataSource = Nothing
            loadData()
            Return
        End If
        If cmbSearch.SelectedIndex = -1 Then
            MessageBox.Show("Bạn hãy chọn một mục để tìm kiếm.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If
        Select Case cmbSearch.SelectedIndex
            Case 0
                Dim nvBUS As New NhanVienBUS()
                Dim nv As New NhanVienDTO()
                Try
                    Dim dtb As DataTable = nvBUS.searchByID(txtSearch.Text)
                    dgvNhanVien.DataSource = Nothing
                    dgvNhanVien.DataSource = dtb
                Catch ex As Exception
                    MessageBox.Show("Đã có lỗi xảy ra trong quá trình tìm kiếm.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return
                End Try
            Case 1
                Dim nvBUS As New NhanVienBUS()
                Dim nv As New NhanVienDTO()
                Try
                    Dim dtb As DataTable = nvBUS.getByName(txtSearch.Text)
                    dgvNhanVien.DataSource = Nothing
                    dgvNhanVien.DataSource = dtb
                Catch ex As Exception
                    MessageBox.Show("Đã có lỗi xảy ra trong quá trình tìm kiếm.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return
                End Try
            Case 2
                Dim nvBUS As New NhanVienBUS()
                Dim nv As New NhanVienDTO()
                Try
                    Dim dtb As DataTable = nvBUS.searchByPhone(txtSearch.Text)
                    dgvNhanVien.DataSource = Nothing
                    dgvNhanVien.DataSource = dtb
                Catch ex As Exception
                    MessageBox.Show("Đã có lỗi xảy ra trong quá trình tìm kiếm.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return
                End Try
            Case 3
                Dim nvBUS As New NhanVienBUS()
                Dim nv As New NhanVienDTO()
                Dim dtb As DataTable
                Try
                    If txtSearch.Text = "Nam" Then
                        dtb = nvBUS.searchBySex(txtSearch.Text)
                        dgvNhanVien.DataSource = Nothing
                        dgvNhanVien.DataSource = dtb
                    ElseIf txtSearch.Text = "Nữ" Or txtSearch.Text = "Nu" Then
                        dtb = nvBUS.searchBySex(cmbGioiTinh.SelectedItem)
                        dgvNhanVien.DataSource = Nothing
                        dgvNhanVien.DataSource = dtb
                    Else
                        MessageBox.Show("Dữ liệu tìm kiếm không đúng yêu cầu, đối với dữ liệu tìm kiếm dạng Giới tính (sex) thì bạn chỉ có thể tìm kiếm theo 2 loại là Nam và Nữ.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return
                    End If
                Catch ex As Exception
                    MessageBox.Show("Đã có lỗi xảy ra trong quá trình tìm kiếm.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return
                End Try
        End Select
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        MessageBox.Show("Bạn chỉ có thể cài đặt mã nhân viên cho một đối tượng một lần duy nhất (từ lúc thêm nhân viên đó vào), nếu muốn chỉnh sửa mã nhân viên thì cần phải xóa nhân viên đó rồi tạo mới lại, cho nên hãy cẩn thận trong việc thêm vào một nhân viên.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
End Class