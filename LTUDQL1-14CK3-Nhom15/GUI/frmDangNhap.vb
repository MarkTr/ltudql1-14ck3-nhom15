﻿Imports DTO
Imports BUS
Public Class frmDangNhap

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        txtUserName.Clear()
        txtPassword.Clear()
    End Sub

    Private Sub btnSignIn_Click(sender As Object, e As EventArgs) Handles btnDangNhap.Click
        If (txtUserName.Text = String.Empty) Then
            MessageBox.Show("Bạn vui lòng nhập vào tên tài khoản.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtUserName.Focus()
            Return
        End If
        If (txtPassword.Text = String.Empty) Then
            MessageBox.Show("Bạn vui lòng nhập vào mật khẩu để đăng nhập.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtPassword.Focus()
            Return
        End If
        Dim tkBUS As New TaiKhoanBUS()
        If tkBUS.checkLogin(txtUserName.Text, txtPassword.Text) = String.Empty Then
            MessageBox.Show(String.Format("Đăng nhập thất bại" & vbNewLine & "Bạn vui lòng kiểm tra lại tài khoản và mật khẩu của mình."), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If
        MessageBox.Show("Xin chào " & txtUserName.Text, "Message", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Dim frm As New frmMain()
        frm.lblTK.Text = txtUserName.Text
        frm.Show()
        Me.Close()
    End Sub


    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txtUserName.Clear()
        txtPassword.Clear()
        txtUserName.Focus()
    End Sub
End Class
