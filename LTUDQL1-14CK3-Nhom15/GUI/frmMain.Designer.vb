﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ReadersManagementToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegReaderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentManagementToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BrownToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StaffToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýTàiKhoảnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýNhânViênToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ThôngTinToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblTK = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReadersManagementToolStripMenuItem, Me.DocumentManagementToolStripMenuItem, Me.BrownToolStripMenuItem, Me.StaffToolStripMenuItem, Me.ThôngTinToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(496, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ReadersManagementToolStripMenuItem
        '
        Me.ReadersManagementToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegReaderToolStripMenuItem, Me.ToolStripMenuItem1})
        Me.ReadersManagementToolStripMenuItem.Name = "ReadersManagementToolStripMenuItem"
        Me.ReadersManagementToolStripMenuItem.Size = New System.Drawing.Size(106, 20)
        Me.ReadersManagementToolStripMenuItem.Text = "Quản Lý Độc Giả"
        '
        'RegReaderToolStripMenuItem
        '
        Me.RegReaderToolStripMenuItem.Name = "RegReaderToolStripMenuItem"
        Me.RegReaderToolStripMenuItem.Size = New System.Drawing.Size(117, 22)
        Me.RegReaderToolStripMenuItem.Text = "Đăng Ký"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(117, 22)
        Me.ToolStripMenuItem1.Text = "Quản Lý"
        '
        'DocumentManagementToolStripMenuItem
        '
        Me.DocumentManagementToolStripMenuItem.Name = "DocumentManagementToolStripMenuItem"
        Me.DocumentManagementToolStripMenuItem.Size = New System.Drawing.Size(105, 20)
        Me.DocumentManagementToolStripMenuItem.Text = "Quản Lý Tài Liệu"
        '
        'BrownToolStripMenuItem
        '
        Me.BrownToolStripMenuItem.Name = "BrownToolStripMenuItem"
        Me.BrownToolStripMenuItem.Size = New System.Drawing.Size(118, 20)
        Me.BrownToolStripMenuItem.Text = "Quản Lý Mượn-Trả"
        '
        'StaffToolStripMenuItem
        '
        Me.StaffToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.QuảnLýTàiKhoảnToolStripMenuItem, Me.QuảnLýNhânViênToolStripMenuItem})
        Me.StaffToolStripMenuItem.Name = "StaffToolStripMenuItem"
        Me.StaffToolStripMenuItem.Size = New System.Drawing.Size(71, 20)
        Me.StaffToolStripMenuItem.Text = "NhânViên"
        '
        'QuảnLýTàiKhoảnToolStripMenuItem
        '
        Me.QuảnLýTàiKhoảnToolStripMenuItem.Name = "QuảnLýTàiKhoảnToolStripMenuItem"
        Me.QuảnLýTàiKhoảnToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.QuảnLýTàiKhoảnToolStripMenuItem.Text = "Quản Lý Tài khoản"
        '
        'QuảnLýNhânViênToolStripMenuItem
        '
        Me.QuảnLýNhânViênToolStripMenuItem.Name = "QuảnLýNhânViênToolStripMenuItem"
        Me.QuảnLýNhânViênToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.QuảnLýNhânViênToolStripMenuItem.Text = "Quản Lý nhân viên"
        '
        'ThôngTinToolStripMenuItem
        '
        Me.ThôngTinToolStripMenuItem.Name = "ThôngTinToolStripMenuItem"
        Me.ThôngTinToolStripMenuItem.Size = New System.Drawing.Size(71, 20)
        Me.ThôngTinToolStripMenuItem.Text = "Thông tin"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Purple
        Me.Label1.Location = New System.Drawing.Point(86, 303)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(144, 31)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Xin chào: "
        '
        'lblTK
        '
        Me.lblTK.AutoSize = True
        Me.lblTK.BackColor = System.Drawing.Color.Transparent
        Me.lblTK.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTK.ForeColor = System.Drawing.Color.Purple
        Me.lblTK.Location = New System.Drawing.Point(289, 303)
        Me.lblTK.Name = "lblTK"
        Me.lblTK.Size = New System.Drawing.Size(41, 31)
        Me.lblTK.TabIndex = 1
        Me.lblTK.Text = "..."
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.library_management.My.Resources.Resources.san_go_walnut_tai_ke_sach
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(496, 367)
        Me.Controls.Add(Me.lblTK)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.Text = "Quản Lý Thu Viện Nhóm 15"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents DocumentManagementToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BrownToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StaffToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReadersManagementToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RegReaderToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblTK As System.Windows.Forms.Label
    Friend WithEvents ThôngTinToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuảnLýTàiKhoảnToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuảnLýNhânViênToolStripMenuItem As ToolStripMenuItem
End Class
