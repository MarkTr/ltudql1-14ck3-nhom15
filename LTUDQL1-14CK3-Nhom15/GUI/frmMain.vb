﻿Imports DTO
Imports BUS

Public Class frmMain
    Public Shared NVLogin As String

    Private Sub BrownToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BrownToolStripMenuItem.Click
        Dim frm As New frmMuon_Tra
        Hide()
        frm.ShowDialog()
        Show()
    End Sub

    Private Sub DocumentManagementToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DocumentManagementToolStripMenuItem.Click
        Dim Book = New frmQLSach
        Book.ShowDialog()
    End Sub

    Private Sub RegReaderToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RegReaderToolStripMenuItem.Click
        Dim frm As New frmThemDocGia
        Hide()
        frm.ShowDialog()
        Show()
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click
        Dim frm As New frmQLDocGia
        Hide()
        frm.ShowDialog()
        Show()
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim tkBUS As New TaiKhoanBUS()
        '1 là admin| <> 1 là mod thường
        If (tkBUS.getAccountRole(lblTK.Text) <> 1) Then
            MenuStrip1.Items.Item(3).Visible = False
            MenuStrip1.Items.Item(3).Enabled = False
        End If
        NVLogin = lblTK.Text
    End Sub



    Public Shared Function MaTuTang(ByVal str As String, ByVal LoaiTang As String) As String
        Dim tem As String = "0000"
        If str = String.Empty Then
            'không làm gì hết
        Else
            Dim num As Integer
            num = str.Remove(0, LoaiTang.Length) + 1
            str = num
            tem = tem.Remove(0, str.Length()) + str
        End If
        Return LoaiTang + tem
    End Function

    Private Sub QuảnLýTàiKhoảnToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuảnLýTàiKhoảnToolStripMenuItem.Click

        QLTK.ShowDialog()
    End Sub

    Private Sub QuảnLýNhânViênToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuảnLýNhânViênToolStripMenuItem.Click
        QLThongTinNhanVien.ShowDialog()
    End Sub
End Class