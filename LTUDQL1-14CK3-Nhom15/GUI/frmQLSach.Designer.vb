﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQLSach
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgrBook = New System.Windows.Forms.DataGridView()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtTuKhoa = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbLoai = New System.Windows.Forms.ComboBox()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnDel = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnCapNhat = New System.Windows.Forms.Button()
        Me.grThaoTac = New System.Windows.Forms.GroupBox()
        Me.btnChiTiet = New System.Windows.Forms.Button()
        Me.grChiTietSach = New System.Windows.Forms.GroupBox()
        Me.lblNoidung = New System.Windows.Forms.Label()
        Me.txtNamXB = New System.Windows.Forms.NumericUpDown()
        Me.btnThuNho = New System.Windows.Forms.Button()
        Me.cbLoaiSach = New System.Windows.Forms.ComboBox()
        Me.NudLanXuatban = New System.Windows.Forms.NumericUpDown()
        Me.NudSoLuong = New System.Windows.Forms.NumericUpDown()
        Me.NudSoTrang = New System.Windows.Forms.NumericUpDown()
        Me.btnLamLai = New System.Windows.Forms.Button()
        Me.txtContent = New System.Windows.Forms.TextBox()
        Me.txtNXB = New System.Windows.Forms.TextBox()
        Me.txtTacGia = New System.Windows.Forms.TextBox()
        Me.txtTenSach = New System.Windows.Forms.TextBox()
        Me.txtMaSach = New System.Windows.Forms.TextBox()
        Me.txtISBN = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        CType(Me.dgrBook, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grThaoTac.SuspendLayout()
        Me.grChiTietSach.SuspendLayout()
        CType(Me.txtNamXB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NudLanXuatban, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NudSoLuong, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NudSoTrang, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgrBook
        '
        Me.dgrBook.AllowUserToAddRows = False
        Me.dgrBook.AllowUserToDeleteRows = False
        Me.dgrBook.AllowUserToResizeColumns = False
        Me.dgrBook.AllowUserToResizeRows = False
        Me.dgrBook.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgrBook.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6})
        Me.dgrBook.Location = New System.Drawing.Point(12, 58)
        Me.dgrBook.Name = "dgrBook"
        Me.dgrBook.ReadOnly = True
        Me.dgrBook.RowHeadersVisible = False
        Me.dgrBook.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgrBook.Size = New System.Drawing.Size(627, 227)
        Me.dgrBook.TabIndex = 0
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "MaSach"
        Me.Column2.HeaderText = "Mã Sách"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column3.DataPropertyName = "TenSach"
        Me.Column3.HeaderText = "Tên Sách"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column4.DataPropertyName = "TacGia"
        Me.Column4.HeaderText = "Tác Giả"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column5.DataPropertyName = "LoaiSach"
        Me.Column5.HeaderText = "Loại Sách"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'Column6
        '
        Me.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Column6.DataPropertyName = "NXB"
        Me.Column6.HeaderText = "NXB"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        '
        'txtTuKhoa
        '
        Me.txtTuKhoa.Location = New System.Drawing.Point(68, 23)
        Me.txtTuKhoa.Multiline = True
        Me.txtTuKhoa.Name = "txtTuKhoa"
        Me.txtTuKhoa.Size = New System.Drawing.Size(236, 20)
        Me.txtTuKhoa.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Tìm"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(310, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Theo"
        '
        'cbLoai
        '
        Me.cbLoai.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbLoai.FormattingEnabled = True
        Me.cbLoai.Items.AddRange(New Object() {"Mã Sách", "Tên Sách", "Tác Giả", "Nhà Xuất Bản", "Năm Xuất Bản", "Tất cả"})
        Me.cbLoai.Location = New System.Drawing.Point(343, 23)
        Me.cbLoai.Name = "cbLoai"
        Me.cbLoai.Size = New System.Drawing.Size(195, 21)
        Me.cbLoai.TabIndex = 4
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(544, 21)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(75, 23)
        Me.btnSearch.TabIndex = 5
        Me.btnSearch.Text = "Tìm"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(17, 17)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(75, 33)
        Me.btnAdd.TabIndex = 6
        Me.btnAdd.Text = "&Thêm Sách"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnDel
        '
        Me.btnDel.Location = New System.Drawing.Point(17, 56)
        Me.btnDel.Name = "btnDel"
        Me.btnDel.Size = New System.Drawing.Size(75, 33)
        Me.btnDel.TabIndex = 7
        Me.btnDel.Text = "&Xóa"
        Me.btnDel.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnEdit.Location = New System.Drawing.Point(647, 56)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 31)
        Me.btnEdit.TabIndex = 8
        Me.btnEdit.Text = "&Sửa"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnCapNhat
        '
        Me.btnCapNhat.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnCapNhat.Location = New System.Drawing.Point(647, 19)
        Me.btnCapNhat.Name = "btnCapNhat"
        Me.btnCapNhat.Size = New System.Drawing.Size(75, 31)
        Me.btnCapNhat.TabIndex = 9
        Me.btnCapNhat.Text = "&Cập Nhật"
        Me.btnCapNhat.UseVisualStyleBackColor = True
        '
        'grThaoTac
        '
        Me.grThaoTac.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grThaoTac.Controls.Add(Me.btnAdd)
        Me.grThaoTac.Controls.Add(Me.btnDel)
        Me.grThaoTac.Controls.Add(Me.btnChiTiet)
        Me.grThaoTac.Location = New System.Drawing.Point(645, 58)
        Me.grThaoTac.Name = "grThaoTac"
        Me.grThaoTac.Size = New System.Drawing.Size(107, 138)
        Me.grThaoTac.TabIndex = 10
        Me.grThaoTac.TabStop = False
        Me.grThaoTac.Text = "Thao Tác"
        '
        'btnChiTiet
        '
        Me.btnChiTiet.Location = New System.Drawing.Point(17, 95)
        Me.btnChiTiet.Name = "btnChiTiet"
        Me.btnChiTiet.Size = New System.Drawing.Size(75, 31)
        Me.btnChiTiet.TabIndex = 8
        Me.btnChiTiet.Text = "&Chi Tiết"
        Me.btnChiTiet.UseVisualStyleBackColor = True
        '
        'grChiTietSach
        '
        Me.grChiTietSach.Controls.Add(Me.lblNoidung)
        Me.grChiTietSach.Controls.Add(Me.txtNamXB)
        Me.grChiTietSach.Controls.Add(Me.btnThuNho)
        Me.grChiTietSach.Controls.Add(Me.btnCapNhat)
        Me.grChiTietSach.Controls.Add(Me.cbLoaiSach)
        Me.grChiTietSach.Controls.Add(Me.NudLanXuatban)
        Me.grChiTietSach.Controls.Add(Me.btnEdit)
        Me.grChiTietSach.Controls.Add(Me.NudSoLuong)
        Me.grChiTietSach.Controls.Add(Me.NudSoTrang)
        Me.grChiTietSach.Controls.Add(Me.btnLamLai)
        Me.grChiTietSach.Controls.Add(Me.txtContent)
        Me.grChiTietSach.Controls.Add(Me.txtNXB)
        Me.grChiTietSach.Controls.Add(Me.txtTacGia)
        Me.grChiTietSach.Controls.Add(Me.txtTenSach)
        Me.grChiTietSach.Controls.Add(Me.txtMaSach)
        Me.grChiTietSach.Controls.Add(Me.txtISBN)
        Me.grChiTietSach.Controls.Add(Me.Label13)
        Me.grChiTietSach.Controls.Add(Me.Label7)
        Me.grChiTietSach.Controls.Add(Me.Label12)
        Me.grChiTietSach.Controls.Add(Me.Label11)
        Me.grChiTietSach.Controls.Add(Me.Label10)
        Me.grChiTietSach.Controls.Add(Me.Label4)
        Me.grChiTietSach.Controls.Add(Me.Label9)
        Me.grChiTietSach.Controls.Add(Me.Label3)
        Me.grChiTietSach.Controls.Add(Me.Label8)
        Me.grChiTietSach.Controls.Add(Me.Label5)
        Me.grChiTietSach.Controls.Add(Me.Label6)
        Me.grChiTietSach.Location = New System.Drawing.Point(15, 308)
        Me.grChiTietSach.Name = "grChiTietSach"
        Me.grChiTietSach.Size = New System.Drawing.Size(734, 228)
        Me.grChiTietSach.TabIndex = 11
        Me.grChiTietSach.TabStop = False
        Me.grChiTietSach.Text = "Chi Tiết Sách"
        '
        'lblNoidung
        '
        Me.lblNoidung.AutoSize = True
        Me.lblNoidung.Location = New System.Drawing.Point(568, 212)
        Me.lblNoidung.Name = "lblNoidung"
        Me.lblNoidung.Size = New System.Drawing.Size(36, 13)
        Me.lblNoidung.TabIndex = 44
        Me.lblNoidung.Text = "0/500"
        '
        'txtNamXB
        '
        Me.txtNamXB.Location = New System.Drawing.Point(111, 136)
        Me.txtNamXB.Maximum = New Decimal(New Integer() {2017, 0, 0, 0})
        Me.txtNamXB.Name = "txtNamXB"
        Me.txtNamXB.Size = New System.Drawing.Size(128, 20)
        Me.txtNamXB.TabIndex = 43
        '
        'btnThuNho
        '
        Me.btnThuNho.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnThuNho.Location = New System.Drawing.Point(647, 132)
        Me.btnThuNho.Name = "btnThuNho"
        Me.btnThuNho.Size = New System.Drawing.Size(75, 31)
        Me.btnThuNho.TabIndex = 9
        Me.btnThuNho.Text = "&Thu Nhỏ"
        Me.btnThuNho.UseVisualStyleBackColor = True
        '
        'cbLoaiSach
        '
        Me.cbLoaiSach.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbLoaiSach.FormattingEnabled = True
        Me.cbLoaiSach.Items.AddRange(New Object() {"MalOaiSach"})
        Me.cbLoaiSach.Location = New System.Drawing.Point(382, 78)
        Me.cbLoaiSach.Name = "cbLoaiSach"
        Me.cbLoaiSach.Size = New System.Drawing.Size(129, 21)
        Me.cbLoaiSach.TabIndex = 42
        '
        'NudLanXuatban
        '
        Me.NudLanXuatban.Location = New System.Drawing.Point(381, 104)
        Me.NudLanXuatban.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.NudLanXuatban.Name = "NudLanXuatban"
        Me.NudLanXuatban.Size = New System.Drawing.Size(128, 20)
        Me.NudLanXuatban.TabIndex = 41
        '
        'NudSoLuong
        '
        Me.NudSoLuong.Location = New System.Drawing.Point(381, 25)
        Me.NudSoLuong.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.NudSoLuong.Name = "NudSoLuong"
        Me.NudSoLuong.Size = New System.Drawing.Size(128, 20)
        Me.NudSoLuong.TabIndex = 40
        '
        'NudSoTrang
        '
        Me.NudSoTrang.Location = New System.Drawing.Point(111, 162)
        Me.NudSoTrang.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.NudSoTrang.Name = "NudSoTrang"
        Me.NudSoTrang.Size = New System.Drawing.Size(128, 20)
        Me.NudSoTrang.TabIndex = 39
        '
        'btnLamLai
        '
        Me.btnLamLai.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnLamLai.Location = New System.Drawing.Point(647, 93)
        Me.btnLamLai.Name = "btnLamLai"
        Me.btnLamLai.Size = New System.Drawing.Size(75, 31)
        Me.btnLamLai.TabIndex = 38
        Me.btnLamLai.Text = "&Làm Lại"
        Me.btnLamLai.UseVisualStyleBackColor = True
        '
        'txtContent
        '
        Me.txtContent.Location = New System.Drawing.Point(381, 138)
        Me.txtContent.Multiline = True
        Me.txtContent.Name = "txtContent"
        Me.txtContent.Size = New System.Drawing.Size(223, 68)
        Me.txtContent.TabIndex = 36
        '
        'txtNXB
        '
        Me.txtNXB.Location = New System.Drawing.Point(109, 105)
        Me.txtNXB.Name = "txtNXB"
        Me.txtNXB.Size = New System.Drawing.Size(130, 20)
        Me.txtNXB.TabIndex = 35
        '
        'txtTacGia
        '
        Me.txtTacGia.Location = New System.Drawing.Point(109, 77)
        Me.txtTacGia.Name = "txtTacGia"
        Me.txtTacGia.Size = New System.Drawing.Size(130, 20)
        Me.txtTacGia.TabIndex = 34
        '
        'txtTenSach
        '
        Me.txtTenSach.Location = New System.Drawing.Point(109, 51)
        Me.txtTenSach.Name = "txtTenSach"
        Me.txtTenSach.Size = New System.Drawing.Size(130, 20)
        Me.txtTenSach.TabIndex = 33
        '
        'txtMaSach
        '
        Me.txtMaSach.Enabled = False
        Me.txtMaSach.Location = New System.Drawing.Point(110, 25)
        Me.txtMaSach.Name = "txtMaSach"
        Me.txtMaSach.ReadOnly = True
        Me.txtMaSach.Size = New System.Drawing.Size(130, 20)
        Me.txtMaSach.TabIndex = 31
        '
        'txtISBN
        '
        Me.txtISBN.Location = New System.Drawing.Point(381, 51)
        Me.txtISBN.Name = "txtISBN"
        Me.txtISBN.Size = New System.Drawing.Size(130, 20)
        Me.txtISBN.TabIndex = 32
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(15, 164)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(51, 13)
        Me.Label13.TabIndex = 29
        Me.Label13.Text = "Số Trang"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(288, 162)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 13)
        Me.Label7.TabIndex = 30
        Me.Label7.Text = "Tóm Tắt"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(16, 138)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(76, 13)
        Me.Label12.TabIndex = 28
        Me.Label12.Text = "Năm Xuất Bản"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(16, 112)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(74, 13)
        Me.Label11.TabIndex = 27
        Me.Label11.Text = "Nhà Xuất Bản"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(16, 83)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(45, 13)
        Me.Label10.TabIndex = 26
        Me.Label10.Text = "Tác Giả"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(288, 111)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 13)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "Lần Tái Bản"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(16, 57)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 13)
        Me.Label9.TabIndex = 21
        Me.Label9.Text = "Tên Sách"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(288, 83)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 13)
        Me.Label3.TabIndex = 23
        Me.Label3.Text = "Loại Sách"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(16, 28)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Mã Sách"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(288, 57)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(32, 13)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "ISBN"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(288, 28)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 13)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "Số Lượng"
        '
        'frmQLSach
        '
        Me.AcceptButton = Me.btnSearch
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(761, 545)
        Me.Controls.Add(Me.grChiTietSach)
        Me.Controls.Add(Me.grThaoTac)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.cbLoai)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtTuKhoa)
        Me.Controls.Add(Me.dgrBook)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmQLSach"
        Me.Text = "Quản Lý Sách"
        CType(Me.dgrBook, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grThaoTac.ResumeLayout(False)
        Me.grChiTietSach.ResumeLayout(False)
        Me.grChiTietSach.PerformLayout()
        CType(Me.txtNamXB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NudLanXuatban, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NudSoLuong, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NudSoTrang, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgrBook As System.Windows.Forms.DataGridView
    Friend WithEvents txtTuKhoa As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cbLoai As System.Windows.Forms.ComboBox
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnDel As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnCapNhat As Button
    Friend WithEvents grThaoTac As GroupBox
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents grChiTietSach As GroupBox
    Friend WithEvents txtNamXB As NumericUpDown
    Friend WithEvents cbLoaiSach As ComboBox
    Friend WithEvents NudLanXuatban As NumericUpDown
    Friend WithEvents NudSoLuong As NumericUpDown
    Friend WithEvents NudSoTrang As NumericUpDown
    Friend WithEvents btnLamLai As Button
    Friend WithEvents txtContent As TextBox
    Friend WithEvents txtNXB As TextBox
    Friend WithEvents txtTacGia As TextBox
    Friend WithEvents txtTenSach As TextBox
    Friend WithEvents txtMaSach As TextBox
    Friend WithEvents txtISBN As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents btnChiTiet As Button
    Friend WithEvents btnThuNho As Button
    Friend WithEvents lblNoidung As Label
End Class
