﻿Imports BUS
Imports DTO
Public Class frmQLSach
    Dim Temp As SachDTO
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim frmAddBook = New frmThemSach
        frmAddBook.ShowDialog()

    End Sub

    Private Sub frmQLSach_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MaximizeBox = False
        MinimizeBox = False
        Height -= grChiTietSach.Height + 10
        grChiTietSach.Visible = False
        cbLoaiSach.DataSource = SachBUS.loadcbb()
        cbLoaiSach.DisplayMember = "LoaiSach"
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim key As String = "%" + txtTuKhoa.Text + "%"
        If cbLoai.SelectedIndex = -1 Then
            MessageBox.Show("Vui lòng chọn tiêu chí tìm kiếm!!", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            dgrBook.DataSource = SachBUS.LoadBook(cbLoai.SelectedIndex, key)
            If (dgrBook.RowCount <= 0) Then
                MessageBox.Show("Không tìm thấy kết quả nào", "Tìm Lại!!!", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If

    End Sub

    Private Sub btnDel_Click(sender As Object, e As EventArgs) Handles btnDel.Click
        Dim choos As Integer
        Try
            choos = dgrBook.CurrentRow.Index
            If MessageBox.Show("bạn chắc chắn muốn xóa " + dgrBook.Rows(choos).Cells(0).Value.ToString() + "?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                SachBUS.xoaSach(dgrBook.Rows(choos).Cells(0).Value.ToString())
            End If
        Catch ex As Exception
            MessageBox.Show("Vui lòng chọn sách muốn xóa!!!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Private Sub btnThuNho_Click(sender As Object, e As EventArgs) Handles btnThuNho.Click
        Height -= grChiTietSach.Height
        grChiTietSach.Visible = False
    End Sub

    Private Sub btnChiTiet_Click(sender As Object, e As EventArgs) Handles btnChiTiet.Click

        If cbLoai.SelectedIndex = -1 Then
            MessageBox.Show("Bạn chưa tìm kiếm sách sao có chi tiết???", "Lưu ý", MessageBoxButtons.OK, MessageBoxIcon.Question)
        Else
            If grChiTietSach.Visible = False Then
                Height += grChiTietSach.Height
                grChiTietSach.Visible = True
                AnHien(False)
            End If
        End If



    End Sub
    Private Sub BLind()
        Dim choos
        Try
            choos = dgrBook.CurrentRow.Index
            Temp = SachBUS.Load1Sach(dgrBook.Rows(choos).Cells(0).Value.ToString())
        Catch ex As Exception
            choos = 0
        End Try
        txtISBN.Text = Temp.ISBN
        NudLanXuatban.Value = Temp.LanXB
        NudSoLuong.Value = Temp.SoLuong
        NudSoTrang.Value = Temp.SoTrang
        txtTacGia.Text = Temp.TacGia
        txtTenSach.Text = Temp.TenSach
        txtMaSach.Text = Temp.MaSach
        cbLoaiSach.SelectedIndex = Temp.MaloaiSach
        txtNamXB.Value = Temp.NamXB
        txtContent.Text = Temp.Noidungtomtat
        txtNXB.Text = Temp.NXB

    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        AnHien(True)

    End Sub

    Private Sub btnLamLai_Click(sender As Object, e As EventArgs) Handles btnLamLai.Click
        AnHien(False)
        BLind()

    End Sub

    Private Sub dgrBook_SelectionChanged(sender As Object, e As EventArgs) Handles dgrBook.SelectionChanged
        BLind()
    End Sub
    Private Sub AnHien(ByVal Bol As Boolean)
        txtContent.Enabled = Bol
        txtISBN.Enabled = Bol
        txtNamXB.Enabled = Bol
        txtNXB.Enabled = Bol
        txtTacGia.Enabled = Bol
        txtTenSach.Enabled = Bol
        NudLanXuatban.Enabled = Bol
        NudSoLuong.Enabled = Bol
        NudSoTrang.Enabled = Bol
        cbLoaiSach.Enabled = Bol
        btnCapNhat.Enabled = Bol
        btnLamLai.Enabled = Bol
    End Sub

    Private Sub btnCapNhat_Click(sender As Object, e As EventArgs) Handles btnCapNhat.Click
        Dim temp As New SachDTO
        With temp
            .ISBN = txtISBN.Text
            .LanXB = NudLanXuatban.Value
            .MaloaiSach = cbLoaiSach.SelectedIndex
            .Noidungtomtat = txtContent.Text
            .NXB = txtNXB.Text
            .SoLuong = NudSoLuong.Value
            .SoTrang = NudSoTrang.Value
            .TacGia = txtTacGia.Text
            .TenSach = txtTenSach.Text
            .NamXB = txtNamXB.Value
            .MaSach = txtMaSach.Text
        End With
        SachBUS.CapNhatSach(temp)
        AnHien(False)
    End Sub

    Private Sub txtContent_TextChanged(sender As Object, e As EventArgs) Handles txtContent.TextChanged
        lblNoidung.Text = txtContent.Text.Length.ToString + "/500"
    End Sub

End Class