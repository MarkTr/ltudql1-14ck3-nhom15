﻿Imports DTO
Imports BUS
Imports System.IO
Public Class frmThemDocGia
    Dim TemDG As New DocGiaDTO
    Sub ReFes()
        txtAdr.Clear()
        dtpickExp.ResetText()
        txtIDcard.Clear()
        txtIDReader.Clear()
        txtName.Clear()
        dtPick.ResetText()
        cbSex.SelectedIndex = -1
        cbType.SelectedIndex = -1
        dtpickExp.ResetText()

        If (grRole.Visible) Then
            grRole.Visible = False
            Width -= grRole.Width
        End If
        'lấy 2 chuỗi là chuỗi lấy từ trong db ra và 1 loại mà ta viết cho nó... VD là DG hoặc BOOK
        txtIDReader.Text = frmMain.MaTuTang(DocGiaBUS.LoadIDReader(), "DG")

    End Sub
    Private Sub btnRefesh_Click(sender As Object, e As EventArgs) Handles btnRefesh.Click
        ReFes()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()

    End Sub

    Private Sub frmRegReader_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ControlBox = False
        grRole.Visible = False
        Width -= grRole.Width
        ReFes()

    End Sub

    Private Sub btnReg_Click(sender As Object, e As EventArgs) Handles btnReg.Click
        If checkdata() Then
            With TemDG
                .MaDG = txtIDReader.Text
                .TenDG = txtName.Text
                .HSD = dtpickExp.Text
                .NgaySinh = dtPick.Text
                .CMND = txtIDcard.Text
                .DiaChi = txtAdr.Text
                .GioiTinh = cbSex.Text
                .Fee = txtFee.Text
                .Role_ID = cbType.SelectedIndex
            End With

            DocGiaBUS.AddReaderBUS(TemDG)
            ReFes()

            'Dim strinlog As String = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & vbNewLine & "Thêm Đọc giả:" & TemDG.TenDG
            'Dim NameFile As String = "Diary.txt"

            'If (File.Exists(NameFile)) Then
            '    Dim ghiTiep As StreamWriter = File.AppendText(NameFile)
            '    ghiTiep.WriteLine(strinlog)
            '    ghiTiep.Close()
            'Else
            '    Dim work As New StreamWriter(NameFile, False)
            '    work.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") & vbNewLine & "Thêm Đọc giả:" & TemDG.TenDG)
            '    work.Close()
            'End If
        End If



    End Sub

    Private Sub cbType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbType.SelectedIndexChanged
        dtpickExp.Value = Now
        dtpickExp.Value = dtpickExp.Value.AddMonths(3)

        If grRole.Visible = False Then
            grRole.Visible = True
            Width += grRole.Width
        End If

        If (cbType.Text = "V.I.P") Then

            txtFee.Text = "2000"
            RoleNgay.Text = "14"
            RoleSach.Text = "5"
            RoleSP.Text = "True"
        Else
            txtFee.Text = "1000"
            RoleNgay.Text = "7"
            RoleSach.Text = "3"
            RoleSP.Text = "False"
        End If


    End Sub

    Private Sub btnRole_Click(sender As Object, e As EventArgs) Handles btnRole.Click
        grRole.Visible = False
        Width -= grRole.Width
    End Sub

    Function checkdata() As Boolean
        Dim strE As String = String.Empty
        If txtAdr.Text = "" Then
            strE = strE + vbLf + "**Địa Chỉ Khách Hàng"
        End If


        If txtIDcard.Text = "" Then
            strE = strE + vbLf + "**Số chừng minh nhân dân"
        End If
        If txtName.Text = "" Then
            strE = strE + vbLf + "**Tên độc giả"
        End If
        If cbSex.SelectedIndex = -1 Then
            strE = strE + vbLf + "**Giới tính"
        End If
        If cbType.SelectedIndex = -1 Then
            strE = strE + vbLf + "**Loại độc giả"
        End If
        If DateTime.Now.Year - dtPick.Value.Year < 15 Then
            strE = strE + vbLf + "**Ngày Sinh"
        End If
        If strE <> String.Empty Then
            MessageBox.Show("Bạn phải điền đầy đủ các thông tin sau: " + vbLf + strE, "Lưu ý!!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End If
        Return True
    End Function
End Class