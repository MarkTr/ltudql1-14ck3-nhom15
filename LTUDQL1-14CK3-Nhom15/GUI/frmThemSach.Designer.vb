﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmThemSach
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtISBN = New System.Windows.Forms.TextBox()
        Me.txtContent = New System.Windows.Forms.TextBox()
        Me.btnThem = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtMaSach = New System.Windows.Forms.TextBox()
        Me.txtTenSach = New System.Windows.Forms.TextBox()
        Me.txtTacGia = New System.Windows.Forms.TextBox()
        Me.txtNXB = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.NudSoTrang = New System.Windows.Forms.NumericUpDown()
        Me.NudSoLuong = New System.Windows.Forms.NumericUpDown()
        Me.NudLanXuatban = New System.Windows.Forms.NumericUpDown()
        Me.cbLoaiSach = New System.Windows.Forms.ComboBox()
        Me.btnLamLai = New System.Windows.Forms.Button()
        Me.lblTB = New System.Windows.Forms.Label()
        Me.txtNamXB = New System.Windows.Forms.NumericUpDown()
        Me.lblNoidung = New System.Windows.Forms.Label()
        CType(Me.NudSoTrang, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NudSoLuong, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NudLanXuatban, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNamXB, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(311, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Số Lượng"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(311, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "ISBN"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(311, 76)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Loại Sách"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(310, 146)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Tóm Tắt"
        '
        'txtISBN
        '
        Me.txtISBN.Location = New System.Drawing.Point(404, 44)
        Me.txtISBN.Name = "txtISBN"
        Me.txtISBN.Size = New System.Drawing.Size(130, 20)
        Me.txtISBN.TabIndex = 8
        '
        'txtContent
        '
        Me.txtContent.Location = New System.Drawing.Point(404, 143)
        Me.txtContent.MaxLength = 500
        Me.txtContent.Multiline = True
        Me.txtContent.Name = "txtContent"
        Me.txtContent.Size = New System.Drawing.Size(164, 107)
        Me.txtContent.TabIndex = 12
        '
        'btnThem
        '
        Me.btnThem.Location = New System.Drawing.Point(132, 208)
        Me.btnThem.Name = "btnThem"
        Me.btnThem.Size = New System.Drawing.Size(75, 23)
        Me.btnThem.TabIndex = 14
        Me.btnThem.Text = "Thêm"
        Me.btnThem.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(39, 21)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Mã Sách"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(39, 50)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 13)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Tên Sách"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(39, 76)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(45, 13)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Tác Giả"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(39, 105)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(74, 13)
        Me.Label11.TabIndex = 3
        Me.Label11.Text = "Nhà Xuất Bản"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(39, 131)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(76, 13)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "Năm Xuất Bản"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(38, 157)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(51, 13)
        Me.Label13.TabIndex = 5
        Me.Label13.Text = "Số Trang"
        '
        'txtMaSach
        '
        Me.txtMaSach.Enabled = False
        Me.txtMaSach.Location = New System.Drawing.Point(133, 18)
        Me.txtMaSach.Name = "txtMaSach"
        Me.txtMaSach.ReadOnly = True
        Me.txtMaSach.Size = New System.Drawing.Size(130, 20)
        Me.txtMaSach.TabIndex = 7
        '
        'txtTenSach
        '
        Me.txtTenSach.Location = New System.Drawing.Point(132, 44)
        Me.txtTenSach.Name = "txtTenSach"
        Me.txtTenSach.Size = New System.Drawing.Size(130, 20)
        Me.txtTenSach.TabIndex = 8
        '
        'txtTacGia
        '
        Me.txtTacGia.Location = New System.Drawing.Point(132, 70)
        Me.txtTacGia.Name = "txtTacGia"
        Me.txtTacGia.Size = New System.Drawing.Size(130, 20)
        Me.txtTacGia.TabIndex = 9
        '
        'txtNXB
        '
        Me.txtNXB.Location = New System.Drawing.Point(132, 98)
        Me.txtNXB.Name = "txtNXB"
        Me.txtNXB.Size = New System.Drawing.Size(130, 20)
        Me.txtNXB.TabIndex = 10
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(311, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Lần Tái Bản"
        '
        'NudSoTrang
        '
        Me.NudSoTrang.Location = New System.Drawing.Point(134, 155)
        Me.NudSoTrang.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.NudSoTrang.Name = "NudSoTrang"
        Me.NudSoTrang.Size = New System.Drawing.Size(128, 20)
        Me.NudSoTrang.TabIndex = 16
        '
        'NudSoLuong
        '
        Me.NudSoLuong.Location = New System.Drawing.Point(404, 18)
        Me.NudSoLuong.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.NudSoLuong.Name = "NudSoLuong"
        Me.NudSoLuong.Size = New System.Drawing.Size(128, 20)
        Me.NudSoLuong.TabIndex = 16
        '
        'NudLanXuatban
        '
        Me.NudLanXuatban.Location = New System.Drawing.Point(404, 97)
        Me.NudLanXuatban.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.NudLanXuatban.Name = "NudLanXuatban"
        Me.NudLanXuatban.Size = New System.Drawing.Size(128, 20)
        Me.NudLanXuatban.TabIndex = 16
        '
        'cbLoaiSach
        '
        Me.cbLoaiSach.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbLoaiSach.FormattingEnabled = True
        Me.cbLoaiSach.Items.AddRange(New Object() {"MalOaiSach"})
        Me.cbLoaiSach.Location = New System.Drawing.Point(405, 71)
        Me.cbLoaiSach.Name = "cbLoaiSach"
        Me.cbLoaiSach.Size = New System.Drawing.Size(129, 21)
        Me.cbLoaiSach.TabIndex = 17
        '
        'btnLamLai
        '
        Me.btnLamLai.Location = New System.Drawing.Point(227, 208)
        Me.btnLamLai.Name = "btnLamLai"
        Me.btnLamLai.Size = New System.Drawing.Size(75, 23)
        Me.btnLamLai.TabIndex = 14
        Me.btnLamLai.Text = "Làm Lại"
        Me.btnLamLai.UseVisualStyleBackColor = True
        '
        'lblTB
        '
        Me.lblTB.AutoSize = True
        Me.lblTB.ForeColor = System.Drawing.Color.Maroon
        Me.lblTB.Location = New System.Drawing.Point(401, 120)
        Me.lblTB.Name = "lblTB"
        Me.lblTB.Size = New System.Drawing.Size(171, 13)
        Me.lblTB.TabIndex = 2
        Me.lblTB.Text = "Nếu là xuất bản lần đầu thì để là 0"
        Me.lblTB.Visible = False
        '
        'txtNamXB
        '
        Me.txtNamXB.Location = New System.Drawing.Point(134, 129)
        Me.txtNamXB.Maximum = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtNamXB.Name = "txtNamXB"
        Me.txtNamXB.Size = New System.Drawing.Size(128, 20)
        Me.txtNamXB.TabIndex = 18
        '
        'lblNoidung
        '
        Me.lblNoidung.AutoSize = True
        Me.lblNoidung.Location = New System.Drawing.Point(521, 253)
        Me.lblNoidung.Name = "lblNoidung"
        Me.lblNoidung.Size = New System.Drawing.Size(36, 13)
        Me.lblNoidung.TabIndex = 19
        Me.lblNoidung.Text = "0/500"
        '
        'frmThemSach
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(629, 274)
        Me.Controls.Add(Me.lblNoidung)
        Me.Controls.Add(Me.txtNamXB)
        Me.Controls.Add(Me.cbLoaiSach)
        Me.Controls.Add(Me.NudLanXuatban)
        Me.Controls.Add(Me.NudSoLuong)
        Me.Controls.Add(Me.NudSoTrang)
        Me.Controls.Add(Me.btnLamLai)
        Me.Controls.Add(Me.btnThem)
        Me.Controls.Add(Me.txtContent)
        Me.Controls.Add(Me.txtNXB)
        Me.Controls.Add(Me.txtTacGia)
        Me.Controls.Add(Me.txtTenSach)
        Me.Controls.Add(Me.txtMaSach)
        Me.Controls.Add(Me.txtISBN)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lblTB)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmThemSach"
        Me.Text = "Thêm Sách"
        CType(Me.NudSoTrang, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NudSoLuong, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NudLanXuatban, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNamXB, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtISBN As System.Windows.Forms.TextBox
    Friend WithEvents txtContent As System.Windows.Forms.TextBox
    Friend WithEvents btnThem As System.Windows.Forms.Button
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents txtMaSach As TextBox
    Friend WithEvents txtTenSach As TextBox
    Friend WithEvents txtTacGia As TextBox
    Friend WithEvents txtNXB As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents NudSoTrang As NumericUpDown
    Friend WithEvents NudSoLuong As NumericUpDown
    Friend WithEvents NudLanXuatban As NumericUpDown
    Friend WithEvents cbLoaiSach As ComboBox
    Friend WithEvents btnLamLai As Button
    Friend WithEvents lblTB As Label
    Friend WithEvents txtNamXB As NumericUpDown
    Friend WithEvents lblNoidung As Label
End Class
