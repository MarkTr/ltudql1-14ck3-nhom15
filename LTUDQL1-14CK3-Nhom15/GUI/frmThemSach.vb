﻿Imports BUS
Imports DTO
Public Class frmThemSach
    Dim a As New frmThemDocGia
    Sub LoadMaSach()
        txtContent.Clear()
        txtISBN.Clear()
        txtMaSach.Clear()
        txtNamXB.ResetText()
        txtNamXB.Value = 0
        txtNamXB.Maximum = Date.Now.Year.ToString
        txtNXB.Clear()
        txtTacGia.Clear()
        txtTenSach.Clear()
        cbLoaiSach.ResetText()
        NudLanXuatban.ResetText()
        NudLanXuatban.Value = 0
        NudSoLuong.ResetText()
        NudSoLuong.Value = 0
        NudSoTrang.ResetText()
        NudSoTrang.Value = 0
        txtMaSach.Text = frmMain.MaTuTang(SachBUS.LayMaSach(), "BOOK") 'ghép lại với BOOK
        txtTenSach.Focus()

        lblTB.Visible = False
        cbLoaiSach.DataSource = SachBUS.loadcbb()
        cbLoaiSach.DisplayMember = "LoaiSach"

    End Sub

    Private Sub frmThemSach_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadMaSach()
    End Sub

    Private Sub btnThem_Click(sender As Object, e As EventArgs) Handles btnThem.Click
        If (checkdataSach()) Then
            Dim tem As New SachDTO
            With tem
                .Noidungtomtat = txtContent.Text
                .ISBN = txtISBN.Text
                .MaSach = txtMaSach.Text
                .NamXB = txtNamXB.Text
                .NXB = txtNXB.Text
                .TacGia = txtTacGia.Text
                .TenSach = txtTenSach.Text
                .MaloaiSach = cbLoaiSach.SelectedIndex
                .LanXB = NudLanXuatban.Text
                .SoLuong = NudSoLuong.Text
                .SoTrang = NudSoTrang.Text
            End With

            SachBUS.ThemSach(tem)
            LoadMaSach()
        End If
    End Sub
    Function checkdataSach() As Boolean
        If txtMaSach.Text = "" Then
            Return False
        End If
        Dim strE As String = String.Empty
        If txtTenSach.Text = "" Then
            strE = strE + vbLf + "**Tên Sách"
        End If

        If txtTacGia.Text = "" Then
            strE = strE + vbLf + "**Tên Tác giả"
        End If

        If txtNXB.Text = "" Then
            strE = strE + vbLf + "**Nhà xuất Bản"
        End If

        If cbLoaiSach.SelectedIndex = -1 Then
            strE = strE + vbLf + "**Loại Sách"
        End If

        If txtNamXB.Text = "" Then
            strE = strE + vbLf + "**Năm Xuất Bản"
        End If

        If txtContent.Text = "" Then
            strE = strE + vbLf + "**Tóm tắt nội dung"
        End If

        If txtISBN.Text = "" Then
            strE = strE + vbLf + "**Mã ISBN (Mã vạch)"
        End If

        If NudSoTrang.Value = 0 Then
            strE = strE + vbLf + "**Số trang >0"
        End If

        If NudSoLuong.Value = 0 Then
            strE = strE + vbLf + "**Số lượng >0"
        End If

        If strE <> String.Empty Then
            MessageBox.Show("Bạn phải điền đầy đủ các thông tin sau: " + vbLf + strE, "Lưu ý!!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        End If

        Return True

    End Function

    Private Sub NudLanXuatban_ValueChanged(sender As Object, e As EventArgs) Handles NudLanXuatban.ValueChanged
        lblTB.Visible = True
    End Sub

    Private Sub btnLamLai_Click(sender As Object, e As EventArgs) Handles btnLamLai.Click
        LoadMaSach()
    End Sub

    Private Sub txtISBN_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtISBN.KeyPress
        If Not (Char.IsDigit(e.KeyChar) OrElse Char.IsControl(e.KeyChar)) Then
            Beep()
            e.Handled = True
            MessageBox.Show("Chỉ có thể nhập số", "Chú ý đừng có tình", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If

    End Sub

    Private Sub txtContent_TextChanged(sender As Object, e As EventArgs) Handles txtContent.TextChanged
        lblNoidung.Text = txtContent.Text.Length.ToString + "/500"
    End Sub
End Class