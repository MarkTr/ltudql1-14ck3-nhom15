﻿Imports DAO
Imports DTO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Public Class DocGiaBUS
    Public Shared Function AddReaderBUS(ByVal Reader As DocGiaDTO)
        If DocGiaDAO.Insert_DocGia(Reader) Then
            Return True
        Else
            Return False
        End If

    End Function
    Public Shared Function Loader(ByVal paramet As String, ByVal i As Integer) As DataTable

        Return DocGiaDAO.LoadRead(paramet, i)
    End Function

    Public Shared Function LoadIDReader() As String
        Return DocGiaDAO.LoadSacle()
    End Function

    Public Shared Function LoadInfo(ByVal strS As String) As DocGiaDTO
        Return DocGiaDAO.LoadOneReader(strS)
    End Function

    Public Shared Function XoaDG(ByVal MADG As String)
        If DocGiaDAO.XoaDG(MADG) = True Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Shared Function Update_readerBUS(ByVal Reader As DocGiaDTO)

        If DocGiaDAO.Update_DocGia(Reader) Then
            Return True
        Else
            Return False
        End If
    End Function
End Class
