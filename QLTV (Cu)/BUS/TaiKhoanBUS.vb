﻿Imports DAO
Imports DTO
Public Class TaiKhoanBUS

    Public Function insert(ByVal tk As TaiKhoanDTO)
        Dim tkDAO As New TaiKhoanDAO()
        Return tkDAO.insert(tk)
    End Function

    Public Function checkLogin(ByVal tk As String, ByVal mk As String)
        Dim tkDAO As New TaiKhoanDAO()
        Return tkDAO.checkLogin(tk, mk)
    End Function
    Public Function Update(ByVal tk As TaiKhoanDTO)
        Dim tkDAO As New TaiKhoanDAO()
        Return tkDAO.insert(tk)
    End Function
    Public Function Delete(ByVal tk As String)
        Dim tkDAO As New TaiKhoanDAO()
        Return tkDAO.Delete(tk)
    End Function

    Public Function getByUsername(ByVal tk As String)
        Dim tkDAO As New TaiKhoanDAO()
        Return tkDAO.getByUserName(tk)
    End Function

    Public Function getAccountRole(ByVal tk As String)
        Dim tkDAO As New TaiKhoanDAO()
        Return tkDAO.getAccountRole(tk)
    End Function

End Class
