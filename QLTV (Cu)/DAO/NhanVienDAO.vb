﻿Imports DTO
Imports System.Data.SqlClient
Imports System.Data.OleDb
Public Class NhanVienDAO

    Public Function getAll() As DataTable
        Dim cmd As New SqlCommand("SELECT * FROM NhanVien")
        Return DataProvider.LoaderFromDATA(cmd)
    End Function

    Public Function getById(ByVal ID As String)
        Dim query As String = String.Format("SELECT * FROM NhanVien WHERE ID = '{0}'", ID)
        Dim cmd As New SqlCommand(query)
        Try
            Return DataProvider.LoaderFromDATA(cmd)
        Catch ex As Exception
            Return vbNull
        End Try

    End Function

End Class
