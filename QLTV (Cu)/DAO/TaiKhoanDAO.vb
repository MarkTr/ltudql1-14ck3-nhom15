﻿Imports DTO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Public Class TaiKhoanDAO
    Public Function getAll() As DataTable
        Dim query As New SqlCommand("SELECT * FROM TaiKhoan")
        Return DataProvider.LoaderFromDATA(query)
    End Function

    Public Function insert(ByVal tk As TaiKhoanDTO) As Integer
        Dim query As String = String.Format("INSERT INTO TaiKhoan VALUES('@username',@password',@MSNVOF, @role")
        Dim cmd As New SqlCommand(query) With {
            .CommandType = CommandType.Text}
        Try
            cmd.Parameters.Add("@username", SqlDbType.NVarChar).Value = tk.TaiKhoan
            cmd.Parameters.Add("@password", SqlDbType.NVarChar).Value = tk.MatKhau
            cmd.Parameters.Add("@MSNVOF", SqlDbType.NVarChar).Value = tk.MSNV
            cmd.Parameters.Add("@role", SqlDbType.Int).Value = 0
            Return cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
            Return 0
        End Try
    End Function

    Public Function Update(ByVal tk As TaiKhoanDTO)
        Dim query As String = String.Format("UPDATE dbo.TaiKhoan set MatKhau = @mk, MSNVOF = @msnv, Cap = @role WHERE TaiKhoan = @tk")
        Dim cmd As New SqlCommand(query) With {
            .CommandType = CommandType.Text}
        Try
            cmd.Parameters.Add("@tk", SqlDbType.NVarChar).Value = tk.TaiKhoan
            cmd.Parameters.Add("@mk", SqlDbType.NVarChar).Value = tk.MatKhau
            cmd.Parameters.Add("@msnv", SqlDbType.NVarChar).Value = tk.MSNV
            cmd.Parameters.Add("@role", SqlDbType.Int).Value = tk.Cap
            Return DataProvider.RunNonQuery(cmd)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function Delete(ByVal tk As String)
        Dim query As String = String.Format("DELETE FROM dbo.TaiKhoan WHERE TaiKhoan = @tk")
        Dim cmd As New SqlCommand(query) With {
            .CommandType = CommandType.Text}
        Try
            cmd.Parameters.Add("@tk", SqlDbType.NVarChar).Value = tk
            Return DataProvider.RunNonQuery(cmd)
        Catch ex As Exception
            Return False
        End Try
    End Function


    Public Function checkLogin(ByVal tk As String, ByVal mk As String) As String
        Dim query As String = String.Format("SELECT TaiKhoan, MatKhau FROM dbo.TaiKhoan WHERE TaiKhoan = '{0}' AND MatKhau = '{1}'", tk, mk)
        Dim cmd As New SqlCommand(query)
        Try
            Return DataProvider.Load1Query(cmd)
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Public Function getAccountRole(ByVal tk As String)
        Dim query As String = String.Format("SELECT Cap FROM dbo.TaiKhoan WHERE TaiKhoan = '{0}'", tk)
        Dim cmd As New SqlCommand(query)
        Try
            Return DataProvider.Load1Query(cmd)
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Public Function getByUserName(ByVal UID As String)
        Dim query As String = String.Format("SELECT * FROM dbo.TaiKhoan WHERE TaiKhoan = {0}", UID)
        Dim cmd As New SqlCommand(query)
        Try
            Return DataProvider.Load1Query(cmd)
        Catch ex As Exception
            Return vbNull
        End Try
    End Function

End Class
