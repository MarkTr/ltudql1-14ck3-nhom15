﻿Public Class HoaDonDTO
    Dim _IDBill As Integer
    Dim _IDreader As Integer
    Dim _Exp As DateTime
    Dim _NameRead As String
    Dim _Fee As Integer

    Public Property IDBill() As Integer
        Get
            Return _IDBill
        End Get
        Set(value As Integer)
            _IDBill = value
        End Set
    End Property

    Public Property IDreader() As Integer
        Get
            Return _IDreader
        End Get
        Set(value As Integer)
            _IDreader = value
        End Set
    End Property

    Public Property Exp() As Date
        Get
            Return _Exp
        End Get
        Set(value As Date)
            _Exp = value
        End Set
    End Property

    Public Property NameRead() As String
        Get
            Return _NameRead
        End Get
        Set(value As String)
            _NameRead = value
        End Set
    End Property

    Public Property Fee() As Integer
        Get
            Return _Fee
        End Get
        Set(value As Integer)
            _Fee = value
        End Set
    End Property
End Class
