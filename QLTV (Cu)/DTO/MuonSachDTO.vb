﻿Public Class MuonSachDTO
#Region "attributes"
    Dim _MaPhieuMuon As String
    Dim _MSDG As String
    Dim _MSNV As String
    Dim _NgayMuon As DateTime

    
#End Region

#Region "Properties"
    Public Property MaPhieuMuon As String
        Get
            Return _MaPhieuMuon
        End Get
        Set(value As String)
            _MaPhieuMuon = value
        End Set
    End Property

    Public Property MSDG As String
        Get
            Return _MSDG
        End Get
        Set(value As String)
            _MSDG = value
        End Set
    End Property

    Public Property MSNV As String
        Get
            Return _MSNV
        End Get
        Set(value As String)
            _MSNV = value
        End Set
    End Property

    Public Property NgayMuon As Date
        Get
            Return _NgayMuon
        End Get
        Set(value As Date)
            _NgayMuon = value
        End Set
    End Property
#End Region
End Class
