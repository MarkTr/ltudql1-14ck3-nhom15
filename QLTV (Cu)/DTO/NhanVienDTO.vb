﻿Public Class NhanVienDTO
    Dim _MSNV As String
    Dim _HoTenNV As String
    Dim _DienThoai As String
    Dim _GioiTinh As String
    Dim _NgaySinh As DateTime

    Public Property MSNV As String
        Get
            Return _MSNV
        End Get
        Set(value As String)
            _MSNV = value
        End Set
    End Property


    Public Property HoTenNV As String
        Get
            Return _HoTenNV
        End Get
        Set(value As String)
            _HoTenNV = value
        End Set
    End Property

    Public Property DienThoai As String
        Get
            Return _DienThoai
        End Get
        Set(value As String)
            _DienThoai = value
        End Set
    End Property

    Public Property NgaySinh As Date
        Get
            Return _NgaySinh
        End Get
        Set(value As Date)
            _NgaySinh = value
        End Set
    End Property

    Public Property GioiTinh As String
        Get
            Return _GioiTinh
        End Get
        Set(value As String)
            _GioiTinh = value
        End Set
    End Property
End Class
