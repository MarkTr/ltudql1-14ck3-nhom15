﻿Public Class SachDTO
    Dim _MaSach As String
    Dim _TenSach As String
    Dim _NXB As String
    Dim _TacGia As String
    Dim _MaloaiSach As Integer
    Dim _NamXB As Integer
    Dim _LanXB As Integer
    Dim _SoLuong As Integer
    Dim _Noidungtomtat As String
    Dim _ISBN As String
    Dim _DangMuon As Integer
    Dim _MatorHu As Integer
    Dim _SoTrang As Integer

    Public Property MaSach As String
        Get
            Return _MaSach
        End Get
        Set(value As String)
            _MaSach = value
        End Set
    End Property

    Public Property TenSach As String
        Get
            Return _TenSach
        End Get
        Set(value As String)
            _TenSach = value
        End Set
    End Property

    Public Property NXB As String
        Get
            Return _NXB
        End Get
        Set(value As String)
            _NXB = value
        End Set
    End Property

    Public Property MaloaiSach As Integer
        Get
            Return _MaloaiSach
        End Get
        Set(value As Integer)
            _MaloaiSach = value
        End Set
    End Property

    Public Property LanXB As Integer
        Get
            Return _LanXB
        End Get
        Set(value As Integer)
            _LanXB = value
        End Set
    End Property

    Public Property NamXB As Integer
        Get
            Return _NamXB
        End Get
        Set(value As Integer)
            _NamXB = value
        End Set
    End Property

    Public Property SoLuong As Integer
        Get
            Return _SoLuong
        End Get
        Set(value As Integer)
            _SoLuong = value
        End Set
    End Property

    Public Property Noidungtomtat As String
        Get
            Return _Noidungtomtat
        End Get
        Set(value As String)
            _Noidungtomtat = value
        End Set
    End Property

    Public Property SoTrang As Integer
        Get
            Return _SoTrang
        End Get
        Set(value As Integer)
            _SoTrang = value
        End Set
    End Property

    Public Property MatorHu As Integer
        Get
            Return _MatorHu
        End Get
        Set(value As Integer)
            _MatorHu = value
        End Set
    End Property

    Public Property DangMuon As Integer
        Get
            Return _DangMuon
        End Get
        Set(value As Integer)
            _DangMuon = value
        End Set
    End Property

    Public Property TacGia As String
        Get
            Return _TacGia
        End Get
        Set(value As String)
            _TacGia = value
        End Set
    End Property

    Public Property ISBN As String
        Get
            Return _ISBN
        End Get
        Set(value As String)
            _ISBN = value
        End Set
    End Property
End Class
