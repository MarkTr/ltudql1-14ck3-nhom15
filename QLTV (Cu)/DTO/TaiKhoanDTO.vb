﻿Public Class TaiKhoanDTO
#Region "attributes"
    Dim _TaiKhoan As String
    Dim _MatKhau As String
    Dim _MSNV As String
    Dim _Cap As Integer
#End Region

#Region "Proerties"
    Public Property MatKhau As String
        Get
            Return _MatKhau
        End Get
        Set(value As String)
            _MatKhau = value
        End Set
    End Property

    Public Property TaiKhoan As String
        Get
            Return _TaiKhoan
        End Get
        Set(value As String)
            _TaiKhoan = value
        End Set
    End Property

    Public Property Cap As Integer
        Get
            Return _Cap
        End Get
        Set(value As Integer)
            _Cap = value
        End Set
    End Property


    Public Property MSNV As String
        Get
            Return _MSNV
        End Get
        Set(value As String)
            _MSNV = value
        End Set
    End Property
#End Region
End Class
