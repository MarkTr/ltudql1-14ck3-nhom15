﻿Imports DAO
Imports DTO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Public Class DocGiaBUS
    Public Shared Sub AddReaderBUS(ByVal Reader As DocGiaDTO)
        If DocGiaDAO.Insert_DocGia(Reader) Then
            MessageBox.Show("Create new Reader Success!", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBox.Show("Create new Reader Fail!!!", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub
    Public Shared Function Loader(ByVal paramet As String, ByVal i As Integer) As DataTable

        Return DocGiaDAO.LoadRead(paramet, i)
    End Function

    Public Shared Function LoadIDReader() As String
        Return DocGiaDAO.LoadSacle()
    End Function

    Public Shared Function LoadInfo(ByVal strS As String) As DocGiaDTO
        Return DocGiaDAO.LoadOneReader(strS)
    End Function

    Public Shared Sub XoaDG(ByVal MADG As String)
        If DocGiaDAO.XoaDG(MADG) = True Then
            MessageBox.Show("Xóa Thành Công!!!", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBox.Show("Đã có lỗi hãy kiểm tra lại thông tin", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Public Shared Sub Update_readerBUS(ByVal Reader As DocGiaDTO)

        If DocGiaDAO.Update_DocGia(Reader) Then
            MessageBox.Show("Update Reader Success!", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBox.Show("Update Reader Fail!!! Try again", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

    Public Shared Function loadcbb() As DataTable
        Return DocGiaDAO.Loadcombobox()
    End Function
    Public Shared Sub DoiServer(ByVal conn As String)
        Try
            DataProvider.DoiChuoi(conn)
            MessageBox.Show("ok!")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
End Class
