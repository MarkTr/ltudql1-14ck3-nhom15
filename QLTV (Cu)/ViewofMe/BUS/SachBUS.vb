﻿Imports DAO
Imports DTO
Public Class SachBUS
    Public Shared Function LoadBook() As DataTable

        Return SachDAO.LoadSach
    End Function
    Public Shared Sub ThemSach(ByVal Sach As SachDTO)
        Try
            If SachDAO.ThemSach(Sach) Then
                MessageBox.Show("Thêm Sách Thành Công!!!", "Quản Lý Sách", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show("Không thể thêm sách!!! Lỗi " + ex.Message, "Quản Lý Sách", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Public Shared Function LayMaSach() As String
        Return SachDAO.LoadMaSach()
    End Function
End Class
