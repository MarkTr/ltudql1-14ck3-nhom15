﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DoiServer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnDoi = New System.Windows.Forms.Button()
        Me.txtConn = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnDoi
        '
        Me.btnDoi.Location = New System.Drawing.Point(470, 35)
        Me.btnDoi.Name = "btnDoi"
        Me.btnDoi.Size = New System.Drawing.Size(75, 23)
        Me.btnDoi.TabIndex = 0
        Me.btnDoi.Text = "&Đổi"
        Me.btnDoi.UseVisualStyleBackColor = True
        '
        'txtConn
        '
        Me.txtConn.Location = New System.Drawing.Point(98, 37)
        Me.txtConn.Name = "txtConn"
        Me.txtConn.Size = New System.Drawing.Size(366, 20)
        Me.txtConn.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(79, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Server thay thế"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(12, 61)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(576, 19)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Lưu ý! Cửa số này thay đổi chuỗi connect phòng trường hợp không conect được thôi!" &
    ""
        '
        'DoiServer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(600, 110)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtConn)
        Me.Controls.Add(Me.btnDoi)
        Me.Name = "DoiServer"
        Me.Text = "Đổi Chuỗi Kết nối"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnDoi As Button
    Friend WithEvents txtConn As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
End Class
