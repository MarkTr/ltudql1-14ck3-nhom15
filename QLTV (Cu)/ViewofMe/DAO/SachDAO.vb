﻿Imports DTO
Imports System.Data.SqlClient
Public Class SachDAO
    Public Shared Function LoadSach() As DataTable
        Dim strQ As String = "SELECT MaSach, TenSach, TacGia, NXB, MaLoaiSach, NamXB, LanXB, SoLuong, NoiDungTomTat, SoTrang, DangMuon, MatOrHu, ISBN FROM QuyenSach"

        Dim cmd As New SqlCommand(strQ)
        cmd.CommandType = CommandType.Text
        DataProvider.LoaderFromDATA(cmd)
        ' cmd.Parameters.Add("@Masach", SqlDbType.NVarChar).Value =

        Return DataProvider.LoaderFromDATA(cmd)
    End Function

    Public Shared Function ThemSach(ByVal Sach As SachDTO) As Boolean
        Dim strQ As String = "insert into QuyenSach values (@MaSach, @TenSach, @TacGia, @NXB, @MaLoaiSach, @NamXB, @LanXB, @SoLuong, @NoiDungTomTat, @SoTrang, @DangMuon, @MatOrHu, @ISBN)"
        Dim ischeck As Boolean = False
        Dim cmd As New SqlCommand(strQ)
        cmd.CommandType = CommandType.Text

        Try
            cmd.Parameters.Add("@MaSach", SqlDbType.NVarChar).Value = Sach.MaSach
            cmd.Parameters.Add("@ISBN", SqlDbType.NVarChar).Value = Sach.ISBN
            cmd.Parameters.Add("@NoiDungTomTat", SqlDbType.NVarChar).Value = Sach.Noidungtomtat
            cmd.Parameters.Add("@TacGia", SqlDbType.NVarChar).Value = Sach.TacGia
            cmd.Parameters.Add("@TenSach", SqlDbType.NVarChar).Value = Sach.TenSach
            cmd.Parameters.Add("@NXB", SqlDbType.NVarChar).Value = Sach.NXB
            cmd.Parameters.Add("@SoLuong", SqlDbType.Int).Value = Sach.SoLuong
            cmd.Parameters.Add("@SoTrang", SqlDbType.Int).Value = Sach.SoTrang
            cmd.Parameters.Add("@NamXB", SqlDbType.Int).Value = Sach.NamXB
            cmd.Parameters.Add("@DangMuon", SqlDbType.Int).Value = Sach.DangMuon
            cmd.Parameters.Add("@MatOrHu", SqlDbType.Int).Value = Sach.MatorHu
            cmd.Parameters.Add("@MaLoaiSach", SqlDbType.Int).Value = Sach.MaloaiSach
            cmd.Parameters.Add("@LanXB", SqlDbType.Int).Value = Sach.LanXB


            If DataProvider.RunNonQuery(cmd) Then
                ischeck = True
            End If

        Catch ex As Exception
            ischeck = False
        End Try
        Return ischeck
    End Function
    Public Shared Function LoadMaSach() As String
        Dim cmd As New SqlCommand("select top 1 masach from QuyenSach order by MaSach desc")
        Return DataProvider.Load1Query(cmd)
    End Function
End Class

'"insert into QuyenSach values (@MaSach, @TenSach, @TacGia, @NXB, @MaLoaiSach, @NamXB, @LanXB, @SoLuong, @NoiDungTomTat, @SoTrang, @DangMuon, @MatOrHu, @ISBN)"