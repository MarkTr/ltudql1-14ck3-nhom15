﻿Public Class ChiTietPhieuMuonDTO
#Region "attributes"
    Dim _MaPhieuMuon As String
    Dim _MaSach As String
    Dim _HanTra As DateTime
    Dim _NgayTra As DateTime
    Dim _NgayMuon As DateTime
    Dim _SoNgayTre As Integer
    Dim _GhiChu As String
    Dim _DaMat As Boolean
    Dim _DaTra As Boolean
    Dim _SoLuong As Integer


#End Region
#Region "Properties"

    Public Property MaPhieuMuon() As String
        Get
            Return _MaPhieuMuon
        End Get
        Set(value As String)
            _MaPhieuMuon = value
        End Set
    End Property

    Public Property MaSach() As String
        Get
            Return _MaSach
        End Get
        Set(value As String)
            _MaSach = value
        End Set
    End Property

    Public Property HanTra() As Date
        Get
            Return _HanTra
        End Get
        Set(value As Date)
            _HanTra = value
        End Set
    End Property

    Public Property SoLuong() As Integer
        Get
            Return _SoLuong
        End Get
        Set(value As Integer)
            _SoLuong = value
        End Set
    End Property

    Public Property DaTra() As Boolean
        Get
            Return _DaTra
        End Get
        Set(value As Boolean)
            _DaTra = value
        End Set
    End Property

    Public Property DaMat() As Boolean
        Get
            Return _DaMat
        End Get
        Set(value As Boolean)
            _DaMat = value
        End Set
    End Property

    Public Property GhiChu() As String
        Get
            Return _GhiChu
        End Get
        Set(value As String)
            _GhiChu = value
        End Set
    End Property

    Public Property SoNgayTre() As Integer
        Get
            Return _SoNgayTre
        End Get
        Set(value As Integer)
            _SoNgayTre = value
        End Set
    End Property

    Public Property NgayMuon() As Date
        Get
            Return _NgayMuon
        End Get
        Set(value As Date)
            _NgayMuon = value
        End Set
    End Property

    Public Property NgayTra() As Date
        Get
            Return _NgayTra
        End Get
        Set(value As Date)
            _NgayTra = value
        End Set
    End Property
#End Region
End Class
