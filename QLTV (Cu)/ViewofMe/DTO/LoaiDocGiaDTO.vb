﻿Public Class LoaiDocGiaDTO
    Dim _ID As Integer
    Dim _Role As String
    Dim _Phi As Integer
    Dim _TLDB As Boolean
    Dim _SoDcSachMuon As Integer
    Dim _SoNGayDcMuon As Integer

    Public Property ID As Integer
        Get
            Return _ID
        End Get
        Set(value As Integer)
            _ID = value
        End Set
    End Property

    Public Property Role As String
        Get
            Return _Role
        End Get
        Set(value As String)
            _Role = value
        End Set
    End Property

    Public Property TLDB As Boolean
        Get
            Return _TLDB
        End Get
        Set(value As Boolean)
            _TLDB = value
        End Set
    End Property

    Public Property SoDcSachMuon As Integer
        Get
            Return _SoDcSachMuon
        End Get
        Set(value As Integer)
            _SoDcSachMuon = value
        End Set
    End Property

    Public Property SoNGayDcMuon As Integer
        Get
            Return _SoNGayDcMuon
        End Get
        Set(value As Integer)
            _SoNGayDcMuon = value
        End Set
    End Property

    Public Property Phi As Integer
        Get
            Return _Phi
        End Get
        Set(value As Integer)
            _Phi = value
        End Set
    End Property
End Class
