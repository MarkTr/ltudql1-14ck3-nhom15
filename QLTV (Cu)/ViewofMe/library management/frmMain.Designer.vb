﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ReadersManagementToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegReaderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentManagementToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BrownToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StaffToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReadersManagementToolStripMenuItem, Me.DocumentManagementToolStripMenuItem, Me.BrownToolStripMenuItem, Me.StaffToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(817, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ReadersManagementToolStripMenuItem
        '
        Me.ReadersManagementToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegReaderToolStripMenuItem, Me.ToolStripMenuItem1})
        Me.ReadersManagementToolStripMenuItem.Name = "ReadersManagementToolStripMenuItem"
        Me.ReadersManagementToolStripMenuItem.Size = New System.Drawing.Size(134, 20)
        Me.ReadersManagementToolStripMenuItem.Text = "Readers Management"
        '
        'RegReaderToolStripMenuItem
        '
        Me.RegReaderToolStripMenuItem.Name = "RegReaderToolStripMenuItem"
        Me.RegReaderToolStripMenuItem.Size = New System.Drawing.Size(160, 22)
        Me.RegReaderToolStripMenuItem.Text = "Registration"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(160, 22)
        Me.ToolStripMenuItem1.Text = "Manager Reader"
        '
        'DocumentManagementToolStripMenuItem
        '
        Me.DocumentManagementToolStripMenuItem.Name = "DocumentManagementToolStripMenuItem"
        Me.DocumentManagementToolStripMenuItem.Size = New System.Drawing.Size(149, 20)
        Me.DocumentManagementToolStripMenuItem.Text = "Document Management"
        '
        'BrownToolStripMenuItem
        '
        Me.BrownToolStripMenuItem.Name = "BrownToolStripMenuItem"
        Me.BrownToolStripMenuItem.Size = New System.Drawing.Size(84, 20)
        Me.BrownToolStripMenuItem.Text = "Borrow /Pay"
        '
        'StaffToolStripMenuItem
        '
        Me.StaffToolStripMenuItem.Name = "StaffToolStripMenuItem"
        Me.StaffToolStripMenuItem.Size = New System.Drawing.Size(42, 20)
        Me.StaffToolStripMenuItem.Text = "staff"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(817, 367)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.Text = "frmMain"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents DocumentManagementToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BrownToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StaffToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReadersManagementToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RegReaderToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
End Class
