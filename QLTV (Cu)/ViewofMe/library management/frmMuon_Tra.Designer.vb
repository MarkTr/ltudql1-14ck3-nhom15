﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMuon_Tra
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabMuon = New System.Windows.Forms.TabPage()
        Me.dgrMuon = New System.Windows.Forms.DataGridView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnThoat = New System.Windows.Forms.Button()
        Me.btnMuon = New System.Windows.Forms.Button()
        Me.btnThemMoi = New System.Windows.Forms.Button()
        Me.grMuon = New System.Windows.Forms.GroupBox()
        Me.txtSoLuong = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dtpNgayTra = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtpNgayMuon = New System.Windows.Forms.DateTimePicker()
        Me.txtMaNhanVien = New System.Windows.Forms.TextBox()
        Me.txtMaDocGia = New System.Windows.Forms.TextBox()
        Me.txtMaPhieuMuon = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tabTra = New System.Windows.Forms.TabPage()
        Me.dgrTra = New System.Windows.Forms.DataGridView()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.btnThoatPay = New System.Windows.Forms.Button()
        Me.btnTraSach = New System.Windows.Forms.Button()
        Me.grTra = New System.Windows.Forms.GroupBox()
        Me.dtpPayNgayTra = New System.Windows.Forms.DateTimePicker()
        Me.dtpPayNgayMuon = New System.Windows.Forms.DateTimePicker()
        Me.txtSoLuongMuon = New System.Windows.Forms.TextBox()
        Me.txtMaSach = New System.Windows.Forms.TextBox()
        Me.txtMaPhieuTra = New System.Windows.Forms.TextBox()
        Me.txtPayMaDocGia = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TabControl1.SuspendLayout()
        Me.tabMuon.SuspendLayout()
        CType(Me.dgrMuon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.grMuon.SuspendLayout()
        Me.tabTra.SuspendLayout()
        CType(Me.dgrTra, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.grTra.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.tabMuon)
        Me.TabControl1.Controls.Add(Me.tabTra)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(578, 417)
        Me.TabControl1.TabIndex = 0
        '
        'tabMuon
        '
        Me.tabMuon.Controls.Add(Me.dgrMuon)
        Me.tabMuon.Controls.Add(Me.GroupBox2)
        Me.tabMuon.Controls.Add(Me.grMuon)
        Me.tabMuon.Location = New System.Drawing.Point(4, 22)
        Me.tabMuon.Name = "tabMuon"
        Me.tabMuon.Padding = New System.Windows.Forms.Padding(3)
        Me.tabMuon.Size = New System.Drawing.Size(570, 391)
        Me.tabMuon.TabIndex = 0
        Me.tabMuon.Text = "Mượn"
        Me.tabMuon.UseVisualStyleBackColor = True
        '
        'dgrMuon
        '
        Me.dgrMuon.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgrMuon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgrMuon.Location = New System.Drawing.Point(6, 145)
        Me.dgrMuon.Name = "dgrMuon"
        Me.dgrMuon.Size = New System.Drawing.Size(543, 240)
        Me.dgrMuon.TabIndex = 12
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.btnThoat)
        Me.GroupBox2.Controls.Add(Me.btnMuon)
        Me.GroupBox2.Controls.Add(Me.btnThemMoi)
        Me.GroupBox2.Location = New System.Drawing.Point(437, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(112, 133)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Control"
        '
        'btnThoat
        '
        Me.btnThoat.Location = New System.Drawing.Point(6, 94)
        Me.btnThoat.Name = "btnThoat"
        Me.btnThoat.Size = New System.Drawing.Size(100, 27)
        Me.btnThoat.TabIndex = 2
        Me.btnThoat.Text = "Thoát"
        Me.btnThoat.UseVisualStyleBackColor = True
        '
        'btnMuon
        '
        Me.btnMuon.Location = New System.Drawing.Point(6, 61)
        Me.btnMuon.Name = "btnMuon"
        Me.btnMuon.Size = New System.Drawing.Size(100, 27)
        Me.btnMuon.TabIndex = 1
        Me.btnMuon.Text = "Mượn"
        Me.btnMuon.UseVisualStyleBackColor = True
        '
        'btnThemMoi
        '
        Me.btnThemMoi.Location = New System.Drawing.Point(6, 22)
        Me.btnThemMoi.Name = "btnThemMoi"
        Me.btnThemMoi.Size = New System.Drawing.Size(100, 28)
        Me.btnThemMoi.TabIndex = 0
        Me.btnThemMoi.Text = "Thêm mới"
        Me.btnThemMoi.UseVisualStyleBackColor = True
        '
        'grMuon
        '
        Me.grMuon.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grMuon.Controls.Add(Me.txtSoLuong)
        Me.grMuon.Controls.Add(Me.Label6)
        Me.grMuon.Controls.Add(Me.dtpNgayTra)
        Me.grMuon.Controls.Add(Me.Label4)
        Me.grMuon.Controls.Add(Me.dtpNgayMuon)
        Me.grMuon.Controls.Add(Me.txtMaNhanVien)
        Me.grMuon.Controls.Add(Me.txtMaDocGia)
        Me.grMuon.Controls.Add(Me.txtMaPhieuMuon)
        Me.grMuon.Controls.Add(Me.Label5)
        Me.grMuon.Controls.Add(Me.Label3)
        Me.grMuon.Controls.Add(Me.Label2)
        Me.grMuon.Controls.Add(Me.Label1)
        Me.grMuon.Location = New System.Drawing.Point(6, 6)
        Me.grMuon.Name = "grMuon"
        Me.grMuon.Size = New System.Drawing.Size(425, 133)
        Me.grMuon.TabIndex = 0
        Me.grMuon.TabStop = False
        Me.grMuon.Text = "Thông Tin Phiếu Mượn"
        '
        'txtSoLuong
        '
        Me.txtSoLuong.Location = New System.Drawing.Point(295, 103)
        Me.txtSoLuong.Name = "txtSoLuong"
        Me.txtSoLuong.Size = New System.Drawing.Size(100, 20)
        Me.txtSoLuong.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(208, 106)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(49, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Số lượng"
        '
        'dtpNgayTra
        '
        Me.dtpNgayTra.Location = New System.Drawing.Point(92, 107)
        Me.dtpNgayTra.Name = "dtpNgayTra"
        Me.dtpNgayTra.Size = New System.Drawing.Size(100, 20)
        Me.dtpNgayTra.TabIndex = 11
        Me.dtpNgayTra.Value = New Date(2016, 11, 30, 0, 0, 0, 0)
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 110)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Ngày trả"
        '
        'dtpNgayMuon
        '
        Me.dtpNgayMuon.Location = New System.Drawing.Point(92, 65)
        Me.dtpNgayMuon.Name = "dtpNgayMuon"
        Me.dtpNgayMuon.Size = New System.Drawing.Size(100, 20)
        Me.dtpNgayMuon.TabIndex = 9
        Me.dtpNgayMuon.Value = New Date(2016, 11, 30, 0, 0, 0, 0)
        '
        'txtMaNhanVien
        '
        Me.txtMaNhanVien.Location = New System.Drawing.Point(295, 27)
        Me.txtMaNhanVien.Name = "txtMaNhanVien"
        Me.txtMaNhanVien.Size = New System.Drawing.Size(100, 20)
        Me.txtMaNhanVien.TabIndex = 8
        '
        'txtMaDocGia
        '
        Me.txtMaDocGia.Location = New System.Drawing.Point(295, 65)
        Me.txtMaDocGia.Name = "txtMaDocGia"
        Me.txtMaDocGia.Size = New System.Drawing.Size(100, 20)
        Me.txtMaDocGia.TabIndex = 7
        '
        'txtMaPhieuMuon
        '
        Me.txtMaPhieuMuon.Location = New System.Drawing.Point(92, 27)
        Me.txtMaPhieuMuon.Name = "txtMaPhieuMuon"
        Me.txtMaPhieuMuon.Size = New System.Drawing.Size(100, 20)
        Me.txtMaPhieuMuon.TabIndex = 5
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(208, 68)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Mã đọc giả"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 68)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Ngày mượn"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(208, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Mã nhân viên"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Mã phiếu mượn"
        '
        'tabTra
        '
        Me.tabTra.Controls.Add(Me.dgrTra)
        Me.tabTra.Controls.Add(Me.GroupBox4)
        Me.tabTra.Controls.Add(Me.grTra)
        Me.tabTra.Controls.Add(Me.TextBox6)
        Me.tabTra.Location = New System.Drawing.Point(4, 22)
        Me.tabTra.Name = "tabTra"
        Me.tabTra.Padding = New System.Windows.Forms.Padding(3)
        Me.tabTra.Size = New System.Drawing.Size(570, 391)
        Me.tabTra.TabIndex = 1
        Me.tabTra.Text = "Trả"
        Me.tabTra.UseVisualStyleBackColor = True
        '
        'dgrTra
        '
        Me.dgrTra.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgrTra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgrTra.Location = New System.Drawing.Point(6, 142)
        Me.dgrTra.Name = "dgrTra"
        Me.dgrTra.Size = New System.Drawing.Size(543, 246)
        Me.dgrTra.TabIndex = 9
        '
        'GroupBox4
        '
        Me.GroupBox4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox4.Controls.Add(Me.btnThoatPay)
        Me.GroupBox4.Controls.Add(Me.btnTraSach)
        Me.GroupBox4.Location = New System.Drawing.Point(437, 6)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(115, 130)
        Me.GroupBox4.TabIndex = 8
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Control"
        '
        'btnThoatPay
        '
        Me.btnThoatPay.Location = New System.Drawing.Point(6, 56)
        Me.btnThoatPay.Name = "btnThoatPay"
        Me.btnThoatPay.Size = New System.Drawing.Size(103, 28)
        Me.btnThoatPay.TabIndex = 1
        Me.btnThoatPay.Text = "Thoát"
        Me.btnThoatPay.UseVisualStyleBackColor = True
        '
        'btnTraSach
        '
        Me.btnTraSach.Location = New System.Drawing.Point(6, 22)
        Me.btnTraSach.Name = "btnTraSach"
        Me.btnTraSach.Size = New System.Drawing.Size(103, 27)
        Me.btnTraSach.TabIndex = 0
        Me.btnTraSach.Text = "Trả sách"
        Me.btnTraSach.UseVisualStyleBackColor = True
        '
        'grTra
        '
        Me.grTra.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grTra.Controls.Add(Me.dtpPayNgayTra)
        Me.grTra.Controls.Add(Me.dtpPayNgayMuon)
        Me.grTra.Controls.Add(Me.txtSoLuongMuon)
        Me.grTra.Controls.Add(Me.txtMaSach)
        Me.grTra.Controls.Add(Me.txtMaPhieuTra)
        Me.grTra.Controls.Add(Me.txtPayMaDocGia)
        Me.grTra.Controls.Add(Me.Label12)
        Me.grTra.Controls.Add(Me.Label11)
        Me.grTra.Controls.Add(Me.Label10)
        Me.grTra.Controls.Add(Me.Label9)
        Me.grTra.Controls.Add(Me.Label8)
        Me.grTra.Controls.Add(Me.Label7)
        Me.grTra.Location = New System.Drawing.Point(6, 6)
        Me.grTra.Name = "grTra"
        Me.grTra.Size = New System.Drawing.Size(425, 130)
        Me.grTra.TabIndex = 0
        Me.grTra.TabStop = False
        Me.grTra.Text = "Thông Tin Trã"
        '
        'dtpPayNgayTra
        '
        Me.dtpPayNgayTra.Location = New System.Drawing.Point(292, 58)
        Me.dtpPayNgayTra.Name = "dtpPayNgayTra"
        Me.dtpPayNgayTra.Size = New System.Drawing.Size(111, 20)
        Me.dtpPayNgayTra.TabIndex = 14
        '
        'dtpPayNgayMuon
        '
        Me.dtpPayNgayMuon.Location = New System.Drawing.Point(292, 23)
        Me.dtpPayNgayMuon.Name = "dtpPayNgayMuon"
        Me.dtpPayNgayMuon.Size = New System.Drawing.Size(111, 20)
        Me.dtpPayNgayMuon.TabIndex = 13
        '
        'txtSoLuongMuon
        '
        Me.txtSoLuongMuon.Location = New System.Drawing.Point(292, 94)
        Me.txtSoLuongMuon.Name = "txtSoLuongMuon"
        Me.txtSoLuongMuon.Size = New System.Drawing.Size(111, 20)
        Me.txtSoLuongMuon.TabIndex = 12
        '
        'txtMaSach
        '
        Me.txtMaSach.Location = New System.Drawing.Point(78, 98)
        Me.txtMaSach.Name = "txtMaSach"
        Me.txtMaSach.Size = New System.Drawing.Size(100, 20)
        Me.txtMaSach.TabIndex = 9
        '
        'txtMaPhieuTra
        '
        Me.txtMaPhieuTra.Location = New System.Drawing.Point(78, 22)
        Me.txtMaPhieuTra.Name = "txtMaPhieuTra"
        Me.txtMaPhieuTra.Size = New System.Drawing.Size(100, 20)
        Me.txtMaPhieuTra.TabIndex = 6
        '
        'txtPayMaDocGia
        '
        Me.txtPayMaDocGia.Location = New System.Drawing.Point(78, 58)
        Me.txtPayMaDocGia.Name = "txtPayMaDocGia"
        Me.txtPayMaDocGia.Size = New System.Drawing.Size(100, 20)
        Me.txtPayMaDocGia.TabIndex = 8
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(208, 97)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(78, 13)
        Me.Label12.TabIndex = 5
        Me.Label12.Text = "Số lượng mượn"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(208, 61)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(47, 13)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "Ngày trả"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(208, 25)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(61, 13)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = "Ngày mượn"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 25)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(66, 13)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Mã phiếu trả"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 101)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(48, 13)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Mã sách"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 61)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(61, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Mã độc giả"
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(94, 116)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(100, 20)
        Me.TextBox6.TabIndex = 7
        '
        'frmMuon_Tra
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(602, 441)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmMuon_Tra"
        Me.Text = "Quản Lý Mượn Trả"
        Me.TabControl1.ResumeLayout(False)
        Me.tabMuon.ResumeLayout(False)
        CType(Me.dgrMuon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.grMuon.ResumeLayout(False)
        Me.grMuon.PerformLayout()
        Me.tabTra.ResumeLayout(False)
        Me.tabTra.PerformLayout()
        CType(Me.dgrTra, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.grTra.ResumeLayout(False)
        Me.grTra.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tabMuon As System.Windows.Forms.TabPage
    Friend WithEvents tabTra As System.Windows.Forms.TabPage
    Friend WithEvents grMuon As System.Windows.Forms.GroupBox
    Friend WithEvents dtpNgayMuon As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtMaNhanVien As System.Windows.Forms.TextBox
    Friend WithEvents txtMaDocGia As System.Windows.Forms.TextBox
    '<<<<<<< HEAD:library management/library management/frmBorrow_Pay.Designer.vb
    Friend WithEvents txtMaPhieuMuon As System.Windows.Forms.TextBox
    '=======
    Friend WithEvents txtPhieuMuon As System.Windows.Forms.TextBox
    '>>>>>>> ViewUpdate-chinh-sua:ViewofMe/library management/frmBorrow_Pay.Designer.vb
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dgrMuon As System.Windows.Forms.DataGridView
    Friend WithEvents btnMuon As System.Windows.Forms.Button
    Friend WithEvents btnThemMoi As System.Windows.Forms.Button
    Friend WithEvents txtSoLuong As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dtpNgayTra As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dgrTra As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    '<<<<<<< HEAD:library management/library management/frmBorrow_Pay.Designer.vb
    'Friend WithEvents btnPayThoat As System.Windows.Forms.Button
    '=======
    Friend WithEvents btnThoatPay As System.Windows.Forms.Button
    '>>>>>>> ViewUpdate-chinh-sua:ViewofMe/library management/frmBorrow_Pay.Designer.vb
    Friend WithEvents btnTraSach As System.Windows.Forms.Button
    Friend WithEvents grTra As System.Windows.Forms.GroupBox
    Friend WithEvents txtSoLuongMuon As System.Windows.Forms.TextBox
    Friend WithEvents txtMaSach As System.Windows.Forms.TextBox
    Friend WithEvents txtMaPhieuTra As System.Windows.Forms.TextBox
    Friend WithEvents txtPayMaDocGia As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents dtpPayNgayTra As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpPayNgayMuon As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnThoat As System.Windows.Forms.Button
End Class
