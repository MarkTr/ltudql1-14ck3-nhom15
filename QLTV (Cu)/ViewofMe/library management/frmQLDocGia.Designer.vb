﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmQLDocGia
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmQLDocGia))
        Me.btnRegReader = New System.Windows.Forms.Button()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.grTimThongtin = New System.Windows.Forms.GroupBox()
        Me.dtReader = New System.Windows.Forms.DataGridView()
        Me.madg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tendg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmnd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Role = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fee = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NBookBor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bookexp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cbSearch = New System.Windows.Forms.ComboBox()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        Me.btnBill = New System.Windows.Forms.Button()
        Me.grInfoReader = New System.Windows.Forms.GroupBox()
        Me.btnCloseInFo = New System.Windows.Forms.Button()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.btnDel = New System.Windows.Forms.Button()
        Me.cbLoaiDocGia = New System.Windows.Forms.ComboBox()
        Me.cbSex = New System.Windows.Forms.ComboBox()
        Me.dtpExp = New System.Windows.Forms.DateTimePicker()
        Me.dtPickBD = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtAddr = New System.Windows.Forms.TextBox()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.txtIDCard = New System.Windows.Forms.TextBox()
        Me.txtIDReader = New System.Windows.Forms.TextBox()
        Me.grBill = New System.Windows.Forms.GroupBox()
        Me.DateExp = New System.Windows.Forms.DateTimePicker()
        Me.dateBill = New System.Windows.Forms.DateTimePicker()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.btnOutBill = New System.Windows.Forms.Button()
        Me.cbBill = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtFee = New System.Windows.Forms.TextBox()
        Me.txtNameBill = New System.Windows.Forms.TextBox()
        Me.txtIDreadBill = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lblHuongDanHoaDon = New System.Windows.Forms.Label()
        Me.grTimThongtin.SuspendLayout()
        CType(Me.dtReader, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grInfoReader.SuspendLayout()
        Me.grBill.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnRegReader
        '
        Me.btnRegReader.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegReader.ImageIndex = 8
        Me.btnRegReader.ImageList = Me.ImageList1
        Me.btnRegReader.Location = New System.Drawing.Point(543, 12)
        Me.btnRegReader.Name = "btnRegReader"
        Me.btnRegReader.Size = New System.Drawing.Size(110, 42)
        Me.btnRegReader.TabIndex = 0
        Me.btnRegReader.Text = "   &Thêm Độc Giả"
        Me.btnRegReader.UseVisualStyleBackColor = True
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "Search")
        Me.ImageList1.Images.SetKeyName(1, "a1a11614be1793b207e4c96c97737346.png")
        Me.ImageList1.Images.SetKeyName(2, "accounting-bill-icon.png")
        Me.ImageList1.Images.SetKeyName(3, "refresh.png")
        Me.ImageList1.Images.SetKeyName(4, "thin-159_bill_money_price_dollars-512.png")
        Me.ImageList1.Images.SetKeyName(5, "Updates Downloading updates.ico")
        Me.ImageList1.Images.SetKeyName(6, "12-512.png")
        Me.ImageList1.Images.SetKeyName(7, "9474-200.png")
        Me.ImageList1.Images.SetKeyName(8, "asdasd.png")
        '
        'grTimThongtin
        '
        Me.grTimThongtin.Controls.Add(Me.dtReader)
        Me.grTimThongtin.Controls.Add(Me.Label12)
        Me.grTimThongtin.Controls.Add(Me.cbSearch)
        Me.grTimThongtin.Controls.Add(Me.btnRegReader)
        Me.grTimThongtin.Controls.Add(Me.txtSearch)
        Me.grTimThongtin.Location = New System.Drawing.Point(12, 12)
        Me.grTimThongtin.Name = "grTimThongtin"
        Me.grTimThongtin.Size = New System.Drawing.Size(659, 313)
        Me.grTimThongtin.TabIndex = 1
        Me.grTimThongtin.TabStop = False
        Me.grTimThongtin.Text = "Search"
        '
        'dtReader
        '
        Me.dtReader.AllowUserToAddRows = False
        Me.dtReader.AllowUserToDeleteRows = False
        Me.dtReader.AllowUserToResizeColumns = False
        Me.dtReader.AllowUserToResizeRows = False
        Me.dtReader.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtReader.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.madg, Me.tendg, Me.cmnd, Me.Role, Me.Fee, Me.NBookBor, Me.Bookexp})
        Me.dtReader.Location = New System.Drawing.Point(7, 60)
        Me.dtReader.Name = "dtReader"
        Me.dtReader.ReadOnly = True
        Me.dtReader.RowHeadersVisible = False
        Me.dtReader.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtReader.Size = New System.Drawing.Size(646, 246)
        Me.dtReader.TabIndex = 20
        '
        'madg
        '
        Me.madg.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.madg.DataPropertyName = "madg"
        Me.madg.HeaderText = "Mã Độc Giả"
        Me.madg.Name = "madg"
        Me.madg.ReadOnly = True
        '
        'tendg
        '
        Me.tendg.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.tendg.DataPropertyName = "tendg"
        Me.tendg.HeaderText = "Tên"
        Me.tendg.Name = "tendg"
        Me.tendg.ReadOnly = True
        '
        'cmnd
        '
        Me.cmnd.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.cmnd.DataPropertyName = "cmnd"
        Me.cmnd.HeaderText = "CMND"
        Me.cmnd.Name = "cmnd"
        Me.cmnd.ReadOnly = True
        '
        'Role
        '
        Me.Role.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Role.DataPropertyName = "Role"
        Me.Role.HeaderText = "Loại Thẻ"
        Me.Role.Name = "Role"
        Me.Role.ReadOnly = True
        '
        'Fee
        '
        Me.Fee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Fee.DataPropertyName = "fee"
        Me.Fee.HeaderText = "Phí"
        Me.Fee.Name = "Fee"
        Me.Fee.ReadOnly = True
        '
        'NBookBor
        '
        Me.NBookBor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.NBookBor.DataPropertyName = "SoSachDangMuon"
        Me.NBookBor.HeaderText = "Số Sách Mượn"
        Me.NBookBor.Name = "NBookBor"
        Me.NBookBor.ReadOnly = True
        '
        'Bookexp
        '
        Me.Bookexp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Bookexp.DataPropertyName = "SachQuaHan"
        Me.Bookexp.HeaderText = "Số Sách Hết Hạn"
        Me.Bookexp.Name = "Bookexp"
        Me.Bookexp.ReadOnly = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(258, 23)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(34, 15)
        Me.Label12.TabIndex = 19
        Me.Label12.Text = "Theo"
        '
        'cbSearch
        '
        Me.cbSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSearch.FormattingEnabled = True
        Me.cbSearch.Items.AddRange(New Object() {"Mã Độc Giả", "CMND", "Họ Tên"})
        Me.cbSearch.Location = New System.Drawing.Point(296, 20)
        Me.cbSearch.Name = "cbSearch"
        Me.cbSearch.Size = New System.Drawing.Size(91, 21)
        Me.cbSearch.TabIndex = 3
        '
        'txtSearch
        '
        Me.txtSearch.Location = New System.Drawing.Point(11, 19)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(241, 22)
        Me.txtSearch.TabIndex = 1
        Me.txtSearch.Text = "Tìm"
        '
        'btnBill
        '
        Me.btnBill.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBill.ImageIndex = 2
        Me.btnBill.ImageList = Me.ImageList1
        Me.btnBill.Location = New System.Drawing.Point(326, 331)
        Me.btnBill.Name = "btnBill"
        Me.btnBill.Size = New System.Drawing.Size(122, 35)
        Me.btnBill.TabIndex = 3
        Me.btnBill.Text = "&Thanh toán phí"
        Me.btnBill.UseVisualStyleBackColor = True
        '
        'grInfoReader
        '
        Me.grInfoReader.Controls.Add(Me.btnCloseInFo)
        Me.grInfoReader.Controls.Add(Me.btnRefresh)
        Me.grInfoReader.Controls.Add(Me.btnUpdate)
        Me.grInfoReader.Controls.Add(Me.btnDel)
        Me.grInfoReader.Controls.Add(Me.cbLoaiDocGia)
        Me.grInfoReader.Controls.Add(Me.cbSex)
        Me.grInfoReader.Controls.Add(Me.dtpExp)
        Me.grInfoReader.Controls.Add(Me.dtPickBD)
        Me.grInfoReader.Controls.Add(Me.Label8)
        Me.grInfoReader.Controls.Add(Me.Label6)
        Me.grInfoReader.Controls.Add(Me.Label5)
        Me.grInfoReader.Controls.Add(Me.Label4)
        Me.grInfoReader.Controls.Add(Me.Label7)
        Me.grInfoReader.Controls.Add(Me.Label3)
        Me.grInfoReader.Controls.Add(Me.Label2)
        Me.grInfoReader.Controls.Add(Me.Label1)
        Me.grInfoReader.Controls.Add(Me.txtAddr)
        Me.grInfoReader.Controls.Add(Me.txtName)
        Me.grInfoReader.Controls.Add(Me.txtIDCard)
        Me.grInfoReader.Controls.Add(Me.txtIDReader)
        Me.grInfoReader.Location = New System.Drawing.Point(677, 12)
        Me.grInfoReader.Name = "grInfoReader"
        Me.grInfoReader.Size = New System.Drawing.Size(530, 174)
        Me.grInfoReader.TabIndex = 2
        Me.grInfoReader.TabStop = False
        Me.grInfoReader.Text = "Thông Tin Chi tiết độc giả"
        '
        'btnCloseInFo
        '
        Me.btnCloseInFo.Location = New System.Drawing.Point(6, 142)
        Me.btnCloseInFo.Name = "btnCloseInFo"
        Me.btnCloseInFo.Size = New System.Drawing.Size(64, 24)
        Me.btnCloseInFo.TabIndex = 5
        Me.btnCloseInFo.Text = "<Đóng"
        Me.btnCloseInFo.UseVisualStyleBackColor = True
        '
        'btnRefresh
        '
        Me.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnRefresh.ImageIndex = 3
        Me.btnRefresh.ImageList = Me.ImageList1
        Me.btnRefresh.Location = New System.Drawing.Point(312, 128)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(69, 40)
        Me.btnRefresh.TabIndex = 4
        Me.btnRefresh.Text = "&Làm Lại"
        Me.btnRefresh.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnUpdate.ImageKey = "Updates Downloading updates.ico"
        Me.btnUpdate.ImageList = Me.ImageList1
        Me.btnUpdate.Location = New System.Drawing.Point(162, 128)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(69, 40)
        Me.btnUpdate.TabIndex = 4
        Me.btnUpdate.Text = "&Cập Nhật"
        Me.btnUpdate.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'btnDel
        '
        Me.btnDel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDel.ImageKey = "a1a11614be1793b207e4c96c97737346.png"
        Me.btnDel.ImageList = Me.ImageList1
        Me.btnDel.Location = New System.Drawing.Point(237, 128)
        Me.btnDel.Name = "btnDel"
        Me.btnDel.Size = New System.Drawing.Size(69, 40)
        Me.btnDel.TabIndex = 4
        Me.btnDel.Text = "&Xóa"
        Me.btnDel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDel.UseVisualStyleBackColor = True
        '
        'cbLoaiDocGia
        '
        Me.cbLoaiDocGia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbLoaiDocGia.FormattingEnabled = True
        Me.cbLoaiDocGia.Location = New System.Drawing.Point(354, 96)
        Me.cbLoaiDocGia.Name = "cbLoaiDocGia"
        Me.cbLoaiDocGia.Size = New System.Drawing.Size(85, 21)
        Me.cbLoaiDocGia.TabIndex = 3
        '
        'cbSex
        '
        Me.cbSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSex.FormattingEnabled = True
        Me.cbSex.Items.AddRange(New Object() {"Nam", "Nữ"})
        Me.cbSex.Location = New System.Drawing.Point(354, 69)
        Me.cbSex.Name = "cbSex"
        Me.cbSex.Size = New System.Drawing.Size(85, 21)
        Me.cbSex.TabIndex = 3
        '
        'dtpExp
        '
        Me.dtpExp.Enabled = False
        Me.dtpExp.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpExp.Location = New System.Drawing.Point(354, 45)
        Me.dtpExp.Name = "dtpExp"
        Me.dtpExp.Size = New System.Drawing.Size(120, 22)
        Me.dtpExp.TabIndex = 2
        '
        'dtPickBD
        '
        Me.dtPickBD.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtPickBD.Location = New System.Drawing.Point(354, 21)
        Me.dtPickBD.Name = "dtPickBD"
        Me.dtPickBD.Size = New System.Drawing.Size(120, 22)
        Me.dtPickBD.TabIndex = 2
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(259, 22)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(61, 15)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Ngày Sinh"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(259, 99)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(72, 15)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Độc giả Loại"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(259, 51)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 15)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Hạn Thẻ"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(259, 78)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 15)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Giới tính"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(19, 104)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 15)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Address"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(19, 78)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 15)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Họ Tên"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(19, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Chứng minh thư"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(19, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Mã độc giả"
        '
        'txtAddr
        '
        Me.txtAddr.Location = New System.Drawing.Point(114, 97)
        Me.txtAddr.Name = "txtAddr"
        Me.txtAddr.Size = New System.Drawing.Size(107, 22)
        Me.txtAddr.TabIndex = 0
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(114, 71)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(107, 22)
        Me.txtName.TabIndex = 0
        '
        'txtIDCard
        '
        Me.txtIDCard.Location = New System.Drawing.Point(114, 45)
        Me.txtIDCard.Name = "txtIDCard"
        Me.txtIDCard.Size = New System.Drawing.Size(107, 22)
        Me.txtIDCard.TabIndex = 0
        '
        'txtIDReader
        '
        Me.txtIDReader.Location = New System.Drawing.Point(114, 19)
        Me.txtIDReader.Name = "txtIDReader"
        Me.txtIDReader.ReadOnly = True
        Me.txtIDReader.Size = New System.Drawing.Size(107, 22)
        Me.txtIDReader.TabIndex = 0
        '
        'grBill
        '
        Me.grBill.Controls.Add(Me.DateExp)
        Me.grBill.Controls.Add(Me.dateBill)
        Me.grBill.Controls.Add(Me.btnBack)
        Me.grBill.Controls.Add(Me.btnOutBill)
        Me.grBill.Controls.Add(Me.cbBill)
        Me.grBill.Controls.Add(Me.Label10)
        Me.grBill.Controls.Add(Me.Label11)
        Me.grBill.Controls.Add(Me.Label13)
        Me.grBill.Controls.Add(Me.Label9)
        Me.grBill.Controls.Add(Me.Label14)
        Me.grBill.Controls.Add(Me.Label16)
        Me.grBill.Controls.Add(Me.txtFee)
        Me.grBill.Controls.Add(Me.txtNameBill)
        Me.grBill.Controls.Add(Me.txtIDreadBill)
        Me.grBill.Location = New System.Drawing.Point(676, 12)
        Me.grBill.Name = "grBill"
        Me.grBill.Size = New System.Drawing.Size(530, 174)
        Me.grBill.TabIndex = 4
        Me.grBill.TabStop = False
        Me.grBill.Text = "Thông Tin Hóa Đơn"
        '
        'DateExp
        '
        Me.DateExp.CustomFormat = "dd/MM/yyyy"
        Me.DateExp.Enabled = False
        Me.DateExp.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateExp.Location = New System.Drawing.Point(342, 20)
        Me.DateExp.Name = "DateExp"
        Me.DateExp.Size = New System.Drawing.Size(98, 22)
        Me.DateExp.TabIndex = 20
        '
        'dateBill
        '
        Me.dateBill.CustomFormat = "dd/MM/yyyy hh:mm:ss"
        Me.dateBill.Enabled = False
        Me.dateBill.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dateBill.Location = New System.Drawing.Point(373, 104)
        Me.dateBill.Name = "dateBill"
        Me.dateBill.Size = New System.Drawing.Size(124, 22)
        Me.dateBill.TabIndex = 19
        '
        'btnBack
        '
        Me.btnBack.ImageKey = "accounting-bill-icon.png"
        Me.btnBack.Location = New System.Drawing.Point(6, 142)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(64, 22)
        Me.btnBack.TabIndex = 6
        Me.btnBack.Text = "<Đóng"
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'btnOutBill
        '
        Me.btnOutBill.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOutBill.ImageKey = "accounting-bill-icon.png"
        Me.btnOutBill.ImageList = Me.ImageList1
        Me.btnOutBill.Location = New System.Drawing.Point(149, 115)
        Me.btnOutBill.Name = "btnOutBill"
        Me.btnOutBill.Size = New System.Drawing.Size(125, 35)
        Me.btnOutBill.TabIndex = 6
        Me.btnOutBill.Text = "&Xuất Hóa Đơn"
        Me.btnOutBill.UseVisualStyleBackColor = True
        '
        'cbBill
        '
        Me.cbBill.Enabled = False
        Me.cbBill.FormattingEnabled = True
        Me.cbBill.Location = New System.Drawing.Point(342, 47)
        Me.cbBill.Name = "cbBill"
        Me.cbBill.Size = New System.Drawing.Size(76, 21)
        Me.cbBill.TabIndex = 18
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(259, 50)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(69, 15)
        Me.Label10.TabIndex = 10
        Me.Label10.Text = "Độc giả loại"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(259, 27)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(49, 15)
        Me.Label11.TabIndex = 11
        Me.Label11.Text = "Hạn thẻ"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(309, 111)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(58, 15)
        Me.Label13.TabIndex = 14
        Me.Label13.Text = "Thời Gian"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(50, 89)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(88, 15)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Phí hàng tháng"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(16, 52)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(69, 15)
        Me.Label14.TabIndex = 14
        Me.Label14.Text = "Tên Độc giả"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Segoe UI", 8.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(16, 29)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(68, 15)
        Me.Label16.TabIndex = 16
        Me.Label16.Text = "Mã Độc Giả"
        '
        'txtFee
        '
        Me.txtFee.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFee.Location = New System.Drawing.Point(158, 87)
        Me.txtFee.Name = "txtFee"
        Me.txtFee.ReadOnly = True
        Me.txtFee.Size = New System.Drawing.Size(107, 22)
        Me.txtFee.TabIndex = 6
        Me.txtFee.Text = "0"
        '
        'txtNameBill
        '
        Me.txtNameBill.Enabled = False
        Me.txtNameBill.Location = New System.Drawing.Point(111, 49)
        Me.txtNameBill.Name = "txtNameBill"
        Me.txtNameBill.Size = New System.Drawing.Size(107, 22)
        Me.txtNameBill.TabIndex = 6
        '
        'txtIDreadBill
        '
        Me.txtIDreadBill.Enabled = False
        Me.txtIDreadBill.Location = New System.Drawing.Point(111, 23)
        Me.txtIDreadBill.Name = "txtIDreadBill"
        Me.txtIDreadBill.Size = New System.Drawing.Size(107, 22)
        Me.txtIDreadBill.TabIndex = 8
        '
        'Button1
        '
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.ImageIndex = 6
        Me.Button1.ImageList = Me.ImageList1
        Me.Button1.Location = New System.Drawing.Point(167, 331)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(137, 35)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "    &Quản Lý Mượn Trả"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lblHuongDanHoaDon
        '
        Me.lblHuongDanHoaDon.AllowDrop = True
        Me.lblHuongDanHoaDon.AutoSize = True
        Me.lblHuongDanHoaDon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblHuongDanHoaDon.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHuongDanHoaDon.ForeColor = System.Drawing.Color.Maroon
        Me.lblHuongDanHoaDon.Location = New System.Drawing.Point(677, 201)
        Me.lblHuongDanHoaDon.Name = "lblHuongDanHoaDon"
        Me.lblHuongDanHoaDon.Size = New System.Drawing.Size(70, 21)
        Me.lblHuongDanHoaDon.TabIndex = 15
        Me.lblHuongDanHoaDon.Text = "Thông tin"
        '
        'frmQLDocGia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1218, 383)
        Me.Controls.Add(Me.lblHuongDanHoaDon)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.grBill)
        Me.Controls.Add(Me.btnBill)
        Me.Controls.Add(Me.grInfoReader)
        Me.Controls.Add(Me.grTimThongtin)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmQLDocGia"
        Me.Text = "Reader"
        Me.grTimThongtin.ResumeLayout(False)
        Me.grTimThongtin.PerformLayout()
        CType(Me.dtReader, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grInfoReader.ResumeLayout(False)
        Me.grInfoReader.PerformLayout()
        Me.grBill.ResumeLayout(False)
        Me.grBill.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnRegReader As Button
    Friend WithEvents grTimThongtin As GroupBox
    Friend WithEvents txtSearch As TextBox
    Friend WithEvents ImageList1 As ImageList
    Friend WithEvents grInfoReader As GroupBox
    Friend WithEvents txtName As TextBox
    Friend WithEvents txtIDCard As TextBox
    Friend WithEvents txtIDReader As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents txtAddr As TextBox
    Friend WithEvents btnBill As Button
    Friend WithEvents btnDel As Button
    Friend WithEvents btnRefresh As Button
    Friend WithEvents btnUpdate As Button
    Friend WithEvents cbLoaiDocGia As ComboBox
    Friend WithEvents cbSex As ComboBox
    Friend WithEvents dtPickBD As DateTimePicker
    Friend WithEvents btnCloseInFo As Button
    Friend WithEvents grBill As GroupBox
    Friend WithEvents cbBill As ComboBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents txtNameBill As TextBox
    Friend WithEvents txtIDreadBill As TextBox
    Friend WithEvents btnOutBill As Button
    Friend WithEvents btnBack As Button
    Friend WithEvents Label9 As Label
    Friend WithEvents txtFee As TextBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Label12 As Label
    Friend WithEvents cbSearch As ComboBox
    Friend WithEvents dateBill As DateTimePicker
    Friend WithEvents DateExp As DateTimePicker
    Friend WithEvents dtReader As DataGridView
    Friend WithEvents dtpExp As DateTimePicker
    Friend WithEvents madg As DataGridViewTextBoxColumn
    Friend WithEvents tendg As DataGridViewTextBoxColumn
    Friend WithEvents cmnd As DataGridViewTextBoxColumn
    Friend WithEvents Role As DataGridViewTextBoxColumn
    Friend WithEvents Fee As DataGridViewTextBoxColumn
    Friend WithEvents NBookBor As DataGridViewTextBoxColumn
    Friend WithEvents Bookexp As DataGridViewTextBoxColumn
    Friend WithEvents Label13 As Label
    Friend WithEvents lblHuongDanHoaDon As Label
End Class
