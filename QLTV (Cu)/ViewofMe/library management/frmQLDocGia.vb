﻿Imports System.IO
Imports BUS
Imports DTO
Public Class frmQLDocGia
    Dim SelectionE As Integer
    Dim temp As DocGiaDTO
    Private Sub btnRegReader_Click(sender As Object, e As EventArgs) Handles btnRegReader.Click
        Dim frmReg = New frmThemDocGia
        frmReg.ShowDialog()
    End Sub



    Private Sub frmManageReader_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Loadcb(cbBill)
        grInfoReader.Visible = False
        grBill.Visible = False
        Width -= grBill.Width
        cbSearch.SelectedIndex = 0
        lblHuongDanHoaDon.Visible = False

    End Sub

    Private Sub lsDataReader_DoubleClick(sender As Object, e As EventArgs)
        If (grInfoReader.Visible = False) Then
            grInfoReader.Visible = True
            If (grBill.Visible = False) Then
                Width += grInfoReader.Width
            Else
                grBill.Visible = False
            End If
        End If
    End Sub

    Private Sub btnBill_Click(sender As Object, e As EventArgs) Handles btnBill.Click
        If (grBill.Visible = False) Then
            grBill.Visible = True
            If (grInfoReader.Visible = False) Then
                Width += grBill.Width
            Else
                grInfoReader.Visible = False
            End If
        End If
        Dim strnot As String =
"***Lưu ý
-Kiểm tra lại thông tin.
-Chỉnh sửa thông tin bằng cách nhấp đúp vào độc giả để mở nơi chỉnh sửa"
        lblHuongDanHoaDon.Text = strnot
        lblHuongDanHoaDon.Visible = True
    End Sub


    Private Sub btnCloseInFo_Click(sender As Object, e As EventArgs) Handles btnCloseInFo.Click
        If (grInfoReader.Visible = True) Then
            grInfoReader.Visible = False
            Width -= grInfoReader.Width
            lblHuongDanHoaDon.Visible = False
        End If
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        If (grBill.Visible = True) Then
            grBill.Visible = False
            Width -= grBill.Width
            lblHuongDanHoaDon.Visible = False
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim frmBrow As New frmMuon_Tra
        frmBrow.Show()
    End Sub

    Private Sub txtSearch_Click(sender As Object, e As EventArgs) Handles txtSearch.Click
        txtSearch.Clear()
    End Sub

    Private Sub btnOutBill_Click(sender As Object, e As EventArgs) Handles btnOutBill.Click
        '        Dim nameFile As String = "test.txt"

        '        Dim Bill As New StreamWriter(nameFile)
        '        Bill.Write(
        '"-------------------------Hóa Đơn-----------------------------------
        'Số Bill		|: 123        Mã ĐG		|: {1}      Loại Thẻ		|: {5}        
        '-------------------------------------------------------------------------
        'Tên Độc Giả	|: {2}
        '-------------------------------------------------------------------------
        'Số Tiền		|: {3}
        '-------------------------------------------------------------------------
        'Hạn Sử Dụng	|: {4}
        '-------------------------------------------------------------------------

        '-------------------------------------------------------------------------
        'Cảm ơn và hẹn gặp lại! :)
        '                   Lúc {0}", dateBill.Text, txtIDreadBill.Text, txtNameBill.Text, txtFee.Text, DateExp.Text, cbBill.Text)
        '        Bill.Close()
        '        MessageBox.Show("Lưu Thành Công", "Mess")
    End Sub


    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        Dim str = "%" + txtSearch.Text.ToString() + "%"

        dtReader.DataSource = DocGiaBUS.Loader(str, cbSearch.SelectedIndex)

    End Sub

    Private Sub cbSearch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbSearch.SelectedIndexChanged
        txtSearch.Clear()
    End Sub

    Private Sub DTBlings(ByRef temp As DocGiaDTO)
        If SelectionE >= 0 Then
            Loadcb(cbLoaiDocGia)
            temp = DocGiaBUS.LoadInfo(dtReader.Rows(SelectionE).Cells(0).Value.ToString())
            txtAddr.Text = temp.DiaChi
            txtIDReader.Text = temp.MaDG
            txtIDCard.Text = temp.CMND
            txtName.Text = temp.TenDG
            dtPickBD.Text = temp.NgaySinh
            dtpExp.Text = temp.HSD
            cbSex.Text = temp.GioiTinh
            cbLoaiDocGia.SelectedIndex = temp.Role_ID
        End If
    End Sub

    Private Sub dtReader_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dtReader.CellDoubleClick
        SelectionE = e.RowIndex
        Dim strnot As String =
"***Lưu ý
-Thông tin phải được nhập đầy đủ
-Nút xóa: để xóa độc giả trong cơ sở dữ liệu
-Nút cập nhật: để chỉnh xửa thông tin đã thay đổi của độc giả
-Nút làm lại: làm mới lại các thông tin"
        If SelectionE >= 0 Then
            If (grInfoReader.Visible = False) Then
                grInfoReader.Visible = True
                If (grBill.Visible = False) Then
                    Width += grInfoReader.Width
                Else
                    grBill.Visible = False
                End If
            End If
            DTBlings(temp)
            lblHuongDanHoaDon.Text = strnot
            lblHuongDanHoaDon.Visible = True
        End If
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        txtAddr.Text = temp.DiaChi
        txtIDReader.Text = temp.MaDG
        txtIDCard.Text = temp.CMND
        txtName.Text = temp.TenDG
        dtPickBD.Text = temp.NgaySinh
        dtpExp.Text = temp.HSD
        cbSex.Text = temp.GioiTinh
        cbLoaiDocGia.SelectedIndex = temp.Role_ID
    End Sub

    Private Sub btnDel_Click(sender As Object, e As EventArgs) Handles btnDel.Click
        Dim resbtn As DialogResult = MessageBox.Show("Bạn có chắc chắn muốn xóa???", "Lưu ý!!!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
        If resbtn = DialogResult.Yes Then
            DocGiaBUS.XoaDG(temp.MaDG)
        End If

    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim Tem As New DocGiaDTO
        If txtAddr.Text <> "" And txtIDCard.Text <> "" And txtName.Text <> "" Then
            Tem.CMND = txtIDCard.Text
            Tem.NgaySinh = dtPickBD.Text
            Tem.Role_ID = cbLoaiDocGia.SelectedIndex
            Tem.GioiTinh = cbSex.Text
            Tem.TenDG = txtName.Text
            Tem.DiaChi = txtAddr.Text
            Tem.MaDG = txtIDReader.Text
            DocGiaBUS.Update_readerBUS(Tem)
            DTBlings(temp)
        Else
            MessageBox.Show("Kiểm tra lại thông tin không đươc bỏ trống!!!", "Lưu ý ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub Loadcb(ByRef cb As ComboBox)
        Dim tbl As New DataTable
        tbl = DocGiaBUS.loadcbb()
        cb.DataSource = tbl
        cb.DisplayMember = "Role"
    End Sub

    Private Sub dtReader_SelectionChanged(sender As Object, e As EventArgs) Handles dtReader.SelectionChanged
        SelectionE = dtReader.CurrentRow.Index
        temp = DocGiaBUS.LoadInfo(dtReader.Rows(SelectionE).Cells(0).Value.ToString())
        txtIDreadBill.Text = temp.MaDG
        txtFee.Text = temp.Fee
        txtNameBill.Text = temp.TenDG
        cbBill.SelectedIndex = temp.Role_ID
        DateExp.Text = temp.NgaySinh
    End Sub


End Class