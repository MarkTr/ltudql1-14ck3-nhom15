﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQLSach
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgrBook = New System.Windows.Forms.DataGridView()
        Me.txtKeyWords = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtTitle = New System.Windows.Forms.ComboBox()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnDel = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnCapNhat = New System.Windows.Forms.Button()
        Me.grThaoTac = New System.Windows.Forms.GroupBox()
        CType(Me.dgrBook, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grThaoTac.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgrBook
        '
        Me.dgrBook.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dgrBook.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgrBook.Location = New System.Drawing.Point(12, 58)
        Me.dgrBook.Name = "dgrBook"
        Me.dgrBook.Size = New System.Drawing.Size(627, 227)
        Me.dgrBook.TabIndex = 0
        '
        'txtKeyWords
        '
        Me.txtKeyWords.Location = New System.Drawing.Point(68, 23)
        Me.txtKeyWords.Multiline = True
        Me.txtKeyWords.Name = "txtKeyWords"
        Me.txtKeyWords.Size = New System.Drawing.Size(236, 20)
        Me.txtKeyWords.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Tìm"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(310, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Theo"
        '
        'txtTitle
        '
        Me.txtTitle.FormattingEnabled = True
        Me.txtTitle.Location = New System.Drawing.Point(343, 23)
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.Size = New System.Drawing.Size(195, 21)
        Me.txtTitle.TabIndex = 4
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(544, 21)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(75, 23)
        Me.btnSearch.TabIndex = 5
        Me.btnSearch.Text = "Tìm"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnAdd.Location = New System.Drawing.Point(17, 19)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(75, 33)
        Me.btnAdd.TabIndex = 6
        Me.btnAdd.Text = "Thêm Sách"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnDel
        '
        Me.btnDel.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnDel.Location = New System.Drawing.Point(17, 58)
        Me.btnDel.Name = "btnDel"
        Me.btnDel.Size = New System.Drawing.Size(75, 33)
        Me.btnDel.TabIndex = 7
        Me.btnDel.Text = "Xóa"
        Me.btnDel.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnEdit.Location = New System.Drawing.Point(17, 97)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 31)
        Me.btnEdit.TabIndex = 8
        Me.btnEdit.Text = "Sửa"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnCapNhat
        '
        Me.btnCapNhat.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnCapNhat.Location = New System.Drawing.Point(17, 134)
        Me.btnCapNhat.Name = "btnCapNhat"
        Me.btnCapNhat.Size = New System.Drawing.Size(75, 34)
        Me.btnCapNhat.TabIndex = 9
        Me.btnCapNhat.Text = "Cập Nhật"
        Me.btnCapNhat.UseVisualStyleBackColor = True
        '
        'grThaoTac
        '
        Me.grThaoTac.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grThaoTac.Controls.Add(Me.btnAdd)
        Me.grThaoTac.Controls.Add(Me.btnCapNhat)
        Me.grThaoTac.Controls.Add(Me.btnDel)
        Me.grThaoTac.Controls.Add(Me.btnEdit)
        Me.grThaoTac.Location = New System.Drawing.Point(645, 58)
        Me.grThaoTac.Name = "grThaoTac"
        Me.grThaoTac.Size = New System.Drawing.Size(107, 187)
        Me.grThaoTac.TabIndex = 10
        Me.grThaoTac.TabStop = False
        Me.grThaoTac.Text = "Thao Tác"
        '
        'frmQLSach
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(761, 310)
        Me.Controls.Add(Me.grThaoTac)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.txtTitle)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtKeyWords)
        Me.Controls.Add(Me.dgrBook)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmQLSach"
        Me.Text = "Quản Lý Sách"
        CType(Me.dgrBook, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grThaoTac.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgrBook As System.Windows.Forms.DataGridView
    Friend WithEvents txtKeyWords As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtTitle As System.Windows.Forms.ComboBox
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnDel As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnCapNhat As Button
    Friend WithEvents grThaoTac As GroupBox
End Class
