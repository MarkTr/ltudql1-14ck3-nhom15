﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmThemDocGia
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnReg = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.grInfo = New System.Windows.Forms.GroupBox()
        Me.dtpickExp = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cbType = New System.Windows.Forms.ComboBox()
        Me.cbSex = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dtPick = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtAdr = New System.Windows.Forms.TextBox()
        Me.txtIDcard = New System.Windows.Forms.TextBox()
        Me.txtFee = New System.Windows.Forms.TextBox()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.txtIDReader = New System.Windows.Forms.TextBox()
        Me.btnRefesh = New System.Windows.Forms.Button()
        Me.Col1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grRole = New System.Windows.Forms.GroupBox()
        Me.RoleSP = New System.Windows.Forms.Label()
        Me.RoleNgay = New System.Windows.Forms.Label()
        Me.RoleSach = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.btnRole = New System.Windows.Forms.Button()
        Me.grInfo.SuspendLayout()
        Me.grRole.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnReg
        '
        Me.btnReg.AutoSize = True
        Me.btnReg.Location = New System.Drawing.Point(14, 187)
        Me.btnReg.Name = "btnReg"
        Me.btnReg.Size = New System.Drawing.Size(78, 25)
        Me.btnReg.TabIndex = 0
        Me.btnReg.Text = "&Thêm"
        Me.btnReg.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(536, 187)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(78, 24)
        Me.btnCancel.TabIndex = 0
        Me.btnCancel.Text = "&Thoát"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'grInfo
        '
        Me.grInfo.Controls.Add(Me.dtpickExp)
        Me.grInfo.Controls.Add(Me.Label8)
        Me.grInfo.Controls.Add(Me.cbType)
        Me.grInfo.Controls.Add(Me.cbSex)
        Me.grInfo.Controls.Add(Me.Label7)
        Me.grInfo.Controls.Add(Me.dtPick)
        Me.grInfo.Controls.Add(Me.Label5)
        Me.grInfo.Controls.Add(Me.Label6)
        Me.grInfo.Controls.Add(Me.Label4)
        Me.grInfo.Controls.Add(Me.Label3)
        Me.grInfo.Controls.Add(Me.Label10)
        Me.grInfo.Controls.Add(Me.Label2)
        Me.grInfo.Controls.Add(Me.Label1)
        Me.grInfo.Controls.Add(Me.txtAdr)
        Me.grInfo.Controls.Add(Me.txtIDcard)
        Me.grInfo.Controls.Add(Me.txtFee)
        Me.grInfo.Controls.Add(Me.txtName)
        Me.grInfo.Controls.Add(Me.txtIDReader)
        Me.grInfo.Location = New System.Drawing.Point(12, 12)
        Me.grInfo.Name = "grInfo"
        Me.grInfo.Size = New System.Drawing.Size(600, 169)
        Me.grInfo.TabIndex = 2
        Me.grInfo.TabStop = False
        Me.grInfo.Text = "Info.Reader"
        '
        'dtpickExp
        '
        Me.dtpickExp.Enabled = False
        Me.dtpickExp.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpickExp.Location = New System.Drawing.Point(95, 101)
        Me.dtpickExp.Name = "dtpickExp"
        Me.dtpickExp.Size = New System.Drawing.Size(134, 20)
        Me.dtpickExp.TabIndex = 5
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(22, 107)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(28, 13)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Exp."
        '
        'cbType
        '
        Me.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbType.FormattingEnabled = True
        Me.cbType.Items.AddRange(New Object() {"Normal", "V.I.P"})
        Me.cbType.Location = New System.Drawing.Point(411, 97)
        Me.cbType.Name = "cbType"
        Me.cbType.Size = New System.Drawing.Size(68, 21)
        Me.cbType.TabIndex = 3
        '
        'cbSex
        '
        Me.cbSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSex.FormattingEnabled = True
        Me.cbSex.Items.AddRange(New Object() {"Male", "Female"})
        Me.cbSex.Location = New System.Drawing.Point(411, 70)
        Me.cbSex.Name = "cbSex"
        Me.cbSex.Size = New System.Drawing.Size(68, 21)
        Me.cbSex.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(325, 100)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(69, 13)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Type Reader"
        '
        'dtPick
        '
        Me.dtPick.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtPick.Location = New System.Drawing.Point(95, 71)
        Me.dtPick.Name = "dtPick"
        Me.dtPick.Size = New System.Drawing.Size(134, 20)
        Me.dtPick.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(325, 73)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(25, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Sex"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(325, 51)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 13)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Adress"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(22, 79)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Birthday"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(22, 52)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Id.Card"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(24, 138)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(25, 13)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "Fee"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(325, 25)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Name"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "ID Reader"
        '
        'txtAdr
        '
        Me.txtAdr.Location = New System.Drawing.Point(411, 44)
        Me.txtAdr.Name = "txtAdr"
        Me.txtAdr.Size = New System.Drawing.Size(172, 20)
        Me.txtAdr.TabIndex = 0
        '
        'txtIDcard
        '
        Me.txtIDcard.Location = New System.Drawing.Point(95, 45)
        Me.txtIDcard.Name = "txtIDcard"
        Me.txtIDcard.Size = New System.Drawing.Size(172, 20)
        Me.txtIDcard.TabIndex = 0
        '
        'txtFee
        '
        Me.txtFee.Location = New System.Drawing.Point(95, 131)
        Me.txtFee.Name = "txtFee"
        Me.txtFee.ReadOnly = True
        Me.txtFee.Size = New System.Drawing.Size(81, 20)
        Me.txtFee.TabIndex = 0
        Me.txtFee.Text = "0"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(411, 19)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(172, 20)
        Me.txtName.TabIndex = 0
        '
        'txtIDReader
        '
        Me.txtIDReader.Location = New System.Drawing.Point(95, 22)
        Me.txtIDReader.Name = "txtIDReader"
        Me.txtIDReader.ReadOnly = True
        Me.txtIDReader.Size = New System.Drawing.Size(172, 20)
        Me.txtIDReader.TabIndex = 0
        Me.txtIDReader.Text = "DG"
        '
        'btnRefesh
        '
        Me.btnRefesh.Location = New System.Drawing.Point(452, 187)
        Me.btnRefesh.Name = "btnRefesh"
        Me.btnRefesh.Size = New System.Drawing.Size(78, 25)
        Me.btnRefesh.TabIndex = 0
        Me.btnRefesh.Text = "&Làm Lại"
        Me.btnRefesh.UseVisualStyleBackColor = True
        '
        'Col1
        '
        Me.Col1.Name = "Col1"
        '
        'grRole
        '
        Me.grRole.Controls.Add(Me.RoleSP)
        Me.grRole.Controls.Add(Me.RoleNgay)
        Me.grRole.Controls.Add(Me.RoleSach)
        Me.grRole.Controls.Add(Me.Label13)
        Me.grRole.Controls.Add(Me.Label12)
        Me.grRole.Controls.Add(Me.Label11)
        Me.grRole.Controls.Add(Me.btnRole)
        Me.grRole.Location = New System.Drawing.Point(618, 12)
        Me.grRole.Name = "grRole"
        Me.grRole.Size = New System.Drawing.Size(200, 169)
        Me.grRole.TabIndex = 3
        Me.grRole.TabStop = False
        Me.grRole.Text = "Role"
        '
        'RoleSP
        '
        Me.RoleSP.AutoSize = True
        Me.RoleSP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RoleSP.ForeColor = System.Drawing.Color.DarkRed
        Me.RoleSP.Location = New System.Drawing.Point(153, 77)
        Me.RoleSP.Name = "RoleSP"
        Me.RoleSP.Size = New System.Drawing.Size(15, 15)
        Me.RoleSP.TabIndex = 1
        Me.RoleSP.Text = "0"
        '
        'RoleNgay
        '
        Me.RoleNgay.AutoSize = True
        Me.RoleNgay.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RoleNgay.ForeColor = System.Drawing.Color.DarkRed
        Me.RoleNgay.Location = New System.Drawing.Point(153, 49)
        Me.RoleNgay.Name = "RoleNgay"
        Me.RoleNgay.Size = New System.Drawing.Size(15, 15)
        Me.RoleNgay.TabIndex = 1
        Me.RoleNgay.Text = "0"
        '
        'RoleSach
        '
        Me.RoleSach.AutoSize = True
        Me.RoleSach.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RoleSach.ForeColor = System.Drawing.Color.DarkRed
        Me.RoleSach.Location = New System.Drawing.Point(153, 25)
        Me.RoleSach.Name = "RoleSach"
        Me.RoleSach.Size = New System.Drawing.Size(15, 15)
        Me.RoleSach.TabIndex = 1
        Me.RoleSach.Text = "0"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.DarkRed
        Me.Label13.Location = New System.Drawing.Point(6, 79)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(93, 15)
        Me.Label13.TabIndex = 1
        Me.Label13.Text = "Tài liệu đặc biệt"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.DarkRed
        Me.Label12.Location = New System.Drawing.Point(6, 48)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(119, 15)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "Số ngày mượn tối đa"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.DarkRed
        Me.Label11.Location = New System.Drawing.Point(6, 22)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(116, 15)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Số sách được mượn"
        '
        'btnRole
        '
        Me.btnRole.Location = New System.Drawing.Point(6, 138)
        Me.btnRole.Name = "btnRole"
        Me.btnRole.Size = New System.Drawing.Size(38, 23)
        Me.btnRole.TabIndex = 0
        Me.btnRole.Text = "<<"
        Me.btnRole.UseVisualStyleBackColor = True
        '
        'frmThemDocGia
        '
        Me.AcceptButton = Me.btnReg
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(826, 225)
        Me.Controls.Add(Me.grRole)
        Me.Controls.Add(Me.grInfo)
        Me.Controls.Add(Me.btnRefesh)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnReg)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmThemDocGia"
        Me.Text = "Regeistration"
        Me.grInfo.ResumeLayout(False)
        Me.grInfo.PerformLayout()
        Me.grRole.ResumeLayout(False)
        Me.grRole.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnReg As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents grInfo As GroupBox
    Friend WithEvents cbSex As ComboBox
    Friend WithEvents dtPick As DateTimePicker
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtAdr As TextBox
    Friend WithEvents txtIDcard As TextBox
    Friend WithEvents txtName As TextBox
    Friend WithEvents txtIDReader As TextBox
    Friend WithEvents btnRefesh As Button
    Friend WithEvents cbType As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents txtFee As TextBox
    Friend WithEvents dtpickExp As DateTimePicker
    Friend WithEvents Col1 As DataGridViewTextBoxColumn
    Friend WithEvents grRole As GroupBox
    Friend WithEvents RoleSP As Label
    Friend WithEvents RoleNgay As Label
    Friend WithEvents RoleSach As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents btnRole As Button
End Class
