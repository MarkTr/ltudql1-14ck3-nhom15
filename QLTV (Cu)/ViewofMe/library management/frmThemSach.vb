﻿Imports BUS
Imports DTO
Public Class frmThemSach
    Dim a As New frmThemDocGia
    Sub LoadMaSach()
        Dim str As String = SachBUS.LayMaSach()
        Dim num As Integer
        Dim tem As String = "0000"
        If str = "" Then
            str = "BOOK0000"
        Else
            num = str.Remove(0, 4) + 1
            str = num
            tem = tem.Remove(0, str.Length()) + str
        End If
        txtMaSach.Text = "BOOK" + tem
        txtBookTitle.Clear()
    End Sub

    Private Sub frmThemSach_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadMaSach()
    End Sub
End Class