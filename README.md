#MÔN LẬP TRÌNH ỨNG DỤNG QUẢN LÝ

###--------------------------------------------------###
* Đồ án lập trình ứng dụng quản lý thư viện.
* Version: 1.0.0.0

###--------------------------------------------------###

* Được viết bằng ngôn ngữ: VB.NET
* Môi trường lập trình (IDE): Microsoft Visual Studio
* Cơ sở dữ liệu được xây dựng trên: SQL Server

###--------------------------------------------------###
* Giáo viên hướng dẫn: Ngô Chánh Đức
* Nhóm: 15
     + Phạm Thành Trọng      - 1363110
     + Trịnh Minh Cường      - 1460173
     + Võ Thanh Danh         - 1460181
     + Nguyễn Hải Khánh Huy  - 1460408